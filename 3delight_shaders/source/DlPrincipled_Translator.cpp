#include "DlPrincipled_translator.h"
#include "3DelightEnvironment.h"
#include "dl_principled.h"

Delight_Principled::Delight_Principled()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlPrincipled.oso";
	m_ids_to_names[COATING_LAYER_THICKNESS] =				std::make_pair("","coating_thickness");
	m_ids_to_names[COATING_LAYER_THICKNESS_SHADER] =		std::make_pair("outColor[0]","coating_thickness");
	m_ids_to_names[COATING_LAYER_COLOR] =					std::make_pair("","coating_color");
	m_ids_to_names[COATING_LAYER_COLOR_SHADER] =			std::make_pair("outColor","coating_color");
	m_ids_to_names[COATING_LAYER_ROUGHNESS] =			    std::make_pair("","coating_roughness");
	m_ids_to_names[COATING_LAYER_ROUGHNESS_SHADER] =		std::make_pair("outColor[0]","coating_roughness");
	m_ids_to_names[COATING_LAYER_SPECULAR_LEVEL] =			std::make_pair("","coating_specular_level");
	m_ids_to_names[COATING_LAYER_SPECULAR_LEVEL_SHADER] =	std::make_pair("outColor[0]","coating_specular_level");
	m_ids_to_names[BASE_LAYER_COLOR] =						std::make_pair("","i_color");
	m_ids_to_names[BASE_LAYER_COLOR_SHADER] =				std::make_pair("outColor", "i_color");
	m_ids_to_names[BASE_LAYER_ROUGHNESS] =					std::make_pair("","roughness");
	m_ids_to_names[BASE_LAYER_ROUGHNESS_SHADER] =			std::make_pair("outColor[0]", "roughness");
	m_ids_to_names[BASE_LAYER_SPECULAR_LEVEL] =				std::make_pair("","specular_level");
	m_ids_to_names[BASE_LAYER_SPECULAR_LEVEL_SHADER] =		std::make_pair("outColor[0]", "specular_level");
	m_ids_to_names[BASE_LAYER_METALLIC] =					std::make_pair("","metallic");
	m_ids_to_names[BASE_LAYER_METALLIC_SHADER] =			std::make_pair("outColor[0]", "metallic");
	m_ids_to_names[BASE_LAYER_ANISOTROPY] =					std::make_pair("","anisotropy");
	m_ids_to_names[BASE_LAYER_ANISOTROPY_SHADER] =			std::make_pair("outColor[0]", "anisotropy");
	m_ids_to_names[BASE_LAYER_ANISOTROPY_DIRECTION] =		std::make_pair("","anisotropy_direction");
	m_ids_to_names[BASE_LAYER_ANISOTROPY_DIRECTION_SHADER]= std::make_pair("outColor","anisotropy_direction");
	m_ids_to_names[BASE_LAYER_OPACITY] =					std::make_pair("","opacity");
	m_ids_to_names[BASE_LAYER_OPACITY_SHADER] =				std::make_pair("outColor[0]","opacity");
	m_ids_to_names[SUBSURFACE_WEIGHT] =						std::make_pair("", "sss_weight");
	m_ids_to_names[SUBSURFACE_WEIGHT_SHADER] =				std::make_pair("outColor[0]", "sss_weight");
	m_ids_to_names[SUBSURFACE_COLOR] =						std::make_pair("","sss_color");
	m_ids_to_names[SUBSURFACE_COLOR_SHADER] =				std::make_pair("outColor", "sss_color");
	m_ids_to_names[SUBSURFACE_ANISOTROPY] =					std::make_pair("","sss_anisotropy");
	m_ids_to_names[SUBSURFACE_ANISOTROPY_SHADER] =			std::make_pair("outColor[0]", "sss_anisotropy");
	m_ids_to_names[SUBSURFACE_SCALE] =						std::make_pair("", "sss_scale");
	m_ids_to_names[SUBSURFACE_SCALE_SHADER] =				std::make_pair("outColor[0]", "sss_scale");
	m_ids_to_names[REFRACTION_WEIGHT] =						std::make_pair("", "refract_weight");
	m_ids_to_names[REFRACTION_WEIGHT_SHADER] =				std::make_pair("outColor[0]", "refract_weight");
	m_ids_to_names[REFRACTION_COLOR] =						std::make_pair("", "volumetric_transparency_color");
	m_ids_to_names[REFRACTION_COLOR_SHADER] =				std::make_pair("outColor", "volumetric_transparency_color");
	m_ids_to_names[REFRACTION_SCATTER] =					std::make_pair("", "volumetric_scattering_color");
	m_ids_to_names[REFRACTION_SCATTER_SHADER] =				std::make_pair("outColor", "volumetric_scattering_color");
	m_ids_to_names[REFRACTION_IOR] =						std::make_pair("", "refract_ior");
	m_ids_to_names[REFRACTION_IOR_SHADER] =					std::make_pair("outColor[0]", "refract_ior");
	m_ids_to_names[REFRACTION_ROUGHNESS] =					std::make_pair("", "refract_roughness");
	m_ids_to_names[REFRACTION_ROUGHNESS_SHADER] =			std::make_pair("outColor[0]", "refract_roughness");
	m_ids_to_names[REFRACTION_DENSITY] =					std::make_pair("", "volumetric_density");
	m_ids_to_names[REFRACTION_DENSITY_SHADER] =				std::make_pair("outColor[0]", "volumetric_density");
	m_ids_to_names[INCANDESCENCE_COLOR] =					std::make_pair("", "incandescence");
	m_ids_to_names[INCANDESCENCE_COLOR_SHADER] =			std::make_pair("outColor", "incandescence");
	m_ids_to_names[INCANDESCENCE_INTENSITY] =				std::make_pair("","incandescence_intensity");
	m_ids_to_names[BUMP_TYPE] =								std::make_pair("","disp_normal_bump_type");
	m_ids_to_names[BUMP_INTENSITY] =						std::make_pair("","disp_normal_bump_intensity");
	m_ids_to_names[BUMP_VALUE] =							std::make_pair("outColor","disp_normal_bump_value");
	m_ids_to_names[BUMP_LAYERS_AFFECTED] =					std::make_pair("","normal_bump_affect_layer");
	m_ids_to_names[BUMP_LAYERS_AFFECTED] =					std::make_pair("","normal_bump_affect_layer");
	m_ids_to_names[DL_AOV_GROUP] =							std::make_pair("outColor","aovGroup");
}
