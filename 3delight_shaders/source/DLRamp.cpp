#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_ramp.h"

static void
initGradient(BaseContainer& bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_GRADIENT, DEFAULTVALUE);
	Gradient* gradient =
		(Gradient*) data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);

	if (gradient) {
		GradientKnot k1, k2;

		if (id == COLOR_RAMP_GRADIENT) {
			k1.col = HSVToRGB(Vector(0, 1, 0.0));
			k1.pos = 0.0;
			k2.col = HSVToRGB(Vector(0, 0, 1.0));
			k2.pos = 1.0;
			gradient->InsertKnot(k1);
			gradient->InsertKnot(k2);
		}
	}

	bc.SetData(id, data);
}

class DLRamp : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLRamp);
	}
};

Bool DLRamp::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(RAMP_MODE, RAMP_MODE_HORIZONTAL);
	data->SetInt32(RAMP_COLOR_MIX, RAMP_COLOR_MIX_RGB);
	data->SetFloat(RAMP_TRANSFORM_REPEAT, 1.0);
	data->SetFloat(RAMP_TRANSFORM_OFFSET, 0);
	data->SetBool(RAMP_TRANSFORM_REVERSE, FALSE);
	data->SetFloat(RAMP_DISTORTION_INTENSITY, 0.125);
	data->SetFloat(RAMP_NOISE_INTENSITY, 0.0);
	data->SetFloat(RAMP_NOISE_HUE, 0.5);
	data->SetFloat(RAMP_NOISE_SATURATION, 0.5);
	data->SetFloat(RAMP_NOISE_VALUE, 0.5);
	data->SetFloat(RAMP_NOISE_TRANSFORM_SCALE, 0.5);
	data->SetFloat(RAMP_NOISE_TRANSFORM_OFFSET, 0.0);
	data->SetFloat(RAMP_NOISE_TRANSFORM_STEPPING, 0.0);
	data->SetFloat(RAMP_NOISE_TRANSFORM_TIME, 0.0);
	data->SetInt32(RAMP_NOISE_LAYERS, 1);
	data->SetFloat(RAMP_NOISE_LAYERS_PERSISTENCE, 0.70);
	data->SetFloat(RAMP_NOISE_LAYERS_SCALE, 0.450);
	data->SetBool(RAMP_ADJUST_INVERT, false);
	data->SetVector(RAMP_ADJUST_COLOR_GAIN, HSVToRGB(Vector(0, 0, 1)));
	data->SetVector(RAMP_ADJUST_COLOR_OFFSET, HSVToRGB(Vector(0, 0, 0)));
	data->SetBool(RAMP_ADJUST_INVERT, false);
	data->SetFloat(POPUP_RAMP_ADJUST_ALPHA_GAIN, 1.0);
	data->SetFloat(RAMP_ADJUST_ALPHA_OFFSET, 0.0);
	initGradient(*data, COLOR_RAMP_GRADIENT);
	return true;
}

Bool DLRamp::GetDDescription(GeListNode* node,
							 Description* description,
							 DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_RAMP);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	// RAMP TRANSFORM GROUP
	HideAndShowTextures(RAMP_TRANSFORM_REPEAT_GROUP_PARAM,
						RAMP_TRANSFORM_REPEAT,
						RAMP_TRANSFORM_REPEAT_SHADER,
						RAMP_TRANSFORM_REPEAT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_TRANSFORM_OFFSET_GROUP_PARAM,
						RAMP_TRANSFORM_OFFSET,
						RAMP_TRANSFORM_OFFSET_SHADER,
						RAMP_TRANSFORM_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	// DISTORTION GROUP
	HideAndShowTextures(RAMP_DISTORTION_INTENSITY_GROUP_PARAM,
						RAMP_DISTORTION_INTENSITY,
						RAMP_DISTORTION_INTENSITY_SHADER,
						RAMP_DISTORTION_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	// NOISE GROUP
	HideAndShowTextures(RAMP_NOISE_INTENSITY_GROUP_PARAM,
						RAMP_NOISE_INTENSITY,
						RAMP_NOISE_INTENSITY_SHADER,
						RAMP_NOISE_INTENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_HUE_GROUP_PARAM,
						RAMP_NOISE_HUE,
						RAMP_NOISE_HUE_SHADER,
						RAMP_NOISE_HUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_SATURATION_GROUP_PARAM,
						RAMP_NOISE_SATURATION,
						RAMP_NOISE_SATURATION_SHADER,
						RAMP_NOISE_SATURATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_VALUE_GROUP_PARAM,
						RAMP_NOISE_VALUE,
						RAMP_NOISE_VALUE_SHADER,
						RAMP_NOISE_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	// NOISE TRANSFORM GROUP
	HideAndShowTextures(RAMP_NOISE_TRANSFORM_SCALE_GROUP_PARAM,
						RAMP_NOISE_TRANSFORM_SCALE,
						RAMP_NOISE_TRANSFORM_SCALE_SHADER,
						RAMP_NOISE_TRANSFORM_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_TRANSFORM_OFFSET_GROUP_PARAM,
						RAMP_NOISE_TRANSFORM_OFFSET,
						RAMP_NOISE_TRANSFORM_OFFSET_SHADER,
						RAMP_NOISE_TRANSFORM_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_TRANSFORM_STEPPING_GROUP_PARAM,
						RAMP_NOISE_TRANSFORM_STEPPING,
						RAMP_NOISE_TRANSFORM_STEPPING_SHADER,
						RAMP_NOISE_TRANSFORM_STEPPING_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_TRANSFORM_TIME_GROUP_PARAM,
						RAMP_NOISE_TRANSFORM_TIME,
						RAMP_NOISE_TRANSFORM_TIME_SHADER,
						RAMP_NOISE_TRANSFORM_TIME_SHADER_TEMP,
						node,
						description,
						dldata);
	// NOISE LAYERS GROUP
	HideAndShowTextures(RAMP_NOISE_LAYERS_PERSISTENCE_GROUP_PARAM,
						RAMP_NOISE_LAYERS_PERSISTENCE,
						RAMP_NOISE_LAYERS_PERSISTENCE_SHADER,
						RAMP_NOISE_LAYERS_PERSISTENCE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_NOISE_LAYERS_SCALE_GROUP_PARAM,
						RAMP_NOISE_LAYERS_SCALE,
						RAMP_NOISE_LAYERS_SCALE_SHADER,
						RAMP_NOISE_LAYERS_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	// ADJUST GROUP
	HideAndShowTextures(RAMP_ADJUST_COLOR_GAIN_GROUP_PARAM,
						RAMP_ADJUST_COLOR_GAIN,
						RAMP_ADJUST_COLOR_GAIN_SHADER,
						RAMP_ADJUST_COLOR_GAIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_ADJUST_COLOR_OFFSET_GROUP_PARAM,
						RAMP_ADJUST_COLOR_OFFSET,
						RAMP_ADJUST_COLOR_OFFSET_SHADER,
						RAMP_ADJUST_COLOR_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_ADJUST_ALPHA_GAIN_GROUP_PARAM,
						RAMP_ADJUST_ALPHA_GAIN,
						RAMP_ADJUST_ALPHA_GAIN_SHADER,
						RAMP_ADJUST_ALPHA_GAIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(RAMP_ADJUST_ALPHA_OFFSET_GROUP_PARAM,
						RAMP_ADJUST_ALPHA_OFFSET,
						RAMP_ADJUST_ALPHA_OFFSET_SHADER,
						RAMP_ADJUST_ALPHA_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLRamp::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_RAMP_TRANSFORM_REPEAT:
			FillPopupMenu(dldata, dp, RAMP_TRANSFORM_REPEAT_GROUP_PARAM);
			break;

		case POPUP_RAMP_TRANSFORM_OFFSET:
			FillPopupMenu(dldata, dp, RAMP_TRANSFORM_OFFSET_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_INTENSITY:
			FillPopupMenu(dldata, dp, RAMP_NOISE_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_HUE:
			FillPopupMenu(dldata, dp, RAMP_NOISE_HUE_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_SATURATION:
			FillPopupMenu(dldata, dp, RAMP_NOISE_SATURATION_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_VALUE:
			FillPopupMenu(dldata, dp, RAMP_NOISE_VALUE_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_TRANSFORM_SCALE:
			FillPopupMenu(dldata, dp, RAMP_NOISE_TRANSFORM_SCALE_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_TRANSFORM_OFFSET:
			FillPopupMenu(dldata, dp, RAMP_NOISE_TRANSFORM_OFFSET_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_TRANSFORM_STEPPING:
			FillPopupMenu(dldata, dp, RAMP_NOISE_TRANSFORM_STEPPING_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_TRANSFORM_TIME:
			FillPopupMenu(dldata, dp, RAMP_NOISE_TRANSFORM_TIME_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_LAYERS_PERSISTENCE:
			FillPopupMenu(dldata, dp, RAMP_NOISE_LAYERS_PERSISTENCE_GROUP_PARAM);
			break;

		case POPUP_RAMP_NOISE_LAYERS_SCALE:
			FillPopupMenu(dldata, dp, RAMP_NOISE_LAYERS_SCALE_GROUP_PARAM);
			break;

		case POPUP_RAMP_ADJUST_COLOR_GAIN:
			FillPopupMenu(dldata, dp, RAMP_ADJUST_COLOR_GAIN_GROUP_PARAM);
			break;

		case POPUP_RAMP_ADJUST_COLOR_OFFSET:
			FillPopupMenu(dldata, dp, RAMP_ADJUST_COLOR_OFFSET_GROUP_PARAM);
			break;

		case POPUP_RAMP_ADJUST_ALPHA_GAIN:
			FillPopupMenu(dldata, dp, RAMP_ADJUST_ALPHA_GAIN_GROUP_PARAM);
			break;

		case POPUP_RAMP_ADJUST_ALPHA_OFFSET:
			FillPopupMenu(dldata, dp, RAMP_ADJUST_ALPHA_OFFSET_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Bool DLRamp::GetDEnabling(GeListNode* node,
						  const DescID& id,
						  const GeData& t_data,
						  DESCFLAGS_ENABLE flags,
						  const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case RAMP_NOISE_HUE:
	case RAMP_NOISE_HUE_SHADER:
	case POPUP_RAMP_NOISE_HUE:
	case RAMP_NOISE_SATURATION:
	case RAMP_NOISE_SATURATION_SHADER:
	case POPUP_RAMP_NOISE_SATURATION:
	case RAMP_NOISE_VALUE:
	case RAMP_NOISE_VALUE_SHADER:
	case POPUP_RAMP_NOISE_VALUE:
	case RAMP_NOISE_TRANSFORM_SCALE:
	case RAMP_NOISE_TRANSFORM_SCALE_SHADER:
	case POPUP_RAMP_NOISE_TRANSFORM_SCALE:
	case RAMP_NOISE_TRANSFORM_OFFSET:
	case RAMP_NOISE_TRANSFORM_OFFSET_SHADER:
	case POPUP_RAMP_NOISE_TRANSFORM_OFFSET:
	case RAMP_NOISE_TRANSFORM_STEPPING:
	case RAMP_NOISE_TRANSFORM_STEPPING_SHADER:
	case POPUP_RAMP_NOISE_TRANSFORM_STEPPING:
	case RAMP_NOISE_TRANSFORM_TIME:
	case RAMP_NOISE_TRANSFORM_TIME_SHADER:
	case POPUP_RAMP_NOISE_TRANSFORM_TIME:
	case RAMP_NOISE_LAYERS_LAYER:
		return (dldata->GetFloat(RAMP_NOISE_INTENSITY) > 0 || dldata->GetLink(RAMP_NOISE_INTENSITY_SHADER, doc) != nullptr);
		break;

	case RAMP_NOISE_LAYERS_PERSISTENCE:
	case RAMP_NOISE_LAYERS_PERSISTENCE_SHADER:
	case POPUP_RAMP_NOISE_LAYERS_PERSISTENCE:
	case RAMP_NOISE_LAYERS_SCALE:
	case RAMP_NOISE_LAYERS_SCALE_SHADER:
	case POPUP_RAMP_NOISE_LAYERS_SCALE:
		return (dldata->GetFloat(RAMP_NOISE_LAYERS_LAYER) > 1 && (dldata->GetFloat(RAMP_NOISE_INTENSITY) > 0 || dldata->GetLink(RAMP_NOISE_INTENSITY_SHADER, doc) != nullptr));
		break;

	default:
		break;
	}

	return true;
}

Vector
DLRamp::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterRampTexture(void)
{
	return RegisterShaderPlugin(
			   DL_RAMP, "Ramp"_s, 0, DLRamp::Alloc, "dl_ramp"_s, 0);
}
