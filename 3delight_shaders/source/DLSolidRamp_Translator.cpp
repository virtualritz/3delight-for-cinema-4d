#include "DLSolidRamp_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_solid_ramp.h"

Delight_SolidRamp::Delight_SolidRamp()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlSolidRamp.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[SOLID_RAMP_SHAPE] = std::make_pair("", "shape");
	m_ids_to_names[COLOR_SOLID_RAMP_GRADIENT] = std::make_pair("", "colorRamp");
	m_ids_to_names[OPACITY_SOLID_RAMP_GRADIENT] =
		std::make_pair("", "opacityRamp");
	m_ids_to_names[SOLID_RAMP_BACKGROUND_COLOR] = std::make_pair("", "color_bg");
	m_ids_to_names[SOLID_RAMP_BACKGROUND_COLOR_SHADER] =
		std::make_pair("outColor", "color_bg");
	m_ids_to_names[SOLID_RAMP_REVERSE] = std::make_pair("", "reverse_ramps");
	m_ids_to_names[SOLID_RAMP_OUTSIDE] = std::make_pair("", "outside");
	m_ids_to_names[SOLID_RAMP_SPACE] = std::make_pair("", "space");
	m_ids_to_names[SOLID_RAMP_DISTORTION_INTENSITY] =
		std::make_pair("", "distortion_intensity");
	m_ids_to_names[SOLID_RAMP_DISTORTION_SHADER] =
		std::make_pair("outColor", "distortion");
	m_ids_to_names[SOLID_RAMP_NOISE_INTENSITY] =
		std::make_pair("", "noise_intensity");
	m_ids_to_names[SOLID_RAMP_NOISE_INTENSITY_SHADER] =
		std::make_pair("outColor[0]", "noise_intensity");
	m_ids_to_names[SOLID_RAMP_NOISE_SCALE] = std::make_pair("", "noise_scale");
	m_ids_to_names[SOLID_RAMP_NOISE_SCALE_SHADER] =
		std::make_pair("outColor[0]", "noise_scale");
	m_ids_to_names[SOLID_RAMP_NOISE_STEPPING] =
		std::make_pair("", "noise_stepping");
	m_ids_to_names[SOLID_RAMP_NOISE_STEPPING_SHADER] =
		std::make_pair("outColor[0]", "noise_stepping");
	m_ids_to_names[SOLID_RAMP_NOISE_TIME] = std::make_pair("", "noise_time");
	m_ids_to_names[SOLID_RAMP_NOISE_TIME_SHADER] =
		std::make_pair("outColor[0]", "noise_time");
	m_ids_to_names[SOLID_RAMP_NOISE_HUE] =
		std::make_pair("", "noise_hue_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_HUE_SHADER] =
		std::make_pair("outColor[0]", "noise_hue_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_SATURATION] =
		std::make_pair("", "noise_saturation_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_SATURATION_SHADER] =
		std::make_pair("outColor[0]", "noise_saturation_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_VALUE] =
		std::make_pair("", "noise_value_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_VALUE_SHADER] =
		std::make_pair("outColor[0]", "noise_value_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_OPACITY] =
		std::make_pair("", "noise_opacity_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "noise_opacity_variation");
	m_ids_to_names[SOLID_RAMP_NOISE_LAYERS_LAYERS] = std::make_pair("", "layers");
	m_ids_to_names[SOLID_RAMP_NOISE_LAYERS_PERSISTENCE] =
		std::make_pair("", "layer_persistence");
	m_ids_to_names[SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_SHADER] =
		std::make_pair("outColor[0]", "layer_persistence");
	m_ids_to_names[SOLID_RAMP_NOISE_LAYERS_SCALE] =
		std::make_pair("", "layer_scale");
	m_ids_to_names[SOLID_RAMP_NOISE_LAYERS_SCALE_SHADER] =
		std::make_pair("outColor[0]", "layer_scale");
	m_ids_to_names[SOLID_RAMP_ADJUST_INVERT] = std::make_pair("", "invert");
}
