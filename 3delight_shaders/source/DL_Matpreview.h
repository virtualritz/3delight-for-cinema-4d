#pragma once

#include "c4d.h"

// Change the preview scene of "material" to the 3delight default
void DL_SetDefaultMatpreview(BaseMaterial* material);
