#pragma once

#include "DL_HookPlugin.h"

class DelightUVCoord : public DL_HookPlugin
{
public:
	void CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser);

private:
	std::string transform_name;
	std::string camera_name;
};
