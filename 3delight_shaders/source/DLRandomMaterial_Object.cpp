#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_RandomMaterial_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_RandomMaterial_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_RANDOM_MATERIAL, doc);
	return true;
}

Bool Register_RnadomMaterial_Object(void)
{
	DL_RandomMaterial_command* new_random_material =
		NewObjClear(DL_RandomMaterial_command);
	RegisterCommandPlugin(DL_RANDOM_MATERIAL_COMMAND,
						  "Random Material"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  0,
						  String("Assign new Random Material"),
						  NewObjClear(DL_RandomMaterial_command));

	if (RegisterCommandPlugin(DL_RANDOM_MATERIAL_SHELF_COMMAND,
							  "Random Material"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  0,
							  String("Assign new Ramdom Material"),
							  new_random_material))
		// Let us know that the material is created from the shelf.
	{
		new_random_material->shelf_used = 1;
	}

	return true;
}
