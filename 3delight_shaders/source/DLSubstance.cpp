#include <algorithm>
#include "DL_Matpreview.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_substance.h"

void eraseSubStr(std::string& mainStr, const std::string& toErase)
{
	// Search for the substring in std::string
	size_t pos = mainStr.find(toErase);

	if (pos != std::string::npos) {
		// If found then erase it from std::string
		mainStr.erase(pos, toErase.length());
	}
}

void AssignTexture(GeListNode* node,
				   BaseMaterial* material,
				   BaseContainer* bc,
				   std::string texture_path,
				   std::string texture)
{
	Filename file = Filename(texture_path.c_str());
	BaseShader* bitmapShader = BaseShader::Alloc(Xbitmap);
	bitmapShader->SetParameter(
		DescID(BITMAPSHADER_FILENAME), file, DESCFLAGS_SET::NONE);
	material->InsertShader(bitmapShader);
	material->Message(MSG_UPDATE);
	material->Update(true, true);

	if (texture == "base_color") {
		bc->SetLink(SUBSTANCE_COLOR_SHADER, bitmapShader);
		bc->SetInt32(SUBSTANCE_COLOR_GROUP_PARAM, TEXTURE_ID);

	} else if (texture == "emissive") {
		bc->SetLink(EMISSIVE_COLOR_SHADER, bitmapShader);
		bc->SetInt32(EMISSIVE_COLOR_GROUP_PARAM, TEXTURE_ID);

	} else if (texture == "metallic") {
		bc->SetLink(SUBSTANCE_METALLIC_SHADER, bitmapShader);
		bc->SetInt32(SUBSTANCE_METALLIC_GROUP_PARAM, TEXTURE_ID);

	} else if (texture == "opacity") {
		bc->SetLink(SUBSTANCE_OPACITY_SHADER, bitmapShader);
		bc->SetInt32(SUBSTANCE_OPACITY_GROUP_PARAM, TEXTURE_ID);

	} else if (texture == "roughness") {
		bc->SetLink(SUBSTANCE_ROUGHNESS_SHADER, bitmapShader);
		bc->SetInt32(SUBSTANCE_ROUGHNESS_GROUP_PARAM, TEXTURE_ID);

	} else if (texture == "specular_level") {
		bc->SetLink(SUBSTANCE_SPECULAR_LEVEL_SHADER, bitmapShader);
		bc->SetInt32(SUBSTANCE_SPECULAR_LEVEL_GROUP_PARAM, TEXTURE_ID);

	} else if (texture == "height") {
		bc->SetLink(BUMP_VALUE, bitmapShader);
	}
}

/*
        Function to check if the name in any of the suffixes
        is substring of the given name.
*/
bool exists(std::vector<std::string> suffixes, std::string& o_name)
{
	int ssize = (int) suffixes.size();

	for (int i = 0; i < ssize; i++) {
		if (o_name.find(suffixes[i]) != std::string::npos) {
			eraseSubStr(o_name, suffixes[i]);
			return true;
		}
	}

	return false;
}

class DL_Substance : public MaterialData
{
	INSTANCEOF(DL_Substance, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Substance);
	}
};

Bool DL_Substance::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseShader* const shader = BaseShader::Alloc(Xbrick);
	const Float step = 360;
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	Vector substance_color = HSVToRGB(Vector(38.182 / step, 0, 0.5));
	data->SetVector(SUBSTANCE_COLOR, substance_color);
	data->SetFloat(SUBSTANCE_ROUGHNESS, 0.3);
	data->SetFloat(SUBSTANCE_SPECULAR_LEVEL, 0.5);
	data->SetFloat(SUBSTANCE_METALLIC, 0);
	data->SetFloat(SUBSTANCE_OPACITY, 1);
	data->SetLink(SUBSTANCE_COLOR_SHADER, (C4DAtomGoal*) shader);
	Vector emissive_color = HSVToRGB(Vector(39.231 / step, 0, 0));
	data->SetVector(EMISSIVE_COLOR, emissive_color);
	data->SetFloat(EMISSIVE_INTENSITY, 1.0);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetLink(BUMP_VALUE, nullptr);
	data->SetFloat(BUMP_INTENSITY, 1);
	return true;
}

Bool DL_Substance::GetDDescription(GeListNode* node,
								   Description* description,
								   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_SUBSTANCE);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(SUBSTANCE_COLOR_GROUP_PARAM,
						SUBSTANCE_COLOR,
						SUBSTANCE_COLOR_SHADER,
						SUBSTANCE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSTANCE_ROUGHNESS_GROUP_PARAM,
						SUBSTANCE_ROUGHNESS,
						SUBSTANCE_ROUGHNESS_SHADER,
						SUBSTANCE_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSTANCE_SPECULAR_LEVEL_GROUP_PARAM,
						SUBSTANCE_SPECULAR_LEVEL,
						SUBSTANCE_SPECULAR_LEVEL_SHADER,
						SUBSTANCE_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSTANCE_METALLIC_GROUP_PARAM,
						SUBSTANCE_METALLIC,
						SUBSTANCE_METALLIC_SHADER,
						SUBSTANCE_METALLIC_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SUBSTANCE_OPACITY_GROUP_PARAM,
						SUBSTANCE_OPACITY,
						SUBSTANCE_OPACITY_SHADER,
						SUBSTANCE_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(EMISSIVE_COLOR_GROUP_PARAM,
						EMISSIVE_COLOR,
						EMISSIVE_COLOR_SHADER,
						EMISSIVE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Substance::Message(GeListNode* node, Int32 type, void* data)
{
	BaseContainer* bc = ((BaseMaterial*) node)->GetDataInstance();
	BaseMaterial* material = (BaseMaterial*) node;
	std::vector<std::string> suffixes{ "base_color", "emissive", "metallic",
									   "normal", "opacity", "height",
									   "roughness", "specular_level" };

	switch (type) {
	case MSG_DESCRIPTION_COMMAND: {
		DescriptionCommand* dc = (DescriptionCommand*) data;

		if (dc->_descId[0].id == ASSIGN_TEXTURES) {
			Filename loadFile;

			// open a file selector dialog to open a file
			if (loadFile.FileSelect(
						FILESELECTTYPE::IMAGES, FILESELECT::LOAD, "Open"_s)) {
				// Get only the suffix of the file (png,jpg etc)
				std::string suffix = loadFile.GetSuffix().GetCStringCopy();
				suffix = "." + suffix;
				// Gets the name of the file without suffix
				loadFile.ClearSuffix();
				std::string file_name = loadFile.GetFileString().GetCStringCopy();
				// Gets the directory of the file including the file in it.
				std::string texturepath = loadFile.GetString().GetCStringCopy();
				// converts the name of the file to lower characters. Not case
				// sensitive
				/*for (int i = 0; i < (int)file_name.length(); ++i)
						                        {
						                                file_name[i] =
						   static_cast<char>(tolower(file_name[i]));
						                        }*/
				std::transform(
					file_name.begin(),
					file_name.end(),
					file_name.begin(),
					[](int ch) -> char { return static_cast<char>(::tolower(ch)); });

				if (exists(suffixes, file_name)) {
					bc->SetLink(SUBSTANCE_COLOR_SHADER, nullptr);
					bc->SetLink(SUBSTANCE_METALLIC_SHADER, nullptr);
					bc->SetLink(SUBSTANCE_OPACITY_SHADER, nullptr);
					bc->SetLink(SUBSTANCE_ROUGHNESS_SHADER, nullptr);
					bc->SetLink(SUBSTANCE_SPECULAR_LEVEL, nullptr);
					bc->SetLink(EMISSIVE_COLOR_SHADER, nullptr);
					bc->SetLink(BUMP_VALUE, nullptr);
					// Get Only the name without the substance
					// type(color,metal). Already done in the exists function.
					// file_name was passed by reference. if it was
					// brick_lumpy_Base_Color now it will be brick_lumpy_.
					eraseSubStr(texturepath, loadFile.GetFileString().GetCStringCopy());
					int ssize = (int) suffixes.size();

					for (int i = 0; i < ssize; i++) {
						// This will have the filenames of all possible
						// subbstance that can be found on the current directory
						// Ex. for brick_lumpy_ it will check
						// brick_lumpy_metallic, brick_lumpy_normal and all the
						// others.
						std::string temp_file =
							texturepath + file_name + suffixes[i] + suffix;
						Filename file = Filename(temp_file.c_str());

						if (GeFExist(file)) {
							AssignTexture(node, material, bc, temp_file, suffixes[i]);
						}
					}
				}
			}
		}

		break;
	}
	}

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_SUBSTANCE_COLOR:
			FillPopupMenu(bc, dp, SUBSTANCE_COLOR_GROUP_PARAM);
			break;

		case POPUP_SUBSTANCE_ROUGHNESS:
			FillPopupMenu(bc, dp, SUBSTANCE_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_SUBSTANCE_SPECULAR_LEVEL:
			FillPopupMenu(bc, dp, SUBSTANCE_SPECULAR_LEVEL_GROUP_PARAM);
			break;

		case POPUP_SUBSTANCE_METALLIC:
			FillPopupMenu(bc, dp, SUBSTANCE_METALLIC_GROUP_PARAM);
			break;

		case POPUP_SUBSTANCE_OPACITY:
			FillPopupMenu(bc, dp, SUBSTANCE_OPACITY_GROUP_PARAM);
			break;

		case POPUP_EMISSIVE_COLOR:
			FillPopupMenu(bc, dp, EMISSIVE_COLOR_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Substance::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Substance::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLSubstance(void)
{
	return RegisterMaterialPlugin(DL_SUBSTANCE,
								  "Substance"_s,
								  PLUGINFLAG_HIDE,
								  DL_Substance::Alloc,
								  "DL_Substance"_s,
								  0);
}
