#include "NSIExportMaterial.h"
#include "3DelightEnvironment.h"
#include "DLAOVsColor.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "dl_aov_group.h"
#include "nsi.hpp"

#define DL_AOV_GROUP 1055640
// Link ID's
enum {
	TOP_LAYER_LINK = 105,
	MIDDLE_LAYER_LINK = 113,
	BOTTOM_LAYER_LINK = 121,
	INPUT_MATERIAL1 = 3,
	INPUT_MATERIAL2 = 10,
	INPUT_MATERIAL3 = 17,
	INPUT_MATERIAL4 = 24,
	INPUT_MATERIAL5 = 31,
	TOON_PHYSICAL_LAYER = 110,
	SILHOUETTES_WIDTH = 220,
	FOLDS_WIDTH = 244,
	CREASES_WIDTH = 267,
	OBJECTS_WIDTH = 285,
	TEXTURE_WIDTH_ATTR = 308,
};

/*
        NSI_Export_Material Translater class is the class for exporting C4D
   standard materials to NSI materials. This class with it's functions will only
   be executed as many times as the number of materials created. So one time for
   each material. CreateNSINodes() function will create the NSI shader for the
   current material with a unique name each time there is a new material and
   connect it with the OSL shader containing all the materials attributes
        ConnectNSINodes() will check for the materials' textures if there is
   used any shader on that specific material or not. if there is we find which
   of the materials' parameter has used that shader and then use nsiConnect to
   connect that shader with it's material and parameter
*/

// Convert Cinema4D Interpolation to the corresponding 3Delight one.
int C4DToDelightInterpolation(int c4d_interp)
{
	if (c4d_interp == GRADIENT_INTERPOLATION_NONE) {
		return 0;

	} else if (c4d_interp == GRADIENT_INTERPOLATION_LINEARKNOT) {
		return 1;
	}

	return 2;
}

void NSI_Export_Material::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string m_material_handle = (std::string) Handle + std::string("shader");
	std::string m_material_attributes = (std::string) Handle;
	ctx.Create(m_material_handle, "shader");
	ctx.Create(m_material_attributes, "attributes");
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string _c_shaderpath = delightpath + (std::string) "/cinema4d" + (std::string) "/osl" + (std::string) "/Main_Shader.oso";

	if (m_ids_to_names.size() > 0) {
		_c_shaderpath = (m_ids_to_names[0].second);
	}

	ctx.SetAttribute(
		m_material_handle,
		NSI::StringArg("shaderfilename", std::string(&_c_shaderpath[0])));
	// parser->SetAssociatedHandle((BaseList2D*)C4DNode,
	// m_material_attributes.c_str()); aovGroup Export
	BaseList2D* baselist = (BaseList2D*) C4DNode;
	BaseContainer* data = baselist->GetDataInstance();
	BaseList2D* shader = data->GetLink(DL_AOV_GROUP, doc);
	BaseContainer* aovs_data = shader->GetDataInstance();

	if (shader) {
		const CustomDataType* dt =
			aovs_data->GetCustomDataType(DL_CUSTOM_COLORS, ID_CUSTOMDATATYPE_AOV);
		// ApplicationOutput(shader->GetName());
		std::string shader_handle = parser->GetHandleName(shader);
		delightpath = DelightEnv::getDelightEnvironment();
		std::string aov_group_path =
			delightpath + (std::string) "/osl" + (std::string) "/dlAOVGroup.oso";
		iCustomDataTypeCOLORAOV* aov_data = (iCustomDataTypeCOLORAOV*) dt;

		if (!aov_data) {
			aov_data = NewObjClear(iCustomDataTypeCOLORAOV);
		}

		ctx.Create(shader_handle, "shader");
		ctx.SetAttribute(shader_handle,
						 NSI::StringArg("shaderfilename", aov_group_path));
		// This will store only the layers that will be displayed
		maxon::BaseArray<float> color_aov;
		maxon::BaseArray<char*> name_aov;
		int layers_to_render = 0;

		for (int i = 0; i < aov_data->m_row_id.GetCount(); i++) {
			if (aov_data->aov_textName[i].IsEqual(""_s) || aov_data->aov_checked[i] == FALSE) {
				continue;
			}

			// ApplicationOutput("Name @",aov_data->aov_textName[i]);
			(void) color_aov.Append(aov_data->aov_color[i].x);
			(void) color_aov.Append(aov_data->aov_color[i].y);
			(void) color_aov.Append(aov_data->aov_color[i].z);
			String aov_name = (String) aov_data->aov_textName[i];
			(void) name_aov.Append(aov_name.GetCStringCopy());
			layers_to_render++;
		}

		if (layers_to_render != 0) {
			NSI::ArgumentList args;
			args.Add(
				NSI::Argument::New("colorAOVNames")
				->SetArrayType(NSITypeString, layers_to_render)
				->CopyValue(&name_aov[0], layers_to_render * sizeof(&name_aov[0])));
			args.Add(
				NSI::Argument::New("colorAOVValues")
				->SetArrayType(NSITypeColor, layers_to_render)
				->CopyValue(&color_aov[0], layers_to_render * sizeof(float) * 3));
			ctx.SetAttribute(shader_handle, args);
			aovs_data->SetBool(IS_USED, TRUE);
		}
	}
}

void NSI_Export_Material::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	std::string m_material_handle = (std::string) Handle + std::string("shader");
	std::string m_material_attributes = (std::string) Handle;
	BaseMaterial* material = (BaseMaterial*) C4DNode;
	BaseContainer* material_container = material->GetDataInstance();
	Int32 id;
	GeData* data = nullptr;
	BrowseContainer browse(material_container);
	ctx.Connect(m_material_handle, "", m_material_attributes, "surfaceshader");
	ctx.Connect("*uvCoord", "o_outUV", m_material_handle, "uvCoord");

	while (browse.GetNext(&id, &data)) {
		if (data->GetType() != DA_ALIASLINK) {
			continue;
		}

		BaseList2D* shader = data->GetLink(doc);

		if (!shader) {
			continue;
		}

		std::string osl_parameter_name;
		std::string osl_source_attr;

		if (m_ids_to_names.count(id) == 1) {
			osl_parameter_name = m_ids_to_names[id].second;
			osl_source_attr = m_ids_to_names[id].first;

		} else {
			osl_parameter_name = "_" + std::to_string(id);
			std::string use_shader =
				"shader" + osl_parameter_name; // pass this parameter to osl to check if
			// shader is loaded or not.
			std::vector<float> col = { 1, 1, 1 };
			ctx.SetAttribute(m_material_handle,
							 NSI::ColorArg(osl_parameter_name, &col[0]));
			ctx.SetAttribute(m_material_handle, NSI::IntegerArg(use_shader, 1));
			osl_source_attr = "outColor";
		}

		// Connect directly to the material.
		if (material->GetType() == DL_LAYERED || material->GetType() == DL_RANDOM_MATERIAL || material->GetType() == DL_TOON)
			switch (id) {
			case TOP_LAYER_LINK:
			case MIDDLE_LAYER_LINK:
			case BOTTOM_LAYER_LINK:
			case INPUT_MATERIAL1:
			case INPUT_MATERIAL2:
			case INPUT_MATERIAL3:
			case INPUT_MATERIAL4:
			case INPUT_MATERIAL5:
			case TOON_PHYSICAL_LAYER: {
				std::string link_shader = parser->GetHandleName(shader) + "shader";
				ctx.Connect(link_shader,
							osl_source_attr,
							m_material_handle,
							osl_parameter_name);
			}

			default:
				std::string link_shader = parser->GetHandleName(shader);
				ctx.Connect(link_shader,
							osl_source_attr,
							m_material_handle,
							osl_parameter_name);
				break;

			} else {
			std::string link_shader = parser->GetHandleName(shader);
			ctx.Connect(
				link_shader, osl_source_attr, m_material_handle, osl_parameter_name);
		}

		//#ifdef VERBOSE
		//		ApplicationOutput("Material @ Parameter ID @, Shader @",
		// m_material_handle.c_str(), id, link_shader.c_str()); #endif // VERBOSE
	}
}

void NSI_Export_Material::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	std::string m_material_handle = (std::string) Handle + std::string("shader");
	std::string m_material_attributes = (std::string) Handle;
	NSI::Context& ctx(parser->GetContext());
	BaseMaterial* material = (BaseMaterial*) C4DNode;
	BaseContainer* material_container = material->GetDataInstance();
	Int32 id;
	GeData* data = nullptr;
	BrowseContainer browse(material_container);
	NSI::ArgumentList args;
	std::string osl_parameter_name;

	while (browse.GetNext(&id, &data)) { // loop through the values
		if (m_ids_to_names.count(id) == 1) {
			osl_parameter_name = m_ids_to_names[id].second;
		}

		/*
			                Here we avoid setting non existent parameters to
			   3Delight materials. For supporting Cinema4D starndard material we are
			   using _+parameter_id. There are  additional IDs like this on our
			   3Delight Materials, which are not part of the respective osl shader,
			   so we just ignore them to avoid warning messages. Warning messages
			   will show when using C4D standard material since we are only
			                supporting some of it's parameters.
			        */
		else if (is_3delight_material) {
			continue;

		} else {
			osl_parameter_name = "_" + std::to_string(id);
		}

		switch (data->GetType()) {
		case DA_LONG: { // Integer data type
			Int32 value = data->GetInt32();
			args.Add(new NSI::IntegerArg(osl_parameter_name, value));
			break;
		}

		case DA_REAL: { // Float data type
			float float_value = (float) data->GetFloat();

			if (material->GetType() == DL_TOON && (id == FOLDS_WIDTH || id == SILHOUETTES_WIDTH || id == CREASES_WIDTH || id == OBJECTS_WIDTH || id == TEXTURE_WIDTH_ATTR)) {
				float_value *= 0.01;
			}

			args.Add(new NSI::FloatArg(osl_parameter_name, float_value));
			break;
		}

		case DA_VECTOR: { // Vector data type
			Vector c4d_vector_value = toLinear(data->GetVector(), doc);
			const float vector_val[3] = { (float) c4d_vector_value.x,
										  (float) c4d_vector_value.y,
										  (float) c4d_vector_value.z
										};
			args.Add(new NSI::ColorArg(osl_parameter_name, &vector_val[0]));
			break;
		}

		case CUSTOMDATATYPE_GRADIENT: {
			GeData gradient_data = material_container->GetData(id);
			Gradient* gradient =
				(Gradient*) gradient_data.GetCustomDataType(CUSTOMDATATYPE_GRADIENT);
			int knot_count = gradient->GetKnotCount();
			float* knots = new float[knot_count];
			float* colors = new float[knot_count * 3];
			int* interpolations = new int[knot_count];

			for (int i = 0; i < knot_count; i++) {
				GradientKnot Knot = gradient->GetKnot(i);
				knots[i] = (float) Knot.pos;
				Vector KnotColor = toLinear((Knot.col), doc);
				colors[i * 3] = (float) KnotColor.x;
				colors[i * 3 + 1] = (float) KnotColor.y;
				colors[i * 3 + 2] = (float) KnotColor.z;
				interpolations[i] = C4DToDelightInterpolation(Knot.interpolation);
			}

			args.Add(NSI::Argument::New(osl_parameter_name + "_Knots")
					 ->SetArrayType(NSITypeFloat, knot_count)
					 ->CopyValue((float*) &knots[0], knot_count * sizeof(float)));
			args.Add(
				NSI::Argument::New(osl_parameter_name + "_Colors")
				->SetArrayType(NSITypeColor, knot_count)
				->CopyValue((float*) &colors[0], knot_count * sizeof(float) * 3));
			args.Add(
				NSI::Argument::New(osl_parameter_name + "_Interpolation")
				->SetArrayType(NSITypeInteger, knot_count)
				->CopyValue((int*) &interpolations[0], knot_count * sizeof(int)));
		}

		case DA_ALIASLINK: { // texture
		}

		default:
			break;
		}
	}

	if (args.size() > 0) {
		ctx.SetAttribute(m_material_handle, args);
	}
}
