#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_glass.h"

class DL_Glass : public MaterialData
{
	INSTANCEOF(DL_Glass, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Glass);
	}
};

Bool DL_Glass::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	Vector reflection_color(0.6, 0.6, 0.6);
	data->SetVector(REFLECTION_COLOR, reflection_color);
	data->SetFloat(REFLECTION_ROUGHNESS, 0);
	data->SetFloat(REFLECTION_IOR, 1.3);
	data->SetBool(REFLECTION_THIN_FILM, 0);
	data->SetFloat(REFLECTION_FILM_THICKNESS, 0.250);
	data->SetFloat(REFLECTION_FILM_IOR, 1.5);
	Vector refraction_color(1, 1, 1);
	data->SetVector(REFRACTION_COLOR, refraction_color);
	data->SetFloat(REFRACTION_ROUGHNESS, 0);
	data->SetFloat(REFRACTION_IOR, 1.3);
	data->SetBool(VOLUMETRIC_ENABLE, false);
	data->SetFloat(VOLUMETRIC_DENSITY, 1.0);
	Vector volumetric_scattering(0, 0, 0);
	data->SetVector(VOLUMETRIC_SCATTERING, volumetric_scattering);
	Vector volumetric_transparency(1, 1, 1);
	data->SetVector(VOLUMETRIC_TRANSPARENCY, volumetric_transparency);
	Vector incandescence_color(0, 0, 0);
	data->SetVector(INCANDESCENCE_COLOR, incandescence_color);
	data->SetFloat(INCANDESCENCE_INTENSITY, 1);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	return true;
}

Bool DL_Glass::GetDEnabling(GeListNode* node,
							const DescID& id,
							const GeData& t_data,
							DESCFLAGS_ENABLE flags,
							const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case REFLECTION_FILM_THICKNESS:
	case POPUP_REFLECTION_FILM_THICKNESS:
	case REFLECTION_FILM_THICKNESS_SHADER:
	case REFLECTION_FILM_IOR:
	case POPUP_REFLECTION_FILM_IOR:
	case REFLECTION_FILM_IOR_SHADER:
		return dldata->GetBool(REFLECTION_THIN_FILM);

	case VOLUMETRIC_DENSITY:
	case VOLUMETRIC_SCATTERING:
	case VOLUMETRIC_TRANSPARENCY:
		return dldata->GetBool(VOLUMETRIC_ENABLE) == true;
		break;

	default:
		break;
	}

	return true;
}

Bool DL_Glass::GetDDescription(GeListNode* node,
							   Description* description,
							   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_GLASS);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*)node)->GetDataInstance();
	HideAndShowTextures(REFLECTION_COLOR_GROUP_PARAM, REFLECTION_COLOR, REFLECTION_COLOR_SHADER, REFLECTION_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFLECTION_ROUGHNESS_GROUP_PARAM, REFLECTION_ROUGHNESS, REFLECTION_ROUGHNESS_SHADER, REFLECTION_ROUGHNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFLECTION_IOR_GROUP_PARAM, REFLECTION_IOR, REFLECTION_IOR_SHADER, REFLECTION_IOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFLECTION_FILM_THICKNESS_GROUP_PARAM, REFLECTION_FILM_THICKNESS, REFLECTION_FILM_THICKNESS_SHADER, REFLECTION_FILM_THICKNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFLECTION_FILM_IOR_GROUP_PARAM, REFLECTION_FILM_IOR, REFLECTION_FILM_IOR_SHADER, REFLECTION_FILM_IOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFRACTION_COLOR_GROUP_PARAM, REFRACTION_COLOR, REFRACTION_COLOR_SHADER, REFRACTION_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFRACTION_ROUGHNESS_GORUP_PARAM, REFRACTION_ROUGHNESS, REFRACTION_ROUGHNESS_SHADER, REFRACTION_ROUGHNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(REFRACTION_IOR_GROUP_PARAM, REFRACTION_IOR, REFRACTION_IOR_SHADER, REFRACTION_IOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(INCANDESCENCE_COLOR_GROUP_PARAM, INCANDESCENCE_COLOR, INCANDESCENCE_COLOR_SHADER, INCANDESCENCE_COLOR_SHADER_TEMP, node, description, dldata);
	return TRUE;
}

Bool DL_Glass::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_REFLECTION_COLOR:
			FillPopupMenu(dldata, dp, REFLECTION_COLOR_GROUP_PARAM);
			break;

		case POPUP_REFLECTION_ROUGHNESS:
			FillPopupMenu(dldata, dp, REFLECTION_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_REFLECTION_IOR:
			FillPopupMenu(dldata, dp, REFLECTION_IOR_GROUP_PARAM);
			break;

		case POPUP_REFLECTION_FILM_THICKNESS:
			FillPopupMenu(dldata, dp, REFLECTION_FILM_THICKNESS_GROUP_PARAM);
			break;

		case POPUP_REFLECTION_FILM_IOR:
			FillPopupMenu(dldata, dp, REFLECTION_FILM_IOR_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_COLOR:
			FillPopupMenu(dldata, dp, REFRACTION_COLOR_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_ROUGHNESS:
			FillPopupMenu(dldata, dp, REFRACTION_ROUGHNESS_GORUP_PARAM);
			break;

		case POPUP_REFRACTION_IOR:
			FillPopupMenu(dldata, dp, REFRACTION_IOR_GROUP_PARAM);
			break;

		case POPUP_INCANDESCENCE_COLOR:
			FillPopupMenu(dldata, dp, INCANDESCENCE_COLOR_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Glass::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Glass::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLGlass(void)
{
	RegisterMaterialPlugin(
		DL_GLASS, "Glass"_s, PLUGINFLAG_HIDE, DL_Glass::Alloc, "Dl_glass"_s, 0);
	return TRUE;
}
