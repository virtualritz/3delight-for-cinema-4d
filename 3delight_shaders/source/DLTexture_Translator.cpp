#include "DLTexture_Translator.h"
#include "3DelightEnvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "dl_texture.h"
#include "nsi.hpp"

Delight_Texture::Delight_Texture()
{
	is_3delight_shader = true;
	// m_ids_to_names[TEXTURE_FILE] = std::make_pair("", "textureFile");
	m_ids_to_names[TEXTURE_DEFAULT_COLOR] = std::make_pair("", "defaultColor");
	m_ids_to_names[TEXTURE_DEFAULT_COLOR_SHADER] =
		std::make_pair("outColor", "defaultColor");
	m_ids_to_names[COLOR_CORRECT_TOON] = std::make_pair("", "tonalAdjust");
	m_ids_to_names[COLOR_CORRECT_COL_GAIN] = std::make_pair("", "colorGain");
	m_ids_to_names[COLOR_CORRECT_COL_GAIN_SHADER] =
		std::make_pair("outColor", "colorGain");
	m_ids_to_names[COLOR_CORRECT_COL_OFFSET] = std::make_pair("", "colorOffset");
	m_ids_to_names[COLOR_CORRECT_COL_OFFSET_SHADER] =
		std::make_pair("outColor", "colorOffset");
	m_ids_to_names[COLOR_CORRECT_ALPHA_GAIN] = std::make_pair("", "alphaGain");
	m_ids_to_names[COLOR_CORRECT_ALPHA_GAIN_SHADER] =
		std::make_pair("outColor[0]", "alphaGain");
	m_ids_to_names[COLOR_CORRECT_ALPHA_OFFSET] =
		std::make_pair("", "alphaOffset");
	m_ids_to_names[COLOR_CORRECT_ALPHA_OFFSET_SHADER] =
		std::make_pair("outColor[0]", "alphaOffset");
	m_ids_to_names[COLOR_CORRECT_IS_ALPHA_LUMIN] =
		std::make_pair("", "alphaIsLuminance");
	m_ids_to_names[COLOR_CORRECT_INVERT] = std::make_pair("outColor", "invert");
	m_ids_to_names[TILE_REMOVAL_BOOL] = std::make_pair("", "tileRemoval");
	m_ids_to_names[TILE_REMOVAL_SOFTNESS] = std::make_pair("", "tileSoftness");
	m_ids_to_names[TILE_REMOVAL_SOFTNESS_SHADER] =
		std::make_pair("outColor[0]", "tileSoftness");
	m_ids_to_names[TILE_REMOVAL_OFFSET] = std::make_pair("", "tileOffset");
	m_ids_to_names[TILE_REMOVAL_OFFSET_SHADER] =
		std::make_pair("outColor[0]", "tileOffset");
	m_ids_to_names[TILE_REMOVAL_ROTATION] = std::make_pair("", "tileRotation");
	m_ids_to_names[TILE_REMOVAL_ROTATION_SHADER] =
		std::make_pair("outColor[0]", "tileRotation");
	m_ids_to_names[TILE_REMOVAL_SCALE] = std::make_pair("", "tileScale");
	m_ids_to_names[TILE_REMOVAL_SCALE_SHADER] =
		std::make_pair("outColor[0]", "tileScale");
	m_ids_to_names[USE_IMAGE_SEQUENCE] = std::make_pair("", "useImageSequence");
	m_ids_to_names[IMAGE_SEQUENCE_FRAME] = std::make_pair("", "frame");
	m_ids_to_names[IMAGE_SEQUENCE_FRAME_OFFSET] =
		std::make_pair("", "frameOffset");
	m_ids_to_names[FILTER_VAL] = std::make_pair("", "filter");
	m_ids_to_names[BLUR_VAL] = std::make_pair("", "blur");
}

bool checkFrame(std::string file)
{
	std::string index = strrchr(&file[0], '.');
	// Getting filename string without the extension
	std::string filename = file.substr(0, file.size() - index.size());

	// checking if frame padding exists and is replaced by #
	if (filename[filename.size() - 1] == '#') {
		return true;
	}

	return false;
}

void Delight_Texture::CreateNSINodes(const char* Handle,
									 const char* ParentTransformHandle,
									 BaseList2D* C4DNode,
									 BaseDocument* doc,
									 DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	m_handle = (std::string) Handle;
	ctx.Create(m_handle, "shader");
	BaseList2D* baselist = (BaseList2D*) C4DNode;
	BaseContainer* data = baselist->GetDataInstance();
	AutoAlloc<Description> description;

	if (description == nullptr) {
		return;
	}

	if (!baselist->GetDescription(description, DESCFLAGS_DESC::NONE)) {
		return;
	}

	// Get the color space string name used from the dropdown.
	// From dropdown you can only take the ID of the selected item instead of
	// its name.
	std::string color_space_text = "";
	int color_space = data->GetInt32(TEXTURE_COLOR_SPACE);

	switch (color_space) {
	case TEXTURE_COLOR_SPACE_SRGB:
		color_space_text = "sRGB";
		break;

	case TEXTURE_COLOR_SPACE_LINEAR:
		color_space_text = "Linear";
		break;

	case TEXTURE_COLOR_SPACE_REC:
		color_space_text = "Rec. 709";
		break;

	default:
		break;
	}

	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath = delightpath + (std::string) "/cinema4d" + (std::string) "/osl" + (std::string) "/dlTexture.oso";
	String texturename = data->GetString(TEXTURE_FILE);
	Bool use_sequence = data->GetBool(USE_IMAGE_SEQUENCE);
	Int32 dl_frame = data->GetInt32(IMAGE_SEQUENCE_FRAME);
	BaseTime time = doc->GetTime();
	Int32 offset = data->GetInt32(IMAGE_SEQUENCE_FRAME_OFFSET);
	Int32 start_frame = dl_frame + offset;
	std::string texture = "";

	if (texturename.IsPopulated()) {
		texture = StringToStdString(texturename);

		if (use_sequence) {
			if (checkFrame(texture)) {
				std::string frame_string = std::to_string(start_frame);
				Replace(texture, "#", frame_string);
				int i = 0;
				bool exists = FALSE;

				// Taking in consideration that frame padding can have up to
				// three 0 before the current frame to be rendered
				while (i++ < 4) {
					if (file_exists(texture)) {
						exists = TRUE;
						break;

					} else {
						Replace(texture, frame_string, "0" + frame_string);
						frame_string = "0" + frame_string;
					}
				}
			}
		}
	}

	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", std::string(&m_shaderpath[0])));
	args.Add(new NSI::StringArg("textureFile", texture));
	args.Add(new NSI::StringArg("textureFile.meta.colorspace",
								color_space_text.c_str()));
	ctx.SetAttribute(m_handle, args);
}
