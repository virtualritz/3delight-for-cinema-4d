#pragma once
#include <iostream>
#include <vector>

class DLCustomAOVS
{
public:
	std::vector<std::string> ColorAOVs;
	std::vector<std::string> FloatAOVs;
	std::string name;
	static int idx;
	int selected_idx;
	DLCustomAOVS(std::string name);
	DLCustomAOVS();
	static std::vector<DLCustomAOVS> customAOV;
};
