#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Skin_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Skin_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_SKIN, doc);
	return true;
}

Bool Register_Skin_Object(void)
{
	DL_Skin_command* new_skin = NewObjClear(DL_Skin_command);
	RegisterCommandPlugin(DL_SKIN_COMMAND,
						  "Skin"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("dlSkin_200.png"_s),
						  String("Assign new Skin"),
						  NewObjClear(DL_Skin_command));

	if (RegisterCommandPlugin(DL_SKIN_SHELF_COMMAND,
							  "Skin"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlSkin_200.png"_s),
							  String("Assign new Skin Material"),
							  new_skin)) {
		new_skin->shelf_used = 1;
	}

	return true;
}