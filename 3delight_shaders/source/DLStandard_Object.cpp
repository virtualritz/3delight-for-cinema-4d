#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Standard_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Standard_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_STANDARD, doc);
	return true;
}

Bool Register_Standard_Object(void)
{
	DL_Standard_command* new_standard = NewObjClear(DL_Standard_command);
	RegisterCommandPlugin(DL_STANDARD_COMMAND,
						  "Standard"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  0,
						  String("Assign new Standard"_s),
						  NewObjClear(DL_Standard_command));

	if (RegisterCommandPlugin(DL_STANDARD_SHELF_COMMAND,
							  "Standard"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  0,
							  String("Assign new Standard Material"_s),
							  new_standard)) {
		new_standard->shelf_used = 1;
	}

	return true;
}
