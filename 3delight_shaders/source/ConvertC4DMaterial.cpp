#include <sstream>
#include "DL_TypeConversions.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "dl_principled.h"
#include "nsi.hpp"

using namespace std;
#define REFRACTION_COLOR 133
class C4DMaterialConvert : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

/*
        Used to Convert Cinema4D Standard Materials to 3Delight Materials.
        As there can not be a perfect conversion due to the differences that
        3Delight materials have compared to Cinema4D material, we are converting
        some of the main paramters and channels of the material, such as color,
        brightness, displacement, reflections, luminance etc. There can be more
        improvements in the future.
*/
Bool C4DMaterialConvert::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	AutoAlloc<AtomArray> selected_materials;
	doc->GetActiveMaterials(*selected_materials);

	// Getting all the selected or active materials
	if (selected_materials) {
		Int32 mat_count = selected_materials->GetCount();

		for (int i = 0; i < mat_count; i++) {
			Material* material = (Material*) selected_materials->GetIndex(i);

			if (material) {
				BaseContainer* c4d_mat_data = material->GetDataInstance();
				Material* insert_material = NULL;
				insert_material = (Material*) BaseMaterial::Alloc(DL_PRINCIPLED);

				if (material->GetChannelState(CHANNEL_COLOR)) {
					Vector color = material->GetAverageColor(CHANNEL_COLOR);
					insert_material = (Material*) BaseMaterial::Alloc(DL_PRINCIPLED);

					if (!insert_material) {
						return false;
					}

					BaseContainer* data =
						((BaseMaterial*) insert_material)->GetDataInstance();
					data->SetVector(BASE_LAYER_COLOR, color);
					BaseList2D* shader_color =
						c4d_mat_data->GetLink(MATERIAL_COLOR_SHADER, doc);

					if (shader_color) {
						/*TO DO:
									                                Show texture instead of
									   color value if a shader is used on the Material Color
									   Shader parameter.
									                                HideAndShowTextures(BASE_COLOR_GROUP_PARAM,
									   BASE_LAYER_COLOR, BASE_LAYER_COLOR_SHADER,
									   BASE_LAYER_COLOR_SHADER_TEMP, node, description,
									   data);
									                        */
						data->SetLink(BASE_LAYER_COLOR_SHADER, shader_color);
					}
				}

				if (material->GetChannelState(CHANNEL_TRANSPARENCY)) {
					Vector color = material->GetAverageColor(CHANNEL_TRANSPARENCY);
					insert_material = (Material*) BaseMaterial::Alloc(DL_GLASS);

					if (!insert_material) {
						return false;
					}

					BaseContainer* data =
						((BaseMaterial*) insert_material)->GetDataInstance();
					data->SetVector(REFRACTION_COLOR, color);
				}

				if (material->GetChannelState(CHANNEL_LUMINANCE)) {
					Vector color = material->GetAverageColor(CHANNEL_LUMINANCE);
					BaseContainer* data;

					if (!insert_material) {
						insert_material = (Material*) BaseMaterial::Alloc(DL_PRINCIPLED);
						data = ((BaseMaterial*) insert_material)->GetDataInstance();
						data->SetVector(BASE_LAYER_COLOR, color);
					}

					data = ((BaseMaterial*) insert_material)->GetDataInstance();
					data->SetVector(INCANDESCENCE_COLOR, color);
					data->SetFloat(INCANDESCENCE_INTENSITY, 1.0);
				}

				if (!material->GetChannelState(CHANNEL_REFLECTION)) {
					BaseContainer* data =
						((BaseMaterial*) insert_material)->GetDataInstance();
					data->SetFloat(BASE_LAYER_SPECULAR_LEVEL, 0.0);
				}

				if (material->GetChannelState(CHANNEL_BUMP)) {
					BaseContainer* data;

					if (!insert_material) {
						insert_material = (Material*) BaseMaterial::Alloc(DL_PRINCIPLED);
						data = ((BaseMaterial*) insert_material)->GetDataInstance();
						data->SetVector(BASE_LAYER_COLOR, Vector(0, 0, 0));
					}

					// GeData bump_shader;
					// material->GetParameter(DescID(MATERIAL_BUMP_SHADER),
					// bump_shader, DESCFLAGS_GET::NONE);
					data = ((BaseMaterial*) insert_material)->GetDataInstance();
					BaseList2D* bump_shader =
						c4d_mat_data->GetLink(MATERIAL_BUMP_SHADER, doc);
					Float bump_strength = c4d_mat_data->GetFloat(MATERIAL_BUMP_STRENGTH);
					data->SetLink(BUMP_VALUE, bump_shader);
					data->SetFloat(BUMP_INTENSITY, bump_strength);
				}

				if (insert_material) {
					insert_material->Update(true, true);
					doc->InsertMaterial(insert_material);
				}
			}
		}
	}

	return true;
}

Bool ConvertC4DMaterial(void)
{
	return RegisterCommandPlugin(C4DMatConvert,
								 "Convert Cinema 4D Material"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 0,
								 String("Convert Cinema 4D Material"_s),
								 NewObjClear(C4DMaterialConvert));
}
