#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_color_correction.h"

class DLColorCorrection : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLColorCorrection);
	}
};

Bool DLColorCorrection::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetVector(INPUT_COLOR, HSVToRGB(Vector(1, 0, 1)));
	data->SetFloat(INPUT_MASK, 1.0);
	data->SetFloat(CORRECTION_GAMMA, 1.0);
	data->SetFloat(CORRECTION_HUE_SHIFT, 0);
	data->SetFloat(CORRECTION_SATURATION, 1.0);
	data->SetFloat(CORRECTION_VIBRANCE, 1.0);
	data->SetFloat(CORRECTION_CONSTRAST, 1.0);
	data->SetFloat(CORRECTION_CONSTRAST_PIVOT, 0.180);
	data->SetFloat(CORRECTION_EXPOSURE, 0);
	data->SetVector(CORRECTION_GAIN, HSVToRGB(Vector(1, 0, 1)));
	data->SetVector(CORRECTION_OFFSET, HSVToRGB(Vector(1, 0, 0)));
	data->SetBool(CORRECTION_INVERT, false);
	return true;
}

Bool DLColorCorrection::GetDDescription(GeListNode* node,
										Description* description,
										DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_COLORCORRECTION);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(INPUT_COLOR_GROUP_PARAM,
						INPUT_COLOR,
						INPUT_COLOR_SHADER,
						INPUT_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_MASK_GROUP_PARAM,
						INPUT_MASK,
						INPUT_MASK_SHADER,
						INPUT_MASK_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_GAMMA_GROUP_PARAM,
						CORRECTION_GAMMA,
						CORRECTION_GAMMA_SHADER,
						CORRECTION_GAMMA_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_HUE_SHIFT_GROUP_PARAM,
						CORRECTION_HUE_SHIFT,
						CORRECTION_HUE_SHIFT_SHADER,
						CORRECTION_HUE_SHIFT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_SATURATION_GROUP_PARAM,
						CORRECTION_SATURATION,
						CORRECTION_SATURATION_SHADER,
						CORRECTION_SATURATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_VIBRANCE_GROUP_PARAM,
						CORRECTION_VIBRANCE,
						CORRECTION_VIBRANCE_SHADER,
						CORRECTION_VIBRANCE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_CONSTRAST_GROUP_PARAM,
						CORRECTION_CONSTRAST,
						CORRECTION_CONSTRAST_SHADER,
						CORRECTION_CONSTRAST_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_CONSTRAST_PIVOT_GROUP_PARAM,
						CORRECTION_CONSTRAST_PIVOT,
						CORRECTION_CONSTRAST_PIVOT_SHADER,
						CORRECTION_CONSTRAST_PIVOT_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_EXPOSURE_GROUP_PARAM,
						CORRECTION_EXPOSURE,
						CORRECTION_EXPOSURE_SHADER,
						CORRECTION_EXPOSURE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_GAIN_GROUP_PARAM,
						CORRECTION_GAIN,
						CORRECTION_GAIN_SHADER,
						CORRECTION_GAIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(CORRECTION_OFFSET_GROUP_PARAM,
						CORRECTION_OFFSET,
						CORRECTION_OFFSET_SHADER,
						CORRECTION_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLColorCorrection::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_INPUT_COLOR:
			FillPopupMenu(dldata, dp, INPUT_COLOR_GROUP_PARAM);
			break;

		case POPUP_INPUT_MASK:
			FillPopupMenu(dldata, dp, INPUT_MASK_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_GAMMA:
			FillPopupMenu(dldata, dp, CORRECTION_GAMMA_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_HUE_SHIFT:
			FillPopupMenu(dldata, dp, CORRECTION_HUE_SHIFT_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_SATURATION:
			FillPopupMenu(dldata, dp, CORRECTION_SATURATION_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_VIBRANCE:
			FillPopupMenu(dldata, dp, CORRECTION_VIBRANCE_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_CONSTRAST:
			FillPopupMenu(dldata, dp, CORRECTION_CONSTRAST_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_CONSTRAST_PIVOT:
			FillPopupMenu(dldata, dp, CORRECTION_CONSTRAST_PIVOT_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_EXPOSURE:
			FillPopupMenu(dldata, dp, CORRECTION_EXPOSURE_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_GAIN:
			FillPopupMenu(dldata, dp, CORRECTION_GAIN_GROUP_PARAM);
			break;

		case POPUP_CORRECTION_OFFSET:
			FillPopupMenu(dldata, dp, CORRECTION_OFFSET_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLColorCorrection::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterColorCorrectionTexture(void)
{
	return RegisterShaderPlugin(DL_COLORCORRECTION,
								"Color Correction"_s,
								0,
								DLColorCorrection::Alloc,
								"dl_color_correction"_s,
								0);
}
