#include "DLFlakes_translator.h"
#include "3DelightEnvironment.h"
#include "dl_flakes.h"

Delight_Flakes::Delight_Flakes()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlFlakes.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[FLAKES_DENSITY] = std::make_pair("", "density");
	m_ids_to_names[FLAKES_DENSITY_SHADER] =
		std::make_pair("outColor[0]", "density");
	m_ids_to_names[FLAKES_SCALE] = std::make_pair("", "scale");
	m_ids_to_names[FLAKES_SCALE_SHADER] = std::make_pair("outColor[0]", "scale");
	m_ids_to_names[FLAKES_RANDOMNESS] = std::make_pair("", "randomness");
	m_ids_to_names[FLAKES_RANDOMNESS_SHADER] =
		std::make_pair("outColor[0]", "randomness");
	m_ids_to_names[FLAKES_LAYERS] = std::make_pair("", "layers");
}
