#include "DL_Primvar_Translator.h"
#include "3DelightEnvironment.h"
#include "DL_TypeConversions.h"
#include "dl_primvar.h"
#include "nsi.hpp"

void DL_PrimvarTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Create the shader node
	ctx.Create(Handle, "shader");
	// std::string shader_handle = (std::string)Handle;
	// ctx.Create(shader_handle, "shader");
	BaseShader* shader = (BaseShader*) C4DNode;
	BaseContainer* data = shader->GetDataInstance();
	std::string varname = StringToStdString(data->GetString(ATTRIBUTE_NAME));
	// Shader file path
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath = delightpath + (std::string) "/cinema4d" + (std::string) "/osl" + (std::string) "/c4d_Primvar.oso";
	ctx.SetAttribute(Handle,
					 (NSI::StringArg("shaderfilename", shaderpath),
					  NSI::StringArg("varname", varname)));
}
