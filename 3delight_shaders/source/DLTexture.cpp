#include "DL_Utilities.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_texture.h"

class DLTexture : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLTexture);
	}
};

Bool DLTexture::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetInt32(TEXTURE_COLOR_SPACE, TEXTURE_COLOR_SPACE_SRGB);
	data->SetVector(TEXTURE_DEFAULT_COLOR, HSVToRGB(Vector(0, 0, 0)));
	data->SetInt32(COLOR_CORRECT_TOON, COLOR_CORRECT_TOON_OFF);
	data->SetVector(COLOR_CORRECT_COL_GAIN, HSVToRGB(Vector(0, 0, 1)));
	data->SetVector(COLOR_CORRECT_COL_OFFSET, HSVToRGB(Vector(0, 0, 0)));
	data->SetFloat(COLOR_CORRECT_ALPHA_GAIN, 1.0);
	data->SetFloat(COLOR_CORRECT_ALPHA_OFFSET, 0);
	data->SetBool(COLOR_CORRECT_IS_ALPHA_LUMIN, FALSE);
	data->SetBool(COLOR_CORRECT_INVERT, FALSE);
	data->SetBool(TILE_REMOVAL_BOOL, FALSE);
	data->SetFloat(TILE_REMOVAL_SOFTNESS, 0.75);
	data->SetFloat(TILE_REMOVAL_OFFSET, 1);
	data->SetFloat(TILE_REMOVAL_ROTATION, 1);
	data->SetFloat(TILE_REMOVAL_SCALE, 0.5);
	data->SetBool(USE_IMAGE_SEQUENCE, FALSE);
	data->SetInt32(IMAGE_SEQUENCE_FRAME, 1);
	data->SetInt32(IMAGE_SEQUENCE_FRAME_OFFSET, 0);
	data->SetFloat(FILTER_VAL, 1);
	data->SetFloat(BLUR_VAL, 0);
	return true;
}

Bool DLTexture::GetDDescription(GeListNode* node,
								Description* description,
								DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_TEXTURE);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(TEXTURE_DEFAULT_COLOR_SHADER_GROUP_PARAM,
						TEXTURE_DEFAULT_COLOR,
						TEXTURE_DEFAULT_COLOR_SHADER,
						TEXTURE_DEFAULT_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	// COLOR CORRECT GROUP
	HideAndShowTextures(COLOR_CORRECT_COL_GAIN_GROUP_PARAM,
						COLOR_CORRECT_COL_GAIN,
						COLOR_CORRECT_COL_GAIN_SHADER,
						COLOR_CORRECT_COL_GAIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COLOR_CORRECT_COL_OFFSET_SHADER_GROUP_PARAM,
						COLOR_CORRECT_COL_OFFSET,
						COLOR_CORRECT_COL_OFFSET_SHADER,
						COLOR_CORRECT_COL_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COLOR_CORRECT_ALPHA_GAIN_SHADER_GROUP_PARAM,
						COLOR_CORRECT_ALPHA_GAIN,
						COLOR_CORRECT_ALPHA_GAIN_SHADER,
						COLOR_CORRECT_ALPHA_GAIN_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COLOR_CORRECT_ALPHA_OFFSET_SHADER_GROUP_PARAM,
						COLOR_CORRECT_ALPHA_OFFSET,
						COLOR_CORRECT_ALPHA_OFFSET_SHADER,
						COLOR_CORRECT_ALPHA_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	// TILE REMOVAL GROUP
	HideAndShowTextures(TILE_REMOVAL_SOFTNESS_SHADER_GROUP_PARAM,
						TILE_REMOVAL_SOFTNESS,
						TILE_REMOVAL_SOFTNESS_SHADER,
						TILE_REMOVAL_SOFTNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_OFFSET_SHADER_GROUP_PARAM,
						TILE_REMOVAL_OFFSET,
						TILE_REMOVAL_OFFSET_SHADER,
						TILE_REMOVAL_OFFSET_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_ROTATION_SHADER_GROUP_PARAM,
						TILE_REMOVAL_ROTATION,
						TILE_REMOVAL_ROTATION_SHADER,
						TILE_REMOVAL_ROTATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TILE_REMOVAL_SOFTNESS_SHADER_GROUP_PARAM,
						TILE_REMOVAL_SCALE,
						TILE_REMOVAL_SCALE_SHADER,
						TILE_REMOVAL_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLTexture::GetDEnabling(GeListNode* node,
							 const DescID& id,
							 const GeData& t_data,
							 DESCFLAGS_ENABLE flags,
							 const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case TILE_REMOVAL_SOFTNESS:
	case TILE_REMOVAL_SOFTNESS_SHADER:
	case TILE_REMOVAL_OFFSET:
	case TILE_REMOVAL_OFFSET_SHADER:
	case TILE_REMOVAL_ROTATION:
	case TILE_REMOVAL_ROTATION_SHADER:
	case TILE_REMOVAL_SCALE:
	case TILE_REMOVAL_SCALE_SHADER:
		return (dldata->GetBool(TILE_REMOVAL_BOOL) == TRUE);
		break;

	case IMAGE_SEQUENCE_FRAME:
	case IMAGE_SEQUENCE_FRAME_OFFSET:
		return (dldata->GetFloat(USE_IMAGE_SEQUENCE) == TRUE);
		break;

	default:
		break;
	}

	return true;
}

Bool DLTexture::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_TEXTURE_DEFAULT_COLOR:
			FillPopupMenu(dldata, dp, TEXTURE_DEFAULT_COLOR_SHADER_GROUP_PARAM);
			break;

		case POPUP_COLOR_CORRECT_COL_GAIN:
			FillPopupMenu(dldata, dp, COLOR_CORRECT_COL_GAIN_GROUP_PARAM);
			break;

		case POPUP_COLOR_CORRECT_COL_OFFSET:
			FillPopupMenu(dldata, dp, COLOR_CORRECT_COL_OFFSET_SHADER_GROUP_PARAM);
			break;

		case POPUP_COLOR_CORRECT_ALPHA_GAIN:
			FillPopupMenu(dldata, dp, COLOR_CORRECT_ALPHA_GAIN_SHADER_GROUP_PARAM);
			break;

		case POPUP_COLOR_CORRECT_ALPHA_OFFSET:
			FillPopupMenu(
				dldata, dp, COLOR_CORRECT_ALPHA_OFFSET_SHADER_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_SOFTNESS:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_SOFTNESS_SHADER_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_OFFSET:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_OFFSET_SHADER_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_ROTATION:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_ROTATION_SHADER_GROUP_PARAM);
			break;

		case POPUP_TILE_REMOVAL_SCALE:
			FillPopupMenu(dldata, dp, TILE_REMOVAL_SCALE_SHADER_GROUP_PARAM);
			break;

		default:
			break;
		}

	} else if (type == MSG_DESCRIPTION_COMMAND) {
		DescriptionCommand* dc = (DescriptionCommand*) data;

		if (dc->_descId[0].id == TEXTURE_FILE_BUTTON) {
			Filename loadFile;

			// open a file selector dialog to open a file
			if (loadFile.FileSelect(
						FILESELECTTYPE::IMAGES, FILESELECT::LOAD, "Open"_s)) {
				std::string file_name = loadFile.GetString().GetCStringCopy();
				dldata->SetString(TEXTURE_FILE, (String) file_name.c_str());
				{
					std::string formatted_fn = getFormattedFilename(file_name);

					if (file_name != formatted_fn && file_name != "" && formatted_fn != "") {
						node->SetParameter(DescID(TEXTURE_FILE),
										   GeData(formatted_fn.c_str()),
										   DESCFLAGS_SET::NONE);
						node->SetParameter(
							DescID(USE_IMAGE_SEQUENCE), GeData(TRUE), DESCFLAGS_SET::NONE);
					}
				}
			}
		}
	}

	EventAdd();
	return true;
}

Vector
DLTexture::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterDelightTexture(void)
{
	return RegisterShaderPlugin(
			   DL_TEXTURE, "Texture"_s, 0, DLTexture::Alloc, "dl_texture"_s, 0);
}
