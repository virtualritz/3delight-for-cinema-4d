#include "DLThin_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_thin.h"

Delight_Thin::Delight_Thin()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlThin.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[FRONT_COLOR] = std::make_pair("", "i_color");
	m_ids_to_names[FRONT_COLOR_SHADER] = std::make_pair("outColor", "i_color");
	m_ids_to_names[FRONT_ROUGHNESS] = std::make_pair("", "roughness");
	m_ids_to_names[FRONT_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "roughness");
	m_ids_to_names[FRONT_SPECULAR] = std::make_pair("", "specular_level");
	m_ids_to_names[FRONT_SPECULAR_SHADER] =
		std::make_pair("outColor[0]", "specular_level");
	m_ids_to_names[BACK_COLOR] = std::make_pair("", "color_back");
	m_ids_to_names[BACK_COLOR_SHADER] = std::make_pair("outColor", "color_back");
	m_ids_to_names[BACK_ROUGHNESS] = std::make_pair("", "roughness_back");
	m_ids_to_names[BACK_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "roughness_back");
	m_ids_to_names[BACK_SPECULAR] = std::make_pair("", "specular_level_back");
	m_ids_to_names[BACK_SPECULAR_SHADER] =
		std::make_pair("outColor[0]", "specular_level_back");
	m_ids_to_names[TRANSLUCENCY_VALUE] = std::make_pair("", "translucency");
	m_ids_to_names[TRANSLUCENCY_VALUE_SHADER] =
		std::make_pair("outColor[0]", "translucency");
	m_ids_to_names[TRANSLUCENCY_OPACITY] = std::make_pair("", "opacity");
	m_ids_to_names[TRANSLUCENCY_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "opacity");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
