#include "DLRamp_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_ramp.h"

Delight_Ramp::Delight_Ramp()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlRamp.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[RAMP_MODE] = std::make_pair("", "mode");
	m_ids_to_names[RAMP_COLOR_MIX] = std::make_pair("", "color_mix_mode");
	m_ids_to_names[COLOR_RAMP_GRADIENT] = std::make_pair("", "colorRamp");
	m_ids_to_names[RAMP_TRANSFORM_REPEAT] = std::make_pair("", "repeat");
	m_ids_to_names[RAMP_TRANSFORM_REPEAT_SHADER] =
		std::make_pair("outColor[0]", "repeat");
	m_ids_to_names[RAMP_TRANSFORM_OFFSET] = std::make_pair("", "offset");
	m_ids_to_names[RAMP_TRANSFORM_OFFSET_SHADER] =
		std::make_pair("outColor[0]", "offset");
	m_ids_to_names[RAMP_TRANSFORM_REVERSE] = std::make_pair("", "reverse");
	m_ids_to_names[RAMP_DISTORTION_INTENSITY] =
		std::make_pair("", "distortion_intensity");
	m_ids_to_names[RAMP_DISTORTION_SHADER] =
		std::make_pair("outColor", "distortion");
	m_ids_to_names[RAMP_NOISE_INTENSITY] = std::make_pair("", "noise_intensity");
	m_ids_to_names[RAMP_NOISE_INTENSITY_SHADER] =
		std::make_pair("outColor[0]", "noise_intensity");
	m_ids_to_names[RAMP_NOISE_HUE] = std::make_pair("", "noise_hue_intensity");
	m_ids_to_names[RAMP_NOISE_HUE_SHADER] =
		std::make_pair("outColor[0]", "noise_hue_intensity");
	m_ids_to_names[RAMP_NOISE_SATURATION] =
		std::make_pair("", "noise_saturation_intensity");
	m_ids_to_names[RAMP_NOISE_SATURATION_SHADER] =
		std::make_pair("outColor[0]", "noise_saturation_intensity");
	m_ids_to_names[RAMP_NOISE_VALUE] =
		std::make_pair("", "noise_value_intensity");
	m_ids_to_names[RAMP_NOISE_VALUE_SHADER] =
		std::make_pair("outColor[0]", "noise_value_intensity");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_SCALE] =
		std::make_pair("", "noise_scale");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_SCALE_SHADER] =
		std::make_pair("outColor[0]", "noise_scale");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_OFFSET] =
		std::make_pair("", "noise_offset");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_OFFSET_SHADER] =
		std::make_pair("outColor[0]", "noise_offset");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_STEPPING] =
		std::make_pair("", "noise_stepping");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_STEPPING_SHADER] =
		std::make_pair("outColor[0]", "noise_stepping");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_TIME] = std::make_pair("", "noise_time");
	m_ids_to_names[RAMP_NOISE_TRANSFORM_TIME_SHADER] =
		std::make_pair("outColor[0]", "noise_time");
	m_ids_to_names[RAMP_NOISE_LAYERS] = std::make_pair("", "layers");
	m_ids_to_names[RAMP_NOISE_LAYERS_PERSISTENCE] =
		std::make_pair("", "layer_persistence");
	m_ids_to_names[RAMP_NOISE_LAYERS_PERSISTENCE_SHADER] =
		std::make_pair("outColor[0]", "layer_persistence");
	m_ids_to_names[RAMP_NOISE_LAYERS_SCALE] = std::make_pair("", "layer_scale");
	m_ids_to_names[RAMP_NOISE_LAYERS_SCALE_SHADER] =
		std::make_pair("outColor[0]", "layer_scale");
	m_ids_to_names[RAMP_ADJUST_INVERT] = std::make_pair("", "invert");
	m_ids_to_names[RAMP_ADJUST_COLOR_GAIN] = std::make_pair("", "colorGain");
	m_ids_to_names[RAMP_ADJUST_COLOR_GAIN_SHADER] =
		std::make_pair("outColor", "colorGain");
	m_ids_to_names[RAMP_ADJUST_COLOR_OFFSET] = std::make_pair("", "colorOffset");
	m_ids_to_names[RAMP_ADJUST_COLOR_OFFSET_SHADER] =
		std::make_pair("outColor", "colorOffset");
	m_ids_to_names[RAMP_ADJUST_ALPHA_GAIN] = std::make_pair("", "alphaGain");
	m_ids_to_names[RAMP_ADJUST_ALPHA_GAIN_SHADER] =
		std::make_pair("outColor[0]", "alphaGain");
	m_ids_to_names[RAMP_ADJUST_ALPHA_OFFSET] = std::make_pair("", "alphaOffset");
	m_ids_to_names[RAMP_ADJUST_ALPHA_OFFSET_SHADER] =
		std::make_pair("outColor[0]", "alphaOffset");
}
