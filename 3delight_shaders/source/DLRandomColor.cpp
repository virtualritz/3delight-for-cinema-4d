#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_random_color.h"

class DLRandomColor : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLRandomColor);
	}
};

Bool DLRandomColor::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetVector(INPUT_COLOR1, Vector(HSVToRGB(Vector(0, 0, 0))));
	data->SetFloat(INPUT_IMPORTANCE1, 1.0);
	data->SetVector(INPUT_COLOR2, Vector(HSVToRGB(Vector(0, 0, 0))));
	data->SetFloat(INPUT_IMPORTANCE2, 1.0);
	data->SetVector(INPUT_COLOR3, Vector(HSVToRGB(Vector(0, 0, 0))));
	data->SetFloat(INPUT_IMPORTANCE3, 1.0);
	data->SetVector(INPUT_COLOR4, Vector(HSVToRGB(Vector(0, 0, 0))));
	data->SetFloat(INPUT_IMPORTANCE4, 1.0);
	data->SetVector(INPUT_COLOR5, Vector(HSVToRGB(Vector(0, 0, 0))));
	data->SetFloat(INPUT_IMPORTANCE5, 1.0);
	return true;
}

Bool DLRandomColor::GetDDescription(GeListNode* node,
									Description* description,
									DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_RANDOMCOLOR);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(INPUT_COLOR1_GROUP_PARAM,
						INPUT_COLOR1,
						INPUT_COLOR1_SHADER,
						INPUT_COLOR1_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE1_GROUP_PARAM,
						INPUT_IMPORTANCE1,
						INPUT_IMPORTANCE1_SHADER,
						INPUT_IMPORTANCE1_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_COLOR2_GROUP_PARAM,
						INPUT_COLOR2,
						INPUT_COLOR2_SHADER,
						INPUT_COLOR2_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE2_GROUP_PARAM,
						INPUT_IMPORTANCE2,
						INPUT_IMPORTANCE2_SHADER,
						INPUT_IMPORTANCE2_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_COLOR3_GROUP_PARAM,
						INPUT_COLOR3,
						INPUT_COLOR3_SHADER,
						INPUT_COLOR3_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE3_GROUP_PARAM,
						INPUT_IMPORTANCE3,
						INPUT_IMPORTANCE3_SHADER,
						INPUT_IMPORTANCE3_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_COLOR4_GROUP_PARAM,
						INPUT_COLOR4,
						INPUT_COLOR4_SHADER,
						INPUT_COLOR4_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE4_GROUP_PARAM,
						INPUT_IMPORTANCE4,
						INPUT_IMPORTANCE4_SHADER,
						INPUT_IMPORTANCE4_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_COLOR5_GROUP_PARAM,
						INPUT_COLOR5,
						INPUT_COLOR5_SHADER,
						INPUT_COLOR5_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE5_GROUP_PARAM,
						INPUT_IMPORTANCE5,
						INPUT_IMPORTANCE5_SHADER,
						INPUT_IMPORTANCE5_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLRandomColor::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_INPUT_COLOR1:
			FillPopupMenu(dldata, dp, INPUT_COLOR1_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE1:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE1_GROUP_PARAM);
			break;

		case POPUP_INPUT_COLOR2:
			FillPopupMenu(dldata, dp, INPUT_COLOR2_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE2:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE2_GROUP_PARAM);
			break;

		case POPUP_INPUT_COLOR3:
			FillPopupMenu(dldata, dp, INPUT_COLOR3_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE3:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE3_GROUP_PARAM);
			break;

		case POPUP_INPUT_COLOR4:
			FillPopupMenu(dldata, dp, INPUT_COLOR4_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE4:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE4_GROUP_PARAM);
			break;

		case POPUP_INPUT_COLOR5:
			FillPopupMenu(dldata, dp, INPUT_COLOR5_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE5:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE5_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLRandomColor::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterRandomColorTexture(void)
{
	return RegisterShaderPlugin(DL_RANDOMCOLOR,
								"Random Color"_s,
								0,
								DLRandomColor::Alloc,
								"dl_random_color"_s,
								0);
}
