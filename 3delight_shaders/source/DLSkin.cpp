#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_skin.h"

class DL_Skin : public MaterialData
{
	INSTANCEOF(DL_Skin, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Skin);
	}
};

Bool DL_Skin::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	const Float step = 360;
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	Vector skin_color = HSVToRGB(Vector(21.414 / step, 0.377, 0.788));
	data->SetVector(SKIN_COLOR, skin_color);
	data->SetFloat(SKIN_ROUGHNESS, 0.3);
	data->SetFloat(SKIN_SPECULAR_LEVEL, 0.5);
	Vector subsurface_color = HSVToRGB(Vector(9.848 / step, 0.766, 0.964));
	data->SetVector(SUBSURFACE_COLOR, subsurface_color);
	data->SetFloat(SUBSURFACE_SCALE, 0.5);
	data->SetFloat(SUBSURFACE_IOR, 1.3);
	data->SetFloat(SUBSURFACE_ANISOTROPY, 0);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	return true;
}

Bool DL_Skin::GetDEnabling(GeListNode* node,
						   const DescID& id,
						   const GeData& t_data,
						   DESCFLAGS_ENABLE flags,
						   const BaseContainer* itemdesc)
{
	return true;
}

Bool DL_Skin::GetDDescription(GeListNode* node,
							  Description* description,
							  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_SKIN);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*)node)->GetDataInstance();
	HideAndShowTextures(SKIN_COLOR_GROUP_PARAM, SKIN_COLOR, SKIN_COLOR_SHADER, SKIN_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SKIN_ROUGHNESS_GROUP_PARAM, SKIN_ROUGHNESS, SKIN_ROUGHNESS_SHADER, SKIN_ROUGHNESS_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SKIN_SPECULAR_LEVEL_GROUP_PARAM, SKIN_SPECULAR_LEVEL, SKIN_SPECULAR_LEVEL_SHADER, SKIN_SPECULAR_LEVEL_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_COLOR_GROUP_PARAM, SUBSURFACE_COLOR, SUBSURFACE_COLOR_SHADER, SUBSURFACE_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_SCALE_GROUP_PARAM, SUBSURFACE_SCALE, SUBSURFACE_SCALE_SHADER, SUBSURFACE_SCALE_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_IOR_GROUP_PARAM, SUBSURFACE_IOR, SUBSURFACE_IOR_SHADER, SUBSURFACE_IOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SUBSURFACE_ANISOTROPY_GROUP_PARAM, SUBSURFACE_ANISOTROPY, SUBSURFACE_ANISOTROPY_SHADER, SUBSURFACE_ANISOTROPY_SHADER_TEMP, node, description, dldata);
	return TRUE;
}

Bool DL_Skin::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_SKIN_COLOR:
			FillPopupMenu(dldata, dp, SKIN_COLOR_GROUP_PARAM);
			break;

		case POPUP_SKIN_ROUGHNESS:
			FillPopupMenu(dldata, dp, SKIN_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_SKIN_SPECULAR_LEVEL:
			FillPopupMenu(dldata, dp, SKIN_SPECULAR_LEVEL_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_COLOR:
			FillPopupMenu(dldata, dp, SUBSURFACE_COLOR_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_SCALE:
			FillPopupMenu(dldata, dp, SUBSURFACE_SCALE_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_IOR:
			FillPopupMenu(dldata, dp, SUBSURFACE_IOR_GROUP_PARAM);
			break;

		case POPUP_SUBSURFACE_ANISOTROPY:
			FillPopupMenu(dldata, dp, SUBSURFACE_ANISOTROPY_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Skin::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Skin::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLSkin(void)
{
	return RegisterMaterialPlugin(
			   DL_SKIN, "Skin"_s, PLUGINFLAG_HIDE, DL_Skin::Alloc, "Dl_skin"_s, 0);
}
