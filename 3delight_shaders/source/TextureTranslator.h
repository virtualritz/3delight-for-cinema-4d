#pragma once

#include "DL_TranslatorPlugin.h"
#include "c4d.h"

class TextureTranslator : public DL_TranslatorPlugin
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);

private:
	BaseContainer data;
};
