#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_flakes.h"

class DLFlakes : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLFlakes);
	}
};

Bool DLFlakes::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetFloat(FLAKES_DENSITY, 0.5);
	data->SetFloat(FLAKES_SCALE, 1.0);
	data->SetFloat(FLAKES_RANDOMNESS, 0.5);
	data->SetInt32(FLAKES_LAYERS, 1);
	return true;
}

Bool DLFlakes::GetDDescription(GeListNode* node,
							   Description* description,
							   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_Flakes);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(FLAKES_DENSITY_GROUP_PARAM,
						FLAKES_DENSITY,
						FLAKES_DENSITY_SHADER,
						FLAKES_DENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_SCALE_GROUP_PARAM,
						FLAKES_SCALE,
						FLAKES_SCALE_SHADER,
						FLAKES_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_RANDOMNESS_GROUP_PARAM,
						FLAKES_RANDOMNESS,
						FLAKES_RANDOMNESS_SHADER,
						FLAKES_RANDOMNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLFlakes::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_FLAKES_DENSITY:
			FillPopupMenu(dldata, dp, FLAKES_DENSITY_GROUP_PARAM);
			break;

		case POPUP_FLAKES_SCALE:
			FillPopupMenu(dldata, dp, FLAKES_SCALE_GROUP_PARAM);
			break;

		case POPUP_FLAKES_RANDOMNESS:
			FillPopupMenu(dldata, dp, FLAKES_RANDOMNESS_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLFlakes::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterFlakesTexture(void)
{
	return RegisterShaderPlugin(
			   DL_Flakes, "Flakes"_s, 0, DLFlakes::Alloc, "dl_flakes"_s, 0);
}
