#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Toon_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Toon_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_TOON, doc);
	return true;
}

Bool Register_Toon_Object(void)
{
	DL_Toon_command* new_toon = NewObjClear(DL_Toon_command);
	RegisterCommandPlugin(DL_TOON_COMMAND,
						  "Toon"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlToon_200.png"_s),
						  String("Assign new Toon"_s),
						  NewObjClear(DL_Toon_command));

	if (RegisterCommandPlugin(DL_TOON_SHELF_COMMAND,
							  "Toon"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlToon_200.png"_s),
							  String("Assign new Toon Material"),
							  new_toon)) {
		new_toon->shelf_used = 1;
	}

	return true;
}
