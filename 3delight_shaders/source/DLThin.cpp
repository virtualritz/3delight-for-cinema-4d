#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_thin.h"

class DL_Thin : public MaterialData
{
	INSTANCEOF(DL_Thin, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc()
	{
		return NewObjClear(DL_Thin);
	}
};

Bool DL_Thin::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetVector(FRONT_COLOR, HSVToRGB(Vector(0, 0, 0.8)));
	data->SetFloat(FRONT_ROUGHNESS, 0.3);
	data->SetFloat(FRONT_SPECULAR, 0.5);
	data->SetVector(BACK_COLOR, HSVToRGB(Vector(0, 0, 0.8)));
	data->SetFloat(BACK_ROUGHNESS, 0.3);
	data->SetFloat(BACK_SPECULAR, 0.5);
	data->SetFloat(TRANSLUCENCY_VALUE, 0.5);
	data->SetFloat(TRANSLUCENCY_OPACITY, 1.0);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	data->SetInt32(BUMP_LAYERS_AFFECTED, AFFECT_BOTH_LAYERS);
	return true;
}

Bool DL_Thin::GetDEnabling(GeListNode* node,
						   const DescID& id,
						   const GeData& t_data,
						   DESCFLAGS_ENABLE flags,
						   const BaseContainer* itemdesc)
{
	return true;
}

Bool DL_Thin::GetDDescription(GeListNode* node,
							  Description* description,
							  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_THIN);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(FRONT_COLOR_GROUP_PARAM,
						FRONT_COLOR,
						FRONT_COLOR_SHADER,
						FRONT_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FRONT_ROUGHNESS_GROUP_PARAM,
						FRONT_ROUGHNESS,
						FRONT_ROUGHNESS_SHADER,
						FRONT_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FRONT_SPECULAR_GROUP_PARAM,
						FRONT_SPECULAR,
						FRONT_SPECULAR_SHADER,
						FRONT_SPECULAR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BACK_COLOR_GROUP_PARAM,
						BACK_COLOR,
						BACK_COLOR_SHADER,
						BACK_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BACK_ROUGHNESS_GROUP_PARAM,
						BACK_ROUGHNESS,
						BACK_ROUGHNESS_SHADER,
						BACK_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BACK_SPECULAR_GROUP_PARAM,
						BACK_SPECULAR,
						BACK_SPECULAR_SHADER,
						BACK_SPECULAR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TRANSLUCENCY_VALUE_GROUP_PARAM,
						TRANSLUCENCY_VALUE,
						TRANSLUCENCY_VALUE_SHADER,
						TRANSLUCENCY_VALUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(TRANSLUCENCY_OPACITY_GROUP_PARAM,
						TRANSLUCENCY_OPACITY,
						TRANSLUCENCY_OPACITY_SHADER,
						TRANSLUCENCY_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Thin::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_FRONT_COLOR:
			FillPopupMenu(dldata, dp, FRONT_COLOR_GROUP_PARAM);
			break;

		case POPUP_FRONT_ROUGHNESS:
			FillPopupMenu(dldata, dp, FRONT_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_FRONT_SPECULAR:
			FillPopupMenu(dldata, dp, FRONT_SPECULAR_GROUP_PARAM);
			break;

		case POPUP_BACK_COLOR:
			FillPopupMenu(dldata, dp, BACK_COLOR_GROUP_PARAM);
			break;

		case POPUP_BACK_ROUGHNESS:
			FillPopupMenu(dldata, dp, BACK_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_BACK_SPECULAR:
			FillPopupMenu(dldata, dp, BACK_SPECULAR_GROUP_PARAM);
			break;

		case POPUP_TRANSLUCENCY_VALUE:
			FillPopupMenu(dldata, dp, TRANSLUCENCY_VALUE_GROUP_PARAM);
			break;

		case POPUP_TRANSLUCENCY_OPACITY:
			FillPopupMenu(dldata, dp, TRANSLUCENCY_OPACITY_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Thin::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Thin::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLThin(void)
{
	return RegisterMaterialPlugin(
			   DL_THIN, "Thin"_s, PLUGINFLAG_HIDE, DL_Thin::Alloc, "dl_thin"_s, 0);
}
