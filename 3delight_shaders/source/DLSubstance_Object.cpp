#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Substance_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Substance_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_SUBSTANCE, doc);
	return true;
}

Bool Register_Substance_Object(void)
{
	DL_Substance_command* new_substance = NewObjClear(DL_Substance_command);
	RegisterCommandPlugin(DL_SUBSTANCE_COMMAND,
						  "Substance"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlSubstance_200.png"_s),
						  String("Assign new Substance"),
						  NewObjClear(DL_Substance_command));

	if (RegisterCommandPlugin(DL_SUBSTANCE_SHELF_COMMAND,
							  "Substance"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlSubstance_200.png"_s),
							  String("Assign new Substance Material"),
							  new_substance)) {
		new_substance->shelf_used = 1;
	}

	return true;
}