#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_color_variation.h"

class DLColorVariation : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DLColorVariation);
	}
};

Bool DLColorVariation::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*) node)->GetDataInstance();
	data->SetVector(INPUT_COLOR, HSVToRGB(Vector(1, 0, 240.0 / 360.0)));
	data->SetFloat(VARIATION_HUE, 0);
	data->SetFloat(VARIATION_SATURATION, 0);
	data->SetFloat(VARIATION_BRIGHTNESS, 0);
	data->SetFloat(RANDOMNESS_TYPE, RANDOMNESS_UNIFORM);
	data->SetFloat(SOURCE_TYPE, SOURCE_TYPE_OBJECT);
	data->SetInt32(SEED, 0);
	return true;
}

Bool DLColorVariation::GetDDescription(GeListNode* node,
									   Description* description,
									   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_COLORVARIATION);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(INPUT_COLOR_GROUP_PARAM,
						INPUT_COLOR,
						INPUT_COLOR_SHADER,
						INPUT_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(VARIATION_HUE_GROUP_PARAM,
						VARIATION_HUE,
						VARIATION_HUE_SHADER,
						VARIATION_HUE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(VARIATION_SATURATION_GROUP_PARAM,
						VARIATION_SATURATION,
						VARIATION_SATURATION_SHADER,
						VARIATION_SATURATION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(VARIATION_BRIGHTNESS_GROUP_PARAM,
						VARIATION_BRIGHTNESS,
						VARIATION_BRIGHTNESS_SHADER,
						VARIATION_BRIGHTNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DLColorVariation::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_INPUT_COLOR:
			FillPopupMenu(dldata, dp, INPUT_COLOR_GROUP_PARAM);
			break;

		case POPUP_VARIATION_HUE:
			FillPopupMenu(dldata, dp, VARIATION_HUE_GROUP_PARAM);
			break;

		case POPUP_VARIATION_SATURATION:
			FillPopupMenu(dldata, dp, VARIATION_SATURATION_GROUP_PARAM);
			break;

		case POPUP_VARIATION_BRIGHTNESS:
			FillPopupMenu(dldata, dp, VARIATION_BRIGHTNESS_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

Vector
DLColorVariation::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterColorVariationTexture(void)
{
	return RegisterShaderPlugin(DL_COLORVARIATION,
								"Color Variation"_s,
								0,
								DLColorVariation::Alloc,
								"dl_color_variation"_s,
								0);
}
