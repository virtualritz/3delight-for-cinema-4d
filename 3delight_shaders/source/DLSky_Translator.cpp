#include "DLSky_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_sky.h"

Delight_Sky::Delight_Sky()
{
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlSky.oso";
	m_ids_to_names[Shader_Path] = std::make_pair("", &shaderpath[0]);
	m_ids_to_names[SKY_INTENSITY] = std::make_pair("", "intensity");
	m_ids_to_names[SKY_INTENSITY_SHADER] =
		std::make_pair("outColor[0]", "intensity");
	m_ids_to_names[SKY_TURBIDITY] = std::make_pair("", "turbidity");
	m_ids_to_names[SKY_GROUND_COLOR] = std::make_pair("", "ground_albedo");
	m_ids_to_names[SKY_ELEVATION] = std::make_pair("", "elevation");
	m_ids_to_names[SKY_AZIMUTH] = std::make_pair("", "azimuth");
	m_ids_to_names[SKY_TINT] = std::make_pair("", "sky_tint");
	m_ids_to_names[SKY_SUN_TINT] = std::make_pair("", "sun_tint");
	m_ids_to_names[SKY_SUN_SIZE] = std::make_pair("", "sun_size");
	m_ids_to_names[SKY_DRAW_SUN_DISK] = std::make_pair("", "sun_enable");
	m_ids_to_names[SKY_DRAW_GROUND] = std::make_pair("", "ground_enable");
}
