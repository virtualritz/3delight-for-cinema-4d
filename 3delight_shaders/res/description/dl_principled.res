CONTAINER dl_principled
{
	NAME dl_principled;
	
	INCLUDE Mpreview;
	INCLUDE Mbase;
	SCALE_H;
	
	GROUP Obaselist
	{
		BOOL PRINCIPLED_MATERIAL_PAGE { HIDDEN; PAGE; PARENTMSG PRINCIPLED_MATERIAL;}
	}
	
	GROUP PRINCIPLED_MATERIAL
	{
		SEPARATOR {LINE;}
		GROUP
		{
			COLUMNS 2;
			SEPARATOR COATING_LAYER{SCALE_H;}
			STATICTEXT{}
		
			REAL COATING_LAYER_THICKNESS{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK COATING_LAYER_THICKNESS_SHADER{ANIM ON; SCALE_H; HIDDEN;}
			SHADERLINK COATING_LAYER_THICKNESS_SHADER_TEMP{ANIM ON; SCALE_H; HIDDEN;}
			POPUP POPUP_THICKNESS{SCALE_V;}
			
			COLOR COATING_LAYER_COLOR{SCALE_H;}
			SHADERLINK COATING_LAYER_COLOR_SHADER{ANIM ON; HIDDEN;}
			SHADERLINK COATING_LAYER_COLOR_SHADER_TEMP{ANIM ON; HIDDEN;}
			POPUP POPUP_COLOR{SCALE_V;}

			REAL COATING_LAYER_ROUGHNESS{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK COATING_LAYER_ROUGHNESS_SHADER{ANIM ON; HIDDEN;}
			SHADERLINK COATING_LAYER_ROUGHNESS_SHADER_TEMP{ANIM ON; HIDDEN;}
			POPUP POPUP_ROUGHNESS{SCALE_V;}

			REAL COATING_LAYER_SPECULAR_LEVEL{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK COATING_LAYER_SPECULAR_LEVEL_SHADER{ANIM ON; HIDDEN;}
			SHADERLINK COATING_LAYER_SPECULAR_LEVEL_SHADER_TEMP{ANIM ON; HIDDEN;}
			POPUP POPUP_SPECULAR{SCALE_V;}	
			
			
			STATICTEXT{SCALE_H;}
			STATICTEXT{}
			SEPARATOR BASE_LAYER{SCALE_H;}
			STATICTEXT{}
			
			
			COLOR BASE_LAYER_COLOR{SCALE_H;}
			SHADERLINK BASE_LAYER_COLOR_SHADER{ANIM ON; HIDDEN;}
			SHADERLINK BASE_LAYER_COLOR_SHADER_TEMP{ANIM ON; HIDDEN;}
			POPUP POPUP_BASECOLOR{SCALE_V;}
		
			REAL BASE_LAYER_ROUGHNESS{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK BASE_LAYER_ROUGHNESS_SHADER{ANIM OFF; HIDDEN;}
			SHADERLINK BASE_LAYER_ROUGHNESS_SHADER_TEMP{ANIM OFF; HIDDEN;}
			POPUP POPUP_BASEROUGHNESS{SCALE_V;}

			REAL BASE_LAYER_SPECULAR_LEVEL{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK BASE_LAYER_SPECULAR_LEVEL_SHADER{ANIM OFF; HIDDEN;}
			SHADERLINK BASE_LAYER_SPECULAR_LEVEL_SHADER_TEMP{ANIM OFF; HIDDEN;}
			POPUP POPUP_BASESPECULAR{SCALE_V;}
		
			REAL BASE_LAYER_METALLIC{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK BASE_LAYER_METALLIC_SHADER{ANIM OFF; HIDDEN;}	
			SHADERLINK BASE_LAYER_METALLIC_SHADER_TEMP{ANIM OFF; HIDDEN;}	
			POPUP POPUP_BASEMETALLIC{SCALE_V;}

			REAL BASE_LAYER_ANISOTROPY{SCALE_H; MIN -1; MAX 1; MINSLIDER -1; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK BASE_LAYER_ANISOTROPY_SHADER{ANIM OFF; HIDDEN;}
			SHADERLINK BASE_LAYER_ANISOTROPY_SHADER_TEMP{ANIM OFF; HIDDEN;}
			POPUP POPUP_BASEANISOTROPY{SCALE_V;}
		
			COLOR BASE_LAYER_ANISOTROPY_DIRECTION{SCALE_H;}
			SHADERLINK BASE_LAYER_ANISOTROPY_DIRECTION_SHADER{ANIM OFF; HIDDEN;}
			SHADERLINK BASE_LAYER_ANISOTROPY_DIRECTION_SHADER_TEMP{ANIM OFF; HIDDEN;}
			POPUP POPUP_BASEANISOTROPY_DIRECTION{SCALE_V;}

			REAL BASE_LAYER_OPACITY{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.001; CUSTOMGUI REALSLIDER;}
			SHADERLINK BASE_LAYER_OPACITY_SHADER{ANIM OFF; HIDDEN;}
			SHADERLINK BASE_LAYER_OPACITY_SHADER_TEMP{ANIM OFF; HIDDEN;}
			POPUP POPUP_BASEOPACITY{SCALE_V;}
		
		
			STATICTEXT{SCALE_H;}
			STATICTEXT{}
			SEPARATOR SUBSURFACE{SCALE_H;}
			STATICTEXT{}
			
			
			REAL SUBSURFACE_WEIGHT{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SHADERLINK SUBSURFACE_WEIGHT_SHADER{ANIM OFF; HIDDEN;SCALE_H;}
			SHADERLINK SUBSURFACE_WEIGHT_SHADER_TEMP{ANIM OFF; HIDDEN;SCALE_H;}
			POPUP POPUP_SUBSURFACE_WEIGHT{SCALE_V;}

			COLOR SUBSURFACE_COLOR{SCALE_H;}
			SHADERLINK SUBSURFACE_COLOR_SHADER{ANIM OFF; HIDDEN;SCALE_H;}
			SHADERLINK SUBSURFACE_COLOR_SHADER_TEMP{ANIM OFF; HIDDEN;SCALE_H;}
			POPUP POPUP_SUBSURFACE_COLOR{SCALE_V;}
			
			REAL SUBSURFACE_ANISOTROPY{SCALE_H; MIN -1; MAX 1; MINSLIDER -1; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SHADERLINK SUBSURFACE_ANISOTROPY_SHADER{ANIM OFF; HIDDEN;SCALE_H;}
			SHADERLINK SUBSURFACE_ANISOTROPY_SHADER_TEMP{ANIM OFF; HIDDEN;SCALE_H;}
			POPUP POPUP_SUBSURFACE_ANISOTROPY{SCALE_V;}

			REAL SUBSURFACE_SCALE{SCALE_H;MIN 0; MAX 1000; MINSLIDER 0; MAXSLIDER 100; STEP 0.1; CUSTOMGUI REALSLIDER;}
			SHADERLINK SUBSURFACE_SCALE_SHADER{ANIM OFF;HIDDEN;SCALE_H;}
			SHADERLINK SUBSURFACE_SCALE_SHADER_TEMP{ANIM OFF;HIDDEN;SCALE_H;}
			POPUP POPUP_SUBSURFACE_SCALE{SCALE_V;}

			
			STATICTEXT{SCALE_H;}
			STATICTEXT{}
			SEPARATOR REFRACTION{SCALE_H;}
			STATICTEXT{}


			REAL REFRACTION_WEIGHT{SCALE_H; MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SHADERLINK REFRACTION_WEIGHT_SHADER{ANIM OFF; HIDDEN;SCALE_H;}
			SHADERLINK REFRACTION_WEIGHT_SHADER_TEMP{ANIM OFF; HIDDEN;SCALE_H;}
			POPUP POPUP_REFRACTION_WEIGHT{SCALE_V;}

			COLOR REFRACTION_COLOR{SCALE_H;}
			SHADERLINK REFRACTION_COLOR_SHADER{ANIM OFF; HIDDEN;SCALE_H;}
			SHADERLINK REFRACTION_COLOR_SHADER_TEMP{ANIM OFF; HIDDEN;SCALE_H;}
			POPUP POPUP_REFRACTION_COLOR{SCALE_V;}

			COLOR REFRACTION_SCATTER{SCALE_H;}
			SHADERLINK REFRACTION_SCATTER_SHADER{ANIM OFF; HIDDEN;SCALE_H;}
			SHADERLINK REFRACTION_SCATTER_SHADER_TEMP{ANIM OFF; HIDDEN;SCALE_H;}
			POPUP POPUP_REFRACTION_SCATTER{SCALE_V;}

			REAL REFRACTION_IOR{SCALE_H;MIN 0; MINSLIDER 0; MAXSLIDER 10; STEP 0.1; CUSTOMGUI REALSLIDER;}
			SHADERLINK REFRACTION_IOR_SHADER{ANIM OFF;HIDDEN;SCALE_H;}
			SHADERLINK REFRACTION_IOR_SHADER_TEMP{ANIM OFF;HIDDEN;SCALE_H;}
			POPUP POPUP_REFRACTION_IOR{SCALE_V;}

			REAL REFRACTION_ROUGHNESS{SCALE_H;MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 10; STEP 0.1; CUSTOMGUI REALSLIDER;}
			SHADERLINK REFRACTION_ROUGHNESS_SHADER{ANIM OFF;HIDDEN;SCALE_H;}
			SHADERLINK REFRACTION_ROUGHNESS_SHADER_TEMP{ANIM OFF;HIDDEN;SCALE_H;}
			POPUP POPUP_REFRACTION_ROUGHNESS{SCALE_V;}

			REAL REFRACTION_DENSITY{SCALE_H;MIN 0; MINSLIDER 0; MAXSLIDER 10; STEP 0.1; CUSTOMGUI REALSLIDER;}
			SHADERLINK REFRACTION_DENSITY_SHADER{ANIM OFF;HIDDEN;SCALE_H;}
			SHADERLINK REFRACTION_DENSITY_SHADER_TEMP{ANIM OFF;HIDDEN;SCALE_H;}
			POPUP POPUP_REFRACTION_DENSITY{SCALE_V;}

			STATICTEXT{SCALE_H;}
			STATICTEXT{}
			SEPARATOR INCANDESCENCE{SCALE_H;}
			STATICTEXT{}

			COLOR INCANDESCENCE_COLOR{SCALE_H;}
			SHADERLINK INCANDESCENCE_COLOR_SHADER{ANIM OFF;HIDDEN;SCALE_H;}
			SHADERLINK INCANDESCENCE_COLOR_SHADER_TEMP{ANIM OFF;HIDDEN;}
			POPUP POPUP_INCANDESCENCE_COLOR{SCALE_V;}

			REAL INCANDESCENCE_INTENSITY{SCALE_H; MIN 0; MINSLIDER 0; MAXSLIDER 10; STEP 0.1; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
		
			GROUP
			{
				STATICTEXT{}
				SEPARATOR BUMP{SCALE_H;}
				LONG BUMP_TYPE
				{
					SCALE_H;
					CYCLE
					{
						BUMP_MAP;
						NORMAL_MAP_DIRECTX;
						NORMAL_MAP_OPENGL;
						DISPLACEMENT_0;
						DISPLACEMENT_5;
					}
				}
				
				SHADERLINK BUMP_VALUE{ANIM OFF;}
								
				REAL BUMP_INTENSITY{MINSLIDER 0; MAXSLIDER 10 ;CUSTOMGUI REALSLIDER;}
				
				LONG BUMP_LAYERS_AFFECTED
				{
					CYCLE
					{
						AFFECT_BOTH_LAYERS;
						AFFECT_COATING_LAYER;
						AFFECT_BASE_LAYER;
					}
				}
			}
			
			
			STATICTEXT{SCALE_H;}
			STATICTEXT{}
			SEPARATOR AOV_PAGE{SCALE_H;}
			STATICTEXT{}
			LINK DL_AOV_GROUP{SCALE_H;}
			
			//GROUP 
			//{
			//	BUTTON DL_ADD_COLOR{}
			//	STRING DL_ADD_COLOR_TEXT{}
			//}
			
		}
	}
	INCLUDE Massign;
}
