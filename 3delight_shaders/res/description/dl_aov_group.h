#pragma once

enum {
	DL_COLOR_AOVs = 1000,
	AOV_GROUP_NAME,
	DL_FLOAT_AOV,
	// AOV_PAGE =1500,
	AOV_ADD,
	AOV_GO,
	AOV_POPUP,
	AOV_POPUP_VALUE,
	SELECTED_AOV,
	DL_CUSTOM_COLORS,
	DL_CUSTOM_FLOATS,
	DL_ADD_COLOR,
	DL_ADD_COLOR_TEXT,
	IS_USED

};
