#ifndef DL_SCENE_PARSER
#define DL_SCENE_PARSER
#include <string>
// Forward declarations
namespace NSI
{
class Context;
}
class BaseDocument;
class BaseContainer;
class BaseList2D;

enum RENDER_MODE {
	C4D_PREVIEW,
	C4D_FINAL,
	DL_IPR,
	DL_INTERACTIVE,
	DL_BATCH,
	DL_EXPORT_NSI,
	DL_STANDIN_EXPORT
};

// Pure virtual interface
class DL_SceneParser
{
public:
	virtual NSI::Context& GetContext() = 0;

	virtual std::string GetHandleName(BaseList2D* node) = 0;

	virtual BaseContainer* GetSettings() = 0;
	virtual RENDER_MODE GetRenderMode() = 0;
	virtual void GetRenderResolution(int* width, int* height) = 0;
};

#endif