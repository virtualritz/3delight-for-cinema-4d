#include <queue>
#include "DL_TypeConversions.h"
#include "ErrorHandler.h"
#include "IDs.h"
#include "SceneParser.h"
#include "c4d.h"
#include "dl_preferences.h"
#include "dlrendersettings.h"
#include "nsi.hpp"

//#include "DL_Render.h"
using namespace std;

void RenderManagerFrameStoppedCallback(void* stoppedcallbackdata,
									   NSIContext_t ctx,
									   int status)
{
	// End context when rendering is completed (or stopped)
	SceneParser* sp = (SceneParser*) stoppedcallbackdata;
	delete sp;

	if (status == NSIRenderAborted) {
		// Render was stopped before completion
		SpecialEventAdd(DL_FRAME_STOPPED);

	} else {
		// Render was succesful, tell rendermanager to continue rendering any
		// remaining frames
		SpecialEventAdd(DL_FRAME_COMPLETED);
	}
}

class RenderManager : public MessageData
{
public:
	RenderManager();
	~RenderManager();

	void FillFrames();

	void RenderNextFrame();

	virtual Bool CoreMessage(Int32 id, const BaseContainer& bc);

private:
	SceneParser* parser;
	queue<long> frames_to_render;
	bool batch;
};

RenderManager::RenderManager()
	: parser(0x0) {}

RenderManager::~RenderManager()
{
	delete (parser);
}

void RenderManager::FillFrames()
{
	BaseDocument* doc = GetActiveDocument();
	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer render_data = rd->GetData();
	BaseTime t = doc->GetTime();
	frames_to_render = queue<long>();
	long frame_start = 0;
	long frame_end = 0;
	long FrameSequence = render_data.GetInt32(RDATA_FRAMESEQUENCE);

	if (FrameSequence == RDATA_FRAMESEQUENCE_MANUAL) {
		BaseTime t1 = render_data.GetTime(RDATA_FRAMEFROM);
		BaseTime t2 = render_data.GetTime(RDATA_FRAMETO);
		frame_start = t1.GetFrame(doc->GetFps());
		frame_end = t2.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_CURRENTFRAME) {
		frame_start = frame_end = t.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_ALLFRAMES) {
		BaseTime t1 = doc->GetMinTime();
		BaseTime t2 = doc->GetMaxTime();
		frame_start = t1.GetFrame(doc->GetFps());
		frame_end = t2.GetFrame(doc->GetFps());

	} else if (FrameSequence == RDATA_FRAMESEQUENCE_PREVIEWRANGE) {
		BaseTime t1 = doc->GetLoopMinTime();
		BaseTime t2 = doc->GetLoopMaxTime();
		frame_start = t1.GetFrame(doc->GetFps());
		frame_end = t2.GetFrame(doc->GetFps());
	}

	long framestep = render_data.GetInt32(RDATA_FRAMESTEP, 1);
	bool renderOK = true;

	for (long i = frame_start; i <= frame_end && renderOK; i = i + framestep) {
		frames_to_render.push(i);
	}
}

void RenderManager::RenderNextFrame()
{
	if (!frames_to_render.empty()) {
		Int64 frame = frames_to_render.front();
		frames_to_render.pop();

		if (batch) {
			StatusSetText("Batch rendering frame " + String::IntToString(frame));

		} else {
			StatusSetText("Rendering frame " + String::IntToString(frame));
		}

		BaseDocument* doc = GetActiveDocument();
		BaseDocument* renderdoc =
			(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
		// ApplicationOutput(doc->GetDocumentPath().GetString());
		// ApplicationOutput(renderdoc->GetDocumentPath().GetString());
		RenderData* rd = renderdoc->GetActiveRenderData();
		BaseContainer renderSettings = rd->GetData();
		BaseContainer* bc =
			GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

		if (!bc) {
			GetWorldContainerInstance()->SetContainer(PREFERENCES_ID,
					BaseContainer());
			bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);
		}

		// Check cloud
		bool use_cloud = false;
		bool output_nsi = false;
		bool has_vp = false;
		BaseVideoPost* vp = rd->GetFirstVideoPost();
		BaseContainer* data;

		while (vp != NULL && !has_vp) {
			has_vp = (vp->GetType() == ID_RENDERSETTINGS); // Look for rendersettings

			if (!has_vp) {
				vp = vp->GetNext();
			}
		}

		if (has_vp) {
			data = vp->GetDataInstance();
			use_cloud = data->GetBool(DL_USE_CLOUD);
			output_nsi = data->GetBool(OUTPUT_NSI_FILES);
		}

		// BaseVideoPost* vp = rd->GetFirstVideoPost();
		parser = new SceneParser(renderdoc);
		NSI::ArgumentList arglist;
		NSIErrorHandler_t eh = NSIErrorHandlerC4D;
		arglist.Add(new NSI::PointerArg("errorhandler", (void*) eh));

		if (use_cloud && !output_nsi) {
			arglist.Add(new NSI::IntegerArg("cloud", 1));
		}

		if (output_nsi) {
			Filename outfile;
			outfile = data->GetFilename(OUTPUT_NSI_FILES_PATH);
			// outfile.ClearSuffix();
			String filepart = outfile.GetFileString();
			outfile.SetFile(
				Filename(filepart + (String) std::to_string(frame).c_str()));
			outfile.SetSuffix(String("nsi"));
			string filename_string = StringToStdString(outfile.GetString());
			arglist.Add(new NSI::StringArg("type", "apistream"));
			arglist.Add(new NSI::StringArg("streamfilename", filename_string));
			arglist.Add(new NSI::StringArg("streamformat", "nsi"));
		}

		// Int32 Used_Renderer = renderSettings.GetInt32(RDATA_RENDERENGINE);
		// if (Used_Renderer == ID_RENDERSETTINGS) {
		// arglist.Add(new NSI::IntegerArg("cloud", 1));
		//}
		// NSIErrorHandler_t eh = NSIErrorHandlerForC4D;
		// if (Used_Renderer == ID_RENDERSETTINGS_CLOUD)
		// arglist.Add(new NSI::IntegerArg("cloud", 1));
		// arglist.Add(new NSI::PointerArg("errorhandler", eh));
		parser->GetContext().Begin(arglist);

		if (batch) {
			parser->SetRenderMode(DL_BATCH);

		} else {
			parser->SetRenderMode(DL_INTERACTIVE);
		}

		// Render scene
		bool RenderOK = parser->InitScene(true, frame);
		parser->SampleFrameMotion(frame);
		BaseDocument::Free(renderdoc);
		int scanning = bc->GetInt32(DL_SCANNING);
		std::string bucket_order = "";

		switch (scanning) {
		case 0:
			bucket_order = "circle";
			break;

		case 1:
			bucket_order = "spiral";
			break;

		case 2:
			bucket_order = "horizontal";
			break;

		case 3:
			bucket_order = "vertical";
			break;

		case 4:
			bucket_order = "zigzag";
			break;

		default:
			bucket_order = "horizontal";
			break;
		}

		if (bucket_order == "") {
			bucket_order = "circle";
		}

		if (batch) {
			bucket_order = "horizontal";
		}

		parser->GetContext().SetAttribute(
			NSI_SCENE_GLOBAL,
			(NSI::IntegerArg("renderatlowpriority", 1),
			 NSI::StringArg("bucketorder", bucket_order)));
		// Start render
		// The render process takes ownership of the SceneParser, which is
		// deleted (along with the NSI context) by the stoppedcallback function
		parser->GetContext().RenderControl(
			(NSI::StringArg("action", "start"),
			 NSI::IntegerArg("progressive", 0),
			 NSI::PointerArg("stoppedcallback",
							 (void*) &RenderManagerFrameStoppedCallback),
			 NSI::PointerArg("stoppedcallbackdata", (void*) parser)));
	}
}

Bool RenderManager::CoreMessage(Int32 id, const BaseContainer& bc)
{
	if (id == DL_START_RENDER || id == DL_START_BATCH_RENDER) {
		if (parser) {
			parser->GetContext().RenderControl(NSI::CStringPArg("action", "stop"));
		}

		if (id == DL_START_RENDER) {
			batch = false;

		} else {
			batch = true;
		}

		FillFrames();
		RenderNextFrame();

	} else if (id == DL_FRAME_COMPLETED) {
		BaseDocument* doc = GetActiveDocument();
		BaseDocument* renderdoc =
			(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
		// ApplicationOutput(doc->GetDocumentPath().GetString());
		// ApplicationOutput(renderdoc->GetDocumentPath().GetString());
		RenderData* rd = renderdoc->GetActiveRenderData();
		BaseContainer renderSettings = rd->GetData();
		bool has_vp = false;
		BaseVideoPost* vp = rd->GetFirstVideoPost();
		BaseContainer* data;

		while (vp != NULL && !has_vp) {
			has_vp = (vp->GetType() == ID_RENDERSETTINGS); // Look for rendersettings

			if (!has_vp) {
				vp = vp->GetNext();
			}
		}

		if (has_vp) {
			data = vp->GetDataInstance();
		}

		StatusSetText(String(""));
		parser = 0x0;
		RenderNextFrame();

	} else if (id == DL_FRAME_STOPPED) {
		StatusSetText(String(""));
		parser = 0x0;
		frames_to_render = queue<long>(); // Clear frame queue

	} else if (id == DL_STOP_RENDERING) {
		if (parser) {
			parser->GetContext().RenderControl(NSI::CStringPArg("action", "stop"));
		}
	}

	return TRUE;
}

Bool RegisterRenderManager(void)
{
	return RegisterMessagePlugin(
			   DL_RENDER_MANAGER, String(), 0, NewObjClear(RenderManager));
}