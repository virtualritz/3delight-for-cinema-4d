#ifndef NSI_C4D_ERROR_HANDLER
#define NSI_C4D_ERROR_HANDLER

void NSIErrorHandlerC4D(void* userdata, int level, int code, const char* message);

#endif
