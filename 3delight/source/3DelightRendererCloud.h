#pragma once

#include "3DelightRenderer.h"
#include "DL_HookPlugin.h"
#include "DL_TranslatorPlugin.h"

class RenderSettingsCloud : public RenderSettings
{
public:
	static NodeData* Alloc(void)
	{
		return NewObj(RenderSettingsCloud) iferr_ignore("Wrong Instance");
	}

	virtual Bool GetDDescription(GeListNode* i_node,
								 Description* i_description,
								 DESCFLAGS_DESC& i_flags);
	virtual Bool Message(GeListNode* i_node, Int32 i_type, void* i_data);
	bool Render3Dl(BaseDocument* doc,
				   long frame,
				   RENDER_MODE mode,
				   bool progressive,
				   BaseContainer* settings);
};
