#ifndef EXPAND_FILENAME_H
#define EXPAND_FILENAME_H

#include <string>

void ExpandFilename(std::string& filename,
					long frame,
					std::string document_path = "",
					std::string document_name = "",
					std::string aov = "");

#endif
