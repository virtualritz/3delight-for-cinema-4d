#pragma once

#include <string.h>
#include "BitmapDisplayDriver.h"
#include "CameraHook.h"
#include "DL_StandIn_Translator.h"
#include "DisplayTranslator.h"
#include "IDs.h"
#include "NSIAPI.h"
#include "PluginManager.h"
#include "RenderOptionsHook.h"
#include "VisibilityTagTranslator.h"
#include "c4d.h"
#include "c4d_gui.h"

PluginManager PM;

Bool RegisterRenderFrame(void);
Bool RegisterRenderSequence(void);
// Bool RegisterInteractiveRenderManager(void);
Bool RegisterDisplay(void);
// Bool RegisterImageLayer(void);
Bool RegisterDL_CameraTag(void);
Bool RegisterDL_MotionBlurTag(void);
// Bool RegisterDL_SubdivisionSurfaceTag(void);

Bool RegisterDL_VisibilityTag(void);
Bool RegisterDL_SubdivisionSurfaceTag(void);
Bool Register3DelightPlugin(void);
Bool Register3DelightCloudPlugin(void);
Bool RegisterCustomListView(void);
Bool RegisterCustomMultiLight(void);
Bool RegisterInteractiveRenderingStart(void);
Bool RegisterInteractiveRenderingStop(void);
Bool RegisterInteractiveRenderManager(void);
Bool RegisterRenderManager(void);
Bool Register3DelightCommand(void);
Bool Register3DelightBatchRenderCommand(void);
Bool RegisterNSIExportCommand(void);
Bool RegisterCreateShelf(void);
Bool RegisterAbout(void);
Bool RegisterHelp(void);
Bool RegisterPreferences();
Bool RegisterStandinNode();
Bool RegisterStandInExport();