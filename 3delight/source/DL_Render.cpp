#include "DL_render.h"
#include <vector>
#include "DL_TypeConversions.h"
#include "IDs.h"
#include "SceneParser.h"
#include "dl_preferences.h"
#include "dlrendersettings.h"
using namespace std;

void FrameStoppedCallback(void* stoppedcallbackdata, NSIContext_t ctx, int status)
{
	// End context when rendering is completed (or stopped)
	SceneParser* sp = (SceneParser*) stoppedcallbackdata;
	delete sp;

	if (status == NSIRenderAborted) {
		// Render was stopped before completion
		SpecialEventAdd(DL_FRAME_STOPPED);

	} else {
		// Render was succesful, tell rendermanager to continue rendering any
		// remaining frames
		SpecialEventAdd(DL_FRAME_COMPLETED);
	}
}

void NSIErrorHandlerForC4D(void* userdata, int level, int code, const char* message)
{
	const char* pre = (const char*) userdata;
	std::string buffer("3Delight");

	if (pre) {
		buffer += " (" + std::string(pre) + "): ";

	} else {
		buffer += ": ";
	}

	buffer += message;

	switch (level) {
	case NSIErrMessage:
		ApplicationOutput(buffer.c_str());
		break;

	case NSIErrInfo:
		ApplicationOutput(buffer.c_str());
		break;

	case NSIErrWarning:
		ApplicationOutput(buffer.c_str());
		break;

	default:
	case NSIErrError:
		ApplicationOutput(buffer.c_str());
		break;
	}
}

bool DL_RenderFrame(BaseDocument* doc,
					long frame,
					RENDER_MODE mode,
					bool progressive)
{
	BaseDocument* renderdoc =
		(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
	RenderData* rd = doc->GetActiveRenderData();
	BaseContainer renderSettings = rd->GetData();
	BaseContainer* bc =
		GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

	if (!bc) {
		GetWorldContainerInstance()->SetContainer(PREFERENCES_ID, BaseContainer());
		bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);
	}

	Int32 Used_Renderer = renderSettings.GetInt32(RDATA_RENDERENGINE);
	SceneParser* sp = new SceneParser(renderdoc);
	NSI::ArgumentList arglist;
	NSIErrorHandler_t eh = NSIErrorHandlerForC4D;

	if (Used_Renderer == ID_RENDERSETTINGS_CLOUD) {
		arglist.Add(new NSI::IntegerArg("cloud", 1));
	}

	arglist.Add(new NSI::PointerArg("errorhandler", (void*) eh));
	sp->GetContext().Begin(arglist);
	sp->SetRenderMode(mode);
	// Render scene
	bool RenderOK = sp->InitScene(true, frame);
	sp->SampleFrameMotion(frame);
	BaseDocument::Free(renderdoc);
	int live_pixel = bc->GetInt32(DL_LIVE_RENDER_COARSENESS);
	int scanning = bc->GetInt32(DL_SCANNING);
	std::string bucket_order = "";

	switch (scanning) {
	case 0:
		bucket_order = "circle";
		break;

	case 1:
		bucket_order = "spiral";
		break;

	case 2:
		bucket_order = "horizontal";
		break;

	case 3:
		bucket_order = "vertical";
		break;

	case 4:
		bucket_order = "zigzag";
		break;

	default:
		bucket_order = "horizontal";
		break;
	}

	sp->GetContext().SetAttribute(
		NSI_SCENE_GLOBAL,
		(NSI::IntegerArg("renderatlowpriority", 1),
		 NSI::StringArg("bucketorder", bucket_order),
		 NSI::IntegerArg("live.pixelsize", live_pixel)));
	sp->GetContext().RenderControl(
		(NSI::StringArg("action", "start"),
		 NSI::IntegerArg("progressive", 0),
		 NSI::PointerArg("stoppedcallback", (void*) &FrameStoppedCallback),
		 NSI::PointerArg("stoppedcallbackdata", (void*) sp)));
	sp->GetContext().RenderControl(NSI::CStringPArg("action", "wait"));
	return RenderOK;
	/*RenderData* rd=doc->GetActiveRenderData();
	    BaseVideoPost* vp=rd->GetFirstVideoPost();
	    bool found=false;
	    while(vp!=NULL && !found){
	            found=(vp->GetType()==ID_RENDERSETTINGS); //Look for
	   rendersettings if(!found){ vp=vp->GetNext();
	            }
	    }
	    if(!found){
	            BaseVideoPost* pvp=BaseVideoPost::Alloc(ID_RENDERSETTINGS);
	            rd->InsertVideoPostLast(pvp);
	            vp=rd->GetFirstVideoPost();
	            while(vp!=NULL && !found){
	                    found=(vp->GetType()==ID_RENDERSETTINGS); //Look for
	   rendersettings if(!found){ vp=vp->GetNext();
	                    }
	            }
	    }
	    if(!found){
	            GePrint("Could not find render settings"_s);
	            return false;
	    }
	    BaseDocument* renderdoc =
	   (BaseDocument*)doc->GetClone(COPYFLAGS::DOCUMENT,nullptr); rd =
	   renderdoc->GetActiveRenderData(); vp = rd->GetFirstVideoPost();
	    BaseContainer render_data = rd->GetData();
	    BaseContainer vp_data = vp->GetData();
	    SceneParser sp;
	    sp.SetRenderMode(mode);
	    //Motion sampling
	    long motionSamples = 2;
	    bool useMotionBlur=vp_data.GetBool(DL_MOTION_BLUR);
	    //long motionSamples=vp_data.GetInt32(DL_MOTION_SAMPLES,2);
	    if(!useMotionBlur){ motionSamples=1; }
	    float ShutterAngle=vp_data.GetFloat(DL_SHUTTER_ANGLE);
	    long fps=doc->GetFps();
	    float shutterOpen=float(frame)/float(fps);
	    float shutterClose=float(frame+ShutterAngle)/float(fps);
	    sp.SetMotionSamples(motionSamples);
	    sp.SetShutter(shutterOpen,shutterClose);
	    //Render scene
	    bool RenderOK=	sp.Parse(renderdoc, frame);
	    BaseDocument::Free(renderdoc);
	    return RenderOK; */
}
