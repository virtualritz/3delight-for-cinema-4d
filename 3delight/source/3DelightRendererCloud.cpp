#include "3DelightRendererCloud.h"
#include <assert.h>
#include <algorithm>
#include <iostream>
#include "DL_Render.h"
#include "ErrorHandler.h"
#include "IDs.h"
#include "SceneParser.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "customgui_inexclude.h"
#include "dl_preferences.h"
#include "dlrendersettingscloud.h"
#include "nsi.hpp"

void FrameStoppedCallbckCloud(void* stoppedcallbackdata,
							  NSIContext_t ctx,
							  int status)
{
	SceneParser* sp = (SceneParser*) stoppedcallbackdata;
	delete sp;
}

bool RenderSettingsCloud::Render3Dl(BaseDocument* doc,
									long frame,
									RENDER_MODE mode,
									bool progressive,
									BaseContainer* settings)
{
	String action = settings->GetString(DL_ISCLICKED);
	NSI::ArgumentList arglist;
	NSIErrorHandler_t eh = NSIErrorHandlerC4D;
	BaseContainer* bc =
		GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);

	if (!bc) {
		GetWorldContainerInstance()->SetContainer(PREFERENCES_ID, BaseContainer());
		bc = GetWorldContainerInstance()->GetContainerInstance(PREFERENCES_ID);
	}

	if (action == "Render") {
		arglist.Add(new NSI::IntegerArg("cloud", 1));
		arglist.Add(new NSI::PointerArg("errorhandler", (void*) eh));

	} else if (action == "Export") {
		String file = settings->GetFilename(DL_FOLDER_OUTPUT).GetString();
		std::string exported = file.GetCStringCopy();
		const char* output = exported.c_str();
		arglist.Add(new NSI::IntegerArg("cloud", 1));
		arglist.Add(new NSI::CStringPArg("streamfilename", output));
	}

	BaseDocument* renderdoc =
		(BaseDocument*) doc->GetClone(COPYFLAGS::DOCUMENT, nullptr);
	SceneParser* sp = new SceneParser(renderdoc);
	sp->GetContext().Begin(arglist);
	sp->SetRenderMode(mode);
	// Render scene
	bool RenderOK = sp->InitScene(true, frame);
	sp->SampleFrameMotion(frame);
	BaseDocument::Free(renderdoc);
	int live_pixel = bc->GetInt32(DL_LIVE_RENDER_COARSENESS);
	int scanning = bc->GetInt32(DL_SCANNING);
	std::string bucket_order = "";

	switch (scanning) {
	case 0:
		bucket_order = "circle";
		break;

	case 1:
		bucket_order = "spiral";
		break;

	case 2:
		bucket_order = "horizontal";
		break;

	case 3:
		bucket_order = "vertical";
		break;

	case 4:
		bucket_order = "zigzag";
		break;

	default:
		bucket_order = "horizontal";
		break;
	}

	sp->GetContext().SetAttribute(
		NSI_SCENE_GLOBAL,
		(NSI::IntegerArg("renderatlowpriority", 1),
		 NSI::StringArg("bucketorder", bucket_order),
		 NSI::IntegerArg("live.pixelsize", live_pixel)));
	sp->GetContext().RenderControl(
		(NSI::StringArg("action", "start"),
		 NSI::IntegerArg("progressive", 0),
		 NSI::PointerArg("stoppedcallback", (void*) &FrameStoppedCallbckCloud),
		 NSI::PointerArg("stoppedcallbackdata", (void*) sp)));
	return RenderOK;
}

/**
        Gets the currently used parameter description of the entity
        which in this case is the plugin with Id ID_RENDERSETTINGS_CLOUD.
*/
Bool RenderSettingsCloud::GetDDescription(GeListNode* i_node,
		Description* i_description,
		DESCFLAGS_DESC& i_flags)
{
	i_description->LoadDescription(ID_RENDERSETTINGS_CLOUD);
	i_flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer names;
	names.SetString(0, "Render"_s);
	names.SetString(1, "Export to NSI File..."_s);
	AddCycleButton(i_description, RENDER_CYCLEBUTTON, DescID(RENDER), "", names);
	AddEmpty(i_description, RENDER_CYCLEBUTTON + 1, DescID(RENDER));
	AddEmpty(i_description, RENDER_CYCLEBUTTON + 2, DescID(RENDER));
	BaseContainer* bc = ((BaseTag*) i_node)->GetDataInstance();
	Bool OPEN_EXR_FORMAT =
		(bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR || bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DEEP);
	Bool TIFF_FORMAT = bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_TIFF;
	Bool JPG_FORMAT = bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_JPEG;
	Bool PNG_FORMAT = bc->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_PNG;

	/**
	            Based on the selected Image format we use ShowDescriptionElement
	   function which is implemented above to hide the dropdowns that does not
	   correspond with the selected format, and show just the one which
	   represents the selected image format. A description contains information
	   about the parameters of a node object.
	    */
	if (OPEN_EXR_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, OPEN_EXR_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, !OPEN_EXR_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, !OPEN_EXR_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, !OPEN_EXR_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT));

	} else if (TIFF_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, !TIFF_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, TIFF_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, !TIFF_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, !TIFF_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT_TIFF));

	} else if (JPG_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, !JPG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, !JPG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, !JPG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, JPG_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT_JPG));

	} else if (PNG_FORMAT) {
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT, !PNG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_TIFF, !PNG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_PNG, PNG_FORMAT);
		ShowDescriptionElement(
			i_node, i_description, DL_DEFAULT_IMAGE_OUTPUT_JPG, !PNG_FORMAT);
		bc->SetInt32(DL_DEFAULT_IMAGE_BITDEPTH,
					 bc->GetInt32(DL_DEFAULT_IMAGE_OUTPUT_PNG));
	}

	return TRUE;
}

Bool RenderSettingsCloud::Message(GeListNode* i_node, Int32 i_type, void* i_data)
{
	switch (i_type) {
	case MSG_DESCRIPTION_COMMAND: {
		DescriptionCommand* dc = (DescriptionCommand*) i_data;
		BaseContainer* dldata = ((BaseVideoPost*) i_node)->GetDataInstance();
		BaseDocument* doc = GetActiveDocument();
		BaseTime t = doc->GetTime();

		if (dc->_descId[0].id == RENDER_CYCLEBUTTON) {
			Int32 action = dldata->GetInt32(RENDER_CYCLEBUTTON);
			long frame = t.GetFrame(doc->GetFps());

			if (action == 0) { // If render button is clicked
				dldata->SetString(DL_ISCLICKED, "Render"_s);
				Render3Dl(doc, frame, DL_INTERACTIVE, true, dldata);
			}

			// If Export to NSI File button is clicked
			else if (action == 1) {
				dldata->SetString(DL_ISCLICKED, "Export"_s);

				if (dldata->GetFilename(DL_FOLDER_OUTPUT).GetString() == "") {
					Filename folder;
					String directory = GeGetStartupApplication().GetString() + "/NSI";
					folder.SetDirectory(directory);
					folder.SetSuffix("nsi"_s);
					folder.FileSelect(
						FILESELECTTYPE::ANYTHING, FILESELECT::SAVE, "Select Folder"_s);
					dldata->SetFilename(DL_FOLDER_OUTPUT, folder);
				}

				Render3Dl(doc, frame, DL_INTERACTIVE, true, dldata);
			}
		}
	}
	}

	return true;
}

Bool Register3DelightCloudPlugin(void)
{
	RegisterIcon(DL_DISPLAY_ON, GeGetPluginResourcePath() + "display-on.png");
	RegisterIcon(DL_DISPLAY_OFF, GeGetPluginResourcePath() + "display.png");
	RegisterIcon(DL_FOLDER_ON, GeGetPluginResourcePath() + "folder-on.png");
	RegisterIcon(DL_FOLDER_OFF, GeGetPluginResourcePath() + "folder.png");
	RegisterIcon(DL_JPG_ON, GeGetPluginResourcePath() + "jpg-on.png");
	RegisterIcon(DL_JPG_OFF, GeGetPluginResourcePath() + "jpg.png");
	return RegisterVideoPostPlugin(ID_RENDERSETTINGS_CLOUD,
								   "3Delight Cloud"_s,
								   PLUGINFLAG_VIDEOPOST_ISRENDERER,
								   RenderSettingsCloud::Alloc,
								   "dlrendersettingscloud"_s,
								   0,
								   0);
}
