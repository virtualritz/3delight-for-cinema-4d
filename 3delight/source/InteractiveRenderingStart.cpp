#include "IDs.h"
#include "c4d.h"

class InteractiveRenderingStart : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool InteractiveRenderingStart::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	SpecialEventAdd(DL_START_INTERACTIVE_RENDERING);
	return true;
}

Bool RegisterInteractiveRenderingStart(void)
{
	return RegisterCommandPlugin(ID_INTERACTIVE_RENDERING_START,
								 "Start Interactive Rendering"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 AutoBitmap("shelf_launchIPR_200.png"_s),
								 String(),
								 NewObjClear(InteractiveRenderingStart));
}