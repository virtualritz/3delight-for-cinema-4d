#include "IDs.h"
#include "SceneParser.h"
#include "c4d.h"
#include "nsi.hpp"

void InteractiveFrameStoppedCallback(void* stoppedcallbackdata,
									 NSIContext_t ctx,
									 int status)
{
	/*
	            End context when rendering is stopped. We are only taking in
	            consideration when rendering is stopped since we are on
	            interactive rendering.
	    */
	SceneParser* sp = (SceneParser*) stoppedcallbackdata;
	delete sp;

	if (status == NSIRenderAborted) {
		SpecialEventAdd(DL_INTERACTIVE_STOPPED);
	}
}

class InteractiveRenderManager : public MessageData
{
public:
	InteractiveRenderManager();
	~InteractiveRenderManager();

	virtual Bool CoreMessage(Int32 id, const BaseContainer& bc);

private:
	SceneParser* parser;
};

InteractiveRenderManager::InteractiveRenderManager()
	: parser(0x0) {}

InteractiveRenderManager::~InteractiveRenderManager()
{
	delete (parser);
}

Bool InteractiveRenderManager::CoreMessage(Int32 id, const BaseContainer& bc)
{
	if (id == EVMSG_CHANGE && parser != nullptr) {
		// Update interactive render
		parser->InteractiveUpdate();
		parser->GetContext().RenderControl(
			(NSI::StringArg("action", "synchronize")));

	} else if (id == DL_START_INTERACTIVE_RENDERING) {
		RenderData* rd = GetActiveDocument()->GetActiveRenderData();
		BaseContainer renderSettings = rd->GetData();
		BaseDocument* doc = GetActiveDocument();

		if (parser) {
			delete parser;
		}

		parser = new SceneParser(doc);
		parser->GetContext().Begin();
		parser->SetRenderMode(DL_IPR);
		parser->InitScene(false, 0);
		parser->InteractiveUpdate();
		NSI::Context& ctx(parser->GetContext());
		ctx.SetAttribute(NSI_SCENE_GLOBAL,
						 (NSI::IntegerArg("renderatlowpriority", 1),
						  NSI::StringArg("bucketorder", "circle")));
		ctx.RenderControl(
			(NSI::StringArg("action", "start"),
			 NSI::IntegerArg("interactive", 1),
			 NSI::PointerArg("stoppedcallback",
							 (void*) &InteractiveFrameStoppedCallback)));

	} else if (id == DL_STOP_RENDERING || id == DL_INTERACTIVE_STOPPED) {
		delete parser;
		parser = nullptr;
	}

	return TRUE;
}

Bool RegisterInteractiveRenderManager(void)
{
	return RegisterMessagePlugin(DL_INTERACTIVE_RENDER_MANAGER,
								 String(),
								 0,
								 NewObjClear(InteractiveRenderManager));
}