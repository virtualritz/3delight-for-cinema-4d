#include "3DelightRenderer.h"
#include "DL_Render.h"
#include "IDs.h"
#include "SceneParser.h"
#include "c4d.h"
#include "nsi.hpp"

class DL_3DelightBatchRender_command : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_3DelightBatchRender_command::Execute(BaseDocument* doc,
		GeDialog* parentManager)
{
	SpecialEventAdd(DL_START_BATCH_RENDER);
	return true;
}

Bool Register3DelightBatchRenderCommand(void)
{
	return RegisterCommandPlugin(ID_BATCH_RENDER_COMMAND,
								 "Batch Render"_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 AutoBitmap("shelf_launchRender_200.png"_s),
								 String("Batch Render With 3Delight"_s),
								 NewObjClear(DL_3DelightBatchRender_command));
}
