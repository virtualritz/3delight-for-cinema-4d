#include "RenderOptionsHook.h"
#include <assert.h>
#include <string.h>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <tuple>
#include "DLAOVsColor.h"
#include "DL_TypeConversions.h"
#include "ExpandFilename.h"
#include "IDs.h"
#include "PluginManager.h"
#include "customgui_inexclude.h"
#include "dlrendersettings.h"
#include "nsi.hpp"

#define DL_CUSTOM_COLORS 1008

using namespace std;

extern PluginManager PM;

BaseObject*
GetNextObject(BaseObject* op)
{
	if (!op) {
		return NULL;
	}

	if (op->GetDown()) {
		return op->GetDown();
	}

	while (!op->GetNext() && op->GetUp()) {
		op = op->GetUp();
	}

	return op->GetNext();
}

vector<BaseObject*>
GetCustomAOV(BaseDocument* doc)
{
	vector<BaseObject*> custom_aovs;
	// Find light groups in scene
	BaseObject* aov_obj = doc->GetFirstObject();

	while (aov_obj) {
		if (aov_obj->GetType() == ID_AOV_GROUP && aov_obj->GetDeformMode()) { // Only look for enabled a groups
			BaseContainer* data = aov_obj->GetDataInstance();

			if (data->GetBool(1012) == TRUE) {
				custom_aovs.push_back(aov_obj);
			}
		}

		aov_obj = GetNextObject(aov_obj);
	}

	return custom_aovs;
}

vector<BaseObject*>
getLights(BaseDocument* doc)
{
	vector<BaseObject*> lights;
	set<BaseList2D*> lights_in_groups;
	// Find light groups in scene
	BaseObject* object = doc->GetFirstObject();

	while (object) {
		if (object->GetType() == ID_LIGHTGROUP && object->GetDeformMode()) { // Only look for enabled light groups
			// ApplicationOutput("Light group: " + object->GetName());
			BaseContainer* data = object->GetDataInstance();
			InExcludeData* lightlist = (InExcludeData*) data->GetCustomDataType(
										   1000, CUSTOMDATATYPE_INEXCLUDE_LIST); // LIGHTGROUP_LIGHTS=1000

			if (lightlist) {
				Int32 nlights = lightlist->GetObjectCount();

				if (nlights > 0) // count the light_set as separated lights only
					// if at least one light source is being added.
					// Thus you will avoid getting a layer with no lights at
					// all.
				{
					lights.push_back(object);
				}

				for (int i = 0; i < nlights; i++) {
					BaseList2D* light = lightlist->ObjectFromIndex(doc, i);
					// ApplicationOutput("Light: " + light->GetName());
					lights_in_groups.insert(light);
				}
			}
		}

		object = GetNextObject(object);
	}

	// Find all lights that are not already in a light group
	object = doc->GetFirstObject();

	while (object) {
		if (PM.IsLight(object)) {
			if (lights_in_groups.find((BaseList2D*) object) == lights_in_groups.end()) { // Don't add light if it belongs to a group
				lights.push_back(object);
			}

			// else {
			//	ApplicationOutput("Light is in group: " + object->GetName());
			//}
		}

		object = GetNextObject(object);
	}

	// ApplicationOutput(String("number of light layers: ") +
	// String::IntToString(int(lights.size())));
	return lights;
}

/*
        Function to get the selected lights from the multilights UI.
*/
/*vector<BaseObject*> getSelectedLights(BaseDocument* doc,vector<Int32>
objectID,maxon::Int all_multi_lights)
{
        vector<BaseObject*> light_list;

        BaseObject* object= doc->GetFirstObject();
        if (!object)
                return light_list;
        bool process = true;
        Int32 Position = 2;
        int lights_size = (int)objectID.size();

        //checking if environment is used and selected.
        bool skyUsed = false;
        for (int i = 0; i < objectID.size(); i++)
        {
                if (objectID[i] == 0)
                {
                        skyUsed = true;
                }
        }


        BaseObject *envShader = NULL;

        while (object)
        {
                if (process && PM.IsLight(object) &&
object->GetType()!=ID_ENVIRONMENTLIGHT)
                {
                        for (int i = 0; i <lights_size; i++)
                        {

                                        //Finding the objects that are selected
in the Multi_light UI
                                        //objectID contains the ID of the
selected objects in the UI and
                                        //now we are getting the corresponing
object for that ID

                                if(objectID[i]==Position)
                                        light_list.push_back(object);

                        }
                        Position++;
                }


                if (process && object->GetType() == ID_ENVIRONMENTLIGHT)
                {
                        if (skyUsed)
                                envShader = object;
                }
                if (object->GetDown() && process)
                {
                        object = object->GetDown();
                        process = true;
                }

                else if (object->GetNext())
                {
                        object = object->GetNext();
                        process = true;
                }

                else if (object->GetUp())
                {
                        object = object->GetUp();
                        process = false;
                }
                else object = NULL;
        }

        //add environment shader to the beginning of the list if exists.
        if (envShader!=NULL)
                light_list.insert(light_list.begin(), envShader);

        return light_list;
}*/

struct DL_LayersData {
	std::string name;
	std::string shortName;
	std::string varName;
	std::string varSource;
	std::string varType;
	bool isShadingComponent;
};

std::vector<DL_LayersData> aov_layers_list = {
	{ "RGBA (color + alpha)", "RGBA", "Ci", "shader", "color", true },
	{ "RGBA (direct)", "directrgba", "Ci.direct", "shader", "color", true },
	{ "RGBA (indirect)", "indirectrgba", "Ci.indirect", "shader", "color", true },
	{ "Diffuse", "Diffuse", "diffuse", "shader", "color", true },
	{
		"Diffuse (direct)",
		"directdiffuse",
		"diffuse.direct",
		"shader",
		"color",
		true
	},
	{
		"Diffuse (indirect)",
		"directdiffuse",
		"diffuse.indirect",
		"shader",
		"color",
		true
	},
	{ "Subsurface scattering", "SSS", "subsurface", "shader", "color", true },
	{ "Reflection", "Reflection", "reflection", "shader", "color", true },
	{
		"Reflection (direct)",
		"directreflection",
		"reflection.direct",
		"shader",
		"color",
		true
	},
	{
		"Reflection (indirect)",
		"indirectreflection",
		"reflection.indirect",
		"shader",
		"color",
		true
	},
	{ "Refraction", "Refraction", "refraction", "shader", "color", true },
	{
		"Volume Scattering",
		"VolumeScattering",
		"volume",
		"shader",
		"color",
		true
	},
	{
		"Incandescence",
		"Incandescence",
		"incandescence",
		"shader",
		"color",
		true
	},
	{ "Toon Base", "ToonBase", "toon_base", "shader", "color", true },
	{ "Toon Diffuse", "ToonDiffuse", "toon_diffuse", "shader", "color", true },
	{ "Toon Specular", "ToonSpecular", "toon_specular", "shader", "color", true },
	{ "Albedo", "Albedo", "albedo", "shader", "color", false },
	{ "Z (depth)", "Z", "z", "builtin", "scalar", false },
	{ "Camera space position", "P", "P.camera", "builtin", "vector", false },
	{ "Camera space normal", "N", "N.camera", "builtin", "vector", false },
	{ "UV", "UV", "uv", "builtin", "vector", false },
	{
		"Geometry Cryptomatte",
		"GeometryCryptomatte",
		"id.geometry",
		"builtin",
		"scalar",
		false
	},
	{
		"Scene Path Cryptomatte",
		"ScenePathCryptomatte",
		"id.scenepath",
		"builtin",
		"scalar",
		false
	},
	{
		"Surface Shader Cryptomatte",
		"SurfaceShaderCryptomatte",
		"id.surfaceshader",
		"builtin",
		"scalar",
		false
	},
	{
		"Relighting Multiplier",
		"RelightingMultiplier",
		"relighting_multiplier",
		"shader",
		"color",
		false
	},
	{
		"Relighting Reference",
		"RelightingReference",
		"relighting_reference",
		"shader",
		"color",
		false
	},
	{
		"Motion Vector",
		"MotionVector",
		"motionvector",
		"builtin",
		"vector",
		false
	}
};

DL_LayersData
GetLayerData(String layer)
{
	DL_LayersData data;

	for (int i = 0; i < aov_layers_list.size(); i++) {
		if (layer == String(aov_layers_list[i].name.c_str())) {
			data = aov_layers_list[i];
		}
	}

	return data;
}

// Get the Filter to be used on nsi for the selectedFilter on the render
// settings
std::string
getFilter(Int32 i_id)
{
	std::string filter;

	switch (i_id) {
	case DL_BLACKMAN_HARRIS:
		filter = "blackman-harris";
		break;

	case DL_MITCHELL:
		filter = "mitchell";
		break;

	case DL_CATMULL_ROM:
		filter = "catmull-rom";
		break;

	case DL_SINC:
		filter = "sinc";
		break;

	case DL_BOX:
		filter = "box";
		break;

	case DL_TRIANGLE:
		filter = "triangle";
		break;

	case DL_GAUSSIAN:
		filter = "gaussian";
		break;

	default:
		filter = "blackman-harris";
		break;
		break;
	}

	assert(filter != "");
	return filter;
}

std::tuple<string, string, string>
OutputLayer(DL_SceneParser* i_parser)
{
	BaseContainer* settings = i_parser->GetSettings();
	string extension;
	string output_format;
	string scalar_format;

	if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR || settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR_DEEP) {
		extension = ".exr";

		if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_OPEN_EXR) {
			output_format = "exr";

		} else {
			output_format = "deepexr";
		}

		if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_SIXTEEN_BIT_FLOAT) {
			scalar_format = "half";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_THIRTYTWO_BIT) {
			scalar_format = "float";
		}

	} else if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_TIFF) {
		extension = ".tif";
		output_format = "tiff";

		if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_EIGHT_BIT) {
			scalar_format = "uint8";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_SIXTEEN_BIT) {
			scalar_format = "uint16";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_THIRTYTWO_BIT) {
			scalar_format = "float";
		}

	} else if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_PNG) {
		extension = ".png";
		output_format = "png";

		if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_EIGHT_BIT) {
			scalar_format = "uint8";

		} else if (settings->GetInt32(DL_DEFAULT_IMAGE_BITDEPTH) == DL_SIXTEEN_BIT) {
			scalar_format = "uint16";
		}

	} else if (settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT) == DL_FORMAT_JPEG) {
		extension = ".jpg";
		output_format = "jpeg";
		scalar_format = "uint8";
	}

	// assert( extension != "" && output_format != "" && scalar_format != "" );
	return std::make_tuple(extension, output_format, scalar_format);
}

/**
        This function is executed each time we render a scene on 3Delight
        Here we get the information from the render settings and according to
        the retrieved information we pass these values with on nsi based on its
   criteria.
*/
void RenderOptionsHook::CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseContainer* settings = parser->GetSettings();
	RENDER_MODE mode = parser->GetRenderMode();

	if (mode == DL_STANDIN_EXPORT) {
		return;
	}

	const CustomDataType* dt =
		settings->GetCustomDataType(DL_CUSTOM_AOV_LAYER, ID_CUSTOMDATATYPE_LAYERS);
	const CustomDataType* multi_light_dt = settings->GetCustomDataType(
			DL_CUSTOM_MULTI_LIGHT, ID_CUSTOMDATATYPE_MULTILIGHTS);
	iCustomDataTypeLayers* data = (iCustomDataTypeLayers*) (dt);
	iCustomDataTypeLights* multi_light_data =
		(iCustomDataTypeLights*) (multi_light_dt);

	if (!data) {
		data = NewObjClear(iCustomDataTypeLayers);
		(void) data->m_selected_layers.Append(String("RGBA (color + alpha)"));
		(void) data->m_selected_id.Append(50);
		(void) data->m_output_layer.Append(TRUE);
	}

	if (!multi_light_data) {
		multi_light_data = NewObjClear(iCustomDataTypeLights);
	}

	double samplingReduceFactor = true;
	bool use_displacement = true;
	bool use_subsurface = true;
	bool disable_image_layer = false;
	bool use_atmosphere = true;
	bool use_multiple_scattering = true;

	if (settings->GetBool(DL_ENABLE_INTERACTIVE_PREVIEW) == true && mode != C4D_PREVIEW) {
		int sampling = settings->GetInt32(DL_SAMPLING);

		if (sampling == DL_ONE_PERCENT) {
			samplingReduceFactor = 0.01;

		} else if (sampling == DL_FOUR_PERCENT) {
			samplingReduceFactor = 0.04;

		} else if (sampling == DL_TEN_PERCENT) {
			samplingReduceFactor = 0.10;

		} else if (sampling == DL_TWENTYFIVE_PERCENT) {
			samplingReduceFactor = 0.25;

		} else if (sampling == DL_HUNDRED_PERCENT) {
			samplingReduceFactor = 1;
		}

		use_displacement = !settings->GetBool(DL_DISABLE_DISPLACEMENT);
		use_subsurface = !settings->GetBool(DL_DISABLE_SUBSURFACE);
		disable_image_layer = settings->GetBool(DL_DISABLE_IMAGE_LAYER);
		use_atmosphere = !settings->GetBool(DL_DISABLE_ATMOSPHERE);
		use_multiple_scattering = !settings->GetBool(DL_DISABLE_MULTIPLE_SCATTER);
	}

	int shading_samples = max(
							  1,
							  (int) (settings->GetInt32(DL_SHADING_SAMPLES) * samplingReduceFactor + 0.5));
	int volume_samples = max(
							 1,
							 (int) (settings->GetInt32(DL_VOLUME_SAMPLES) * samplingReduceFactor + 0.5));
	int diffuse_depth = settings->GetInt32(DL_MAX_DIFFUSE_DEPTH);
	int reflection_depth = settings->GetInt32(DL_MAX_REFLECTION_DEPTH);
	int refraction_depth = settings->GetInt32(DL_MAX_REFRACTION_DEPTH);
	int hair_depth = settings->GetInt32(DL_MAX_HAIR_DEPTH);
	double max_distance = settings->GetFloat(DL_MAX_DISTANCE);
	ctx.SetAttribute(
		NSI_SCENE_GLOBAL,
		(NSI::IntegerArg("quality.shadingsamples", shading_samples),
		 NSI::IntegerArg("quality.volumesamples", volume_samples),
		 NSI::IntegerArg("maximumraydepth.diffuse", diffuse_depth),
		 NSI::IntegerArg("maximumraydepth.reflection", reflection_depth),
		 NSI::IntegerArg("maximumraydepth.refraction", refraction_depth),
		 NSI::IntegerArg("maximumraydepth.hair", hair_depth)));
	ctx.SetAttribute(NSI_SCENE_GLOBAL,
					 (NSI::DoubleArg("maximumraylength.specular", max_distance),
					  NSI::DoubleArg("maximumraylength.diffuse", max_distance),
					  NSI::DoubleArg("maximumraylength.reflection", max_distance),
					  NSI::DoubleArg("maximumraylength.refraction", max_distance),
					  NSI::DoubleArg("maximumraylength.hair", max_distance)));
	ctx.SetAttribute(NSI_SCENE_GLOBAL,
					 (NSI::IntegerArg("show.displacement", use_displacement),
					  NSI::IntegerArg("show.osl.subsurface", use_subsurface),
					  NSI::IntegerArg("show.atmosphere", use_atmosphere),
					  NSI::DoubleArg("show.multiplescattering",
									 (double) use_multiple_scattering)));
	string filter_output = getFilter(settings->GetInt32(DL_PIXEL_FILTER));
	int m_layer_number = (int) data->m_selected_layers.GetCount();
	int sortkey = 0;
	auto layeroutput = OutputLayer(parser);
	m_extension = get<0>(layeroutput);
	m_output_format = get<1>(layeroutput);
	m_scalar_format = get<2>(layeroutput);
	bool render_to_display = (settings->GetBool(DISPLAY_RENDERED_IMAGES) || settings->GetBool(SAVE_AND_DISPLAY));
	bool render_to_file = settings->GetBool(SAVE_RENDERED_IMAGES) || settings->GetBool(SAVE_AND_DISPLAY);
	bool render_to_jpeg = settings->GetBool(SAVE_ADDITIONAL_JPEG);
	bool export_to_nsi = false;
	BaseTime t = doc->GetTime();
	long frame = t.GetFrame(doc->GetFps());
	// Generate zero padded frame number
	ostringstream ss;
	ss << std::setw(4) << std::setfill('0') << frame;
	string framenumber_ext = "_" + ss.str();

	// Check if we should render to display
	if (mode == C4D_PREVIEW || mode == C4D_FINAL || mode == DL_BATCH) {
		render_to_display = false;
	}

	// Check if we should render to file
	if (mode == C4D_PREVIEW || mode == C4D_FINAL || mode == DL_IPR) {
		render_to_file = false;
	}

	if (mode == DL_EXPORT_NSI) {
		export_to_nsi = true;
		render_to_display = false;
	}

	if (settings->GetBool(OUTPUT_NSI_FILES)) {
		export_to_nsi = true;
		render_to_display = false;
		render_to_file = false;
	}

	Filename fn = settings->GetFilename(DL_DEFAULT_IMAGE_FILENAME);

	if (!fn.IsPopulated()) {
		render_to_file = false;
	}

	fn.ClearSuffix();

	if (!render_to_file) {
		render_to_jpeg = false;
	}

	bool multilayer = true;
	Int32 fileformat = settings->GetInt32(DL_DEFAULT_IMAGE_FORMAT);

	if (fileformat == DL_FORMAT_JPEG || fileformat == DL_FORMAT_PNG) { // Only output multilayer file if the
		// fileformat supports this
		multilayer = false;
	}

	bool multilight = settings->GetBool(DL_MULTILIGHT);
	vector<BaseObject*> lights;

	if (multilight) {
		lights = getLights(doc);
	}

	m_file_driver_handle = "3dlfc4d::multilayerfileDriver";

	if (multilayer && (render_to_file || export_to_nsi)) { // create output driver for multilayer file
		std::string filename = StringToStdString(fn.GetString());
		filename = filename + framenumber_ext + m_extension;
		ctx.Create(m_file_driver_handle, "outputdriver");
		ctx.SetAttribute(m_file_driver_handle,
						 (NSI::StringArg("drivername", m_output_format.c_str()),
						  NSI::StringArg("imagefilename", filename.c_str())));
	}

	// create iDisplay output driver
	string displaydriver_filename;

	if (fn.IsPopulated()) {
		displaydriver_filename = StringToStdString(fn.GetFileString());

	} else {
		Filename docname = doc->GetDocumentName();
		displaydriver_filename = StringToStdString(docname.GetFileString());
	}

	std::string idisplay_driver = "idisplay";

	if (render_to_display) {
		m_display_driver_handle = "displayDriver";
		ctx.Create(m_display_driver_handle, "outputdriver");
		ctx.SetAttribute(m_display_driver_handle,
						 (NSI::StringArg("drivername", idisplay_driver),
						  NSI::StringArg("imagefilename", displaydriver_filename)));
	}

	for (int i = 0; i < m_layer_number; i++) {
		Bool is_layer_active = data->m_output_layer[i];
		DL_LayersData LayerData = GetLayerData(data->m_selected_layers[i]);
		m_aov = LayerData.varName;
		m_variable_source = LayerData.varSource;
		m_aovname = LayerData.shortName;
		m_variable_type = LayerData.varType;
		bool IsShadingComponent = LayerData.isShadingComponent;
		int with_alpha = 0;

		if (m_aov == "Ci") { // Only export alpha for the RGBA layer
			with_alpha = 1;
		}

		string aov_ext = "_" + m_aovname;

		if (m_layer_number == 1 && m_aov == "Ci") { // Don't add aov name to file name if we are only
			// rendering a single RGBA (Ci) layer
			aov_ext = "";
		}

		// Render only RGBA if Disable Extra Image Layers toggle is selected
		if (disable_image_layer && m_aov != "Ci") {
			continue;
		}

		//-------------------------------------------
		//     Render layer to JPEG
		//-------------------------------------------
		if (is_layer_active && render_to_jpeg && m_aov == "Ci") {
			m_layer_jpg = string("3dlfc4d::jpg_outputlayer") + std::to_string(i);
			ctx.Create(m_layer_jpg, "outputlayer");
			ctx.SetAttribute(
				m_layer_jpg,
				(NSI::StringArg("variablename", m_aov.c_str()),
				 NSI::StringArg("layertype", m_variable_type.c_str()),
				 NSI::StringArg("variablesource", m_variable_source.c_str()),
				 NSI::StringArg("scalarformat", "uint8"),
				 NSI::IntegerArg("withalpha", with_alpha),
				 NSI::IntegerArg("sortkey", sortkey++),
				 NSI::StringArg("filter", filter_output.c_str()),
				 NSI::StringArg("colorprofile", "srgb")));
			m_output_driver_handle =
				string("3dlfc4d::jpg_driver") + std::to_string(i);
			ctx.Create(m_output_driver_handle, "outputdriver");
			std::string filename = StringToStdString(fn.GetString());
			filename = filename + aov_ext + framenumber_ext + ".jpg";
			ctx.SetAttribute(m_output_driver_handle,
							 (NSI::StringArg("drivername", "jpeg"),
							  NSI::StringArg("imagefilename", filename.c_str())));
			ctx.Connect(m_output_driver_handle, "", m_layer_jpg, "outputdrivers");
			ctx.Connect(
				m_layer_jpg, "", "3dlfc4d::scene_camera_screen", "outputlayers");

			// Multi light.
			if (multilight && IsShadingComponent) { // Only export multilight layers
				// for shading components
				int lights_size = (int) lights.size();
				string light_handle;
				string multilight_layer_handle;

				for (int j = 0; j < lights_size; j++) {
					BaseList2D* passed_light = (BaseList2D*) lights[j];

					if (passed_light) {
						multilight_layer_handle =
							string("3dlfc4d::MultiLightJpgLayer" + std::to_string(i) + string("_") + std::to_string(j));
						ctx.Create(multilight_layer_handle, "outputlayer");
						ctx.SetAttribute(
							multilight_layer_handle,
							(NSI::StringArg("variablename", m_aov.c_str()),
							 NSI::StringArg("layertype", m_variable_type.c_str()),
							 NSI::StringArg("variablesource", m_variable_source.c_str()),
							 NSI::StringArg("scalarformat", "uint8"),
							 NSI::IntegerArg("withalpha", with_alpha),
							 NSI::IntegerArg("sortkey", sortkey++),
							 NSI::StringArg("filter", filter_output.c_str()),
							 NSI::StringArg("colorprofile", "srgb")));
						light_handle = parser->GetHandleName(passed_light);

						if (passed_light->GetType() != ID_LIGHTGROUP) {
							light_handle =
								string("X_") + light_handle; // Except for light groups, we use
							// the handle of the parent
							// transform of the light
						}

						// ApplicationOutput(String(light_handle.c_str()));
						ctx.Connect(light_handle, "", multilight_layer_handle, "lightset");
						ctx.Connect(multilight_layer_handle,
									"",
									"3dlfc4d::scene_camera_screen",
									"outputlayers");
						// Create driver for light
						string light_name = StringToStdString(passed_light->GetName());
						light_name = "_" + light_name;
						string filedriver = "3dlfc4d::jpg_outputdriver" + std::to_string(i) + "_light" + std::to_string(j);
						std::string set_filename = StringToStdString(fn.GetString());
						set_filename =
							set_filename + aov_ext + light_name + framenumber_ext + ".jpg";
						ctx.Create(filedriver, "outputdriver");
						ctx.SetAttribute(
							filedriver,
							(NSI::StringArg("drivername", "jpeg"),
							 NSI::StringArg("imagefilename", set_filename.c_str())));
						ctx.Connect(
							filedriver, "", multilight_layer_handle, "outputdrivers");
					}
				}
			}

			// Draw outlines on "Ci" when Rendering on JPG
			if (m_aov == "Ci") {
				ctx.SetAttribute(m_layer_jpg, NSI::IntegerArg("drawoutlines", 1));
			}
		}

		//-------------------------------------------
		//     Render layer to file
		//-------------------------------------------
		if (is_layer_active && (render_to_file || export_to_nsi)) {
			string filedriver;

			if (multilayer) { // Render to existing multilayer driver
				filedriver = m_file_driver_handle;

			} else { // Create new driver for this layer
				filedriver = "3dlfc4d::file_outputdriver" + std::to_string(i);
				std::string filename = StringToStdString(fn.GetString());
				filename = filename + aov_ext + framenumber_ext + m_extension;
				ctx.Create(filedriver, "outputdriver");
				ctx.SetAttribute(filedriver,
								 (NSI::StringArg("drivername", m_output_format.c_str()),
								  NSI::StringArg("imagefilename", filename.c_str())));
			}

			m_layer_file = string("3dlfc4d::file_outputlayer") + std::to_string(i);
			ctx.Create(m_layer_file, "outputlayer");
			ctx.SetAttribute(
				m_layer_file,
				(NSI::StringArg("variablename", m_aov.c_str()),
				 NSI::StringArg("layertype", m_variable_type.c_str()),
				 NSI::StringArg("variablesource", m_variable_source.c_str()),
				 NSI::StringArg("scalarformat", m_scalar_format.c_str()),
				 NSI::IntegerArg("withalpha", with_alpha),
				 NSI::IntegerArg("sortkey", sortkey++),
				 NSI::StringArg("filter", filter_output.c_str())));

			if (m_scalar_format == "uint8") {
				ctx.SetAttribute(m_layer_file,
								 (NSI::StringArg("colorprofile", "srgb")));
			}

			ctx.Connect(
				m_layer_file, "", "3dlfc4d::scene_camera_screen", "outputlayers");
			ctx.Connect(filedriver, "", m_layer_file, "outputdrivers");

			// Multi light.
			if (multilight && IsShadingComponent) { // Only export multilight layers
				// for shading components
				int lights_size = (int) lights.size();
				string light_handle;
				string multilight_layer_handle;

				for (int j = 0; j < lights_size; j++) {
					BaseList2D* passed_light = (BaseList2D*) lights[j];

					if (passed_light) {
						multilight_layer_handle =
							string("3dlfc4d::MultiLightFileLayer" + std::to_string(i) + string("_") + std::to_string(j));
						ctx.Create(multilight_layer_handle, "outputlayer");
						ctx.SetAttribute(
							multilight_layer_handle,
							(NSI::StringArg("variablename", m_aov.c_str()),
							 NSI::StringArg("layertype", m_variable_type.c_str()),
							 NSI::StringArg("variablesource", m_variable_source.c_str()),
							 NSI::StringArg("scalarformat", m_scalar_format.c_str()),
							 NSI::IntegerArg("withalpha", with_alpha),
							 NSI::IntegerArg("sortkey", sortkey++),
							 NSI::StringArg("filter", filter_output.c_str())));
						light_handle = parser->GetHandleName(passed_light);

						if (passed_light->GetType() != ID_LIGHTGROUP) {
							light_handle =
								string("X_") + light_handle; // Except for light groups, we use
							// the handle of the parent
							// transform of the light
						}

						ctx.Connect(light_handle, "", multilight_layer_handle, "lightset");
						// ctx.Connect(m_display_driver_handle, "",
						// multilight_layer_handle, "outputdrivers");
						ctx.Connect(multilight_layer_handle,
									"",
									"3dlfc4d::scene_camera_screen",
									"outputlayers");

						if (multilayer) { // Use existing multilight driver
							ctx.Connect(m_file_driver_handle,
										"",
										multilight_layer_handle,
										"outputdrivers");

						} else { // Create driver for light
							string light_name = StringToStdString(passed_light->GetName());
							light_name = "_" + light_name;
							filedriver = "3dlfc4d::file_outputdriver" + std::to_string(i) + "_light" + std::to_string(j);
							std::string filename = StringToStdString(fn.GetString());
							filename =
								filename + aov_ext + light_name + framenumber_ext + m_extension;
							ctx.Create(filedriver, "outputdriver");
							ctx.SetAttribute(
								filedriver,
								(NSI::StringArg("drivername", m_output_format.c_str()),
								 NSI::StringArg("imagefilename", filename.c_str())));
							ctx.Connect(
								filedriver, "", multilight_layer_handle, "outputdrivers");
						}
					}
				}
			}

			// Draw outlines on "Ci" when Rendering on file
			if (m_aov == "Ci") {
				ctx.SetAttribute(m_layer_file, NSI::IntegerArg("drawoutlines", 1));
			}
		}

		//-------------------------------------------
		//     Render layer to iDislay
		//-------------------------------------------
		if ((is_layer_active && render_to_display)) {
			m_layer_handle = string("3dlfc4d::DisplayLayer") + std::to_string(i);
			ctx.Create(m_layer_handle, "outputlayer");
			ctx.SetAttribute(
				m_layer_handle,
				(NSI::StringArg("variablename", m_aov.c_str()),
				 NSI::StringArg("layertype", m_variable_type.c_str()),
				 NSI::StringArg("variablesource", m_variable_source.c_str()),
				 NSI::StringArg("scalarformat", m_scalar_format.c_str()),
				 NSI::IntegerArg("withalpha", with_alpha),
				 NSI::IntegerArg("sortkey", sortkey++),
				 NSI::StringArg("filter", filter_output.c_str())));
			ctx.Connect(
				m_layer_handle, "", "3dlfc4d::scene_camera_screen", "outputlayers");
			ctx.Connect(m_display_driver_handle, "", m_layer_handle, "outputdrivers");

			// Multi light.
			if (multilight && IsShadingComponent) { // Only export multilight layers
				// for shading components
				int lights_size = (int) lights.size();
				string light_handle;
				string multilight_layer_handle;

				for (int j = 0; j < lights_size; j++) {
					BaseList2D* passed_light = (BaseList2D*) lights[j];

					if (passed_light) {
						multilight_layer_handle =
							string("3dlfc4d::MultiLightDisplayLayer" + std::to_string(i) + string("_") + std::to_string(j));
						ctx.Create(multilight_layer_handle, "outputlayer");
						ctx.SetAttribute(
							multilight_layer_handle,
							(NSI::StringArg("variablename", m_aov.c_str()),
							 NSI::StringArg("layertype", m_variable_type.c_str()),
							 NSI::StringArg("variablesource", m_variable_source.c_str()),
							 NSI::StringArg("scalarformat", m_scalar_format.c_str()),
							 NSI::IntegerArg("withalpha", with_alpha),
							 NSI::IntegerArg("sortkey", sortkey++),
							 NSI::StringArg("filter", filter_output.c_str())));
						light_handle = parser->GetHandleName(passed_light);

						if (passed_light->GetType() != ID_LIGHTGROUP) {
							light_handle =
								string("X_") + light_handle; // Except for light groups, we use
							// the handle of the parent
							// transform of the light
						}

						ctx.Connect(light_handle, "", multilight_layer_handle, "lightset");
						ctx.Connect(m_display_driver_handle,
									"",
									multilight_layer_handle,
									"outputdrivers");
						ctx.Connect(multilight_layer_handle,
									"",
									"3dlfc4d::scene_camera_screen",
									"outputlayers");
					}
				}
			}

			// Custom AOVs
			std::vector<BaseObject*> getCustomAvs = GetCustomAOV(doc);

			for (int k = 0; k < getCustomAvs.size(); k++) {
				BaseContainer* aovs_data = getCustomAvs[k]->GetDataInstance();
				const CustomDataType* aov_dt =
					aovs_data->GetCustomDataType(DL_CUSTOM_COLORS, ID_CUSTOMDATATYPE_AOV);
				iCustomDataTypeCOLORAOV* aov_data = (iCustomDataTypeCOLORAOV*) aov_dt;

				if (!aov_data) {
					aov_data = NewObjClear(iCustomDataTypeCOLORAOV);
				}

				for (int j = 0; j < aov_data->m_row_id.GetCount(); j++) {
					if (aov_data->aov_textName[j].IsEqual(""_s) || aov_data->aov_checked[j] == FALSE) {
						continue;
					}

					String layer = (String) aov_data->aov_textName[j];
					std::string custom_layer = string(
												   "3dlfc4d::CustomLayer " + (std::string) layer.GetCStringCopy() + std::to_string(i) + string("_") + std::to_string(j));
					ctx.Create(custom_layer, "outputlayer");
					ctx.SetAttribute(custom_layer,
									 (NSI::StringArg("variablename",
													 (std::string) layer.GetCStringCopy()),
									  NSI::StringArg("layertype", "color"),
									  NSI::StringArg("scalarformat", m_scalar_format),
									  NSI::IntegerArg("withalpha", with_alpha),
									  NSI::StringArg("variablesource", "shader"),
									  NSI::StringArg("filter", filter_output.c_str())));
					ctx.Connect(
						custom_layer, "", "3dlfc4d::scene_camera_screen", "outputlayers");
					ctx.Connect(
						m_display_driver_handle, "", custom_layer, "outputdrivers");
				}
			}

			// Draw outlines on "Ci" when Rendering on iDisplay
			if (m_aov == "Ci") {
				ctx.SetAttribute(m_layer_handle, NSI::IntegerArg("drawoutlines", 1));
			}
		}
	}
}

void RenderOptionsHook::ConnectNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	return;
}
