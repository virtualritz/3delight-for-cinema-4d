CONTAINER Oarealight
{
	NAME Oarealight;
	INCLUDE Obase;

	GROUP AREA_LIGHT
	{
		DEFAULT 1;
		SEPARATOR {LINE;}
		GROUP
		{
			COLUMNS 2;
				
			COLOR LIGHTCARD_COLOR{SCALE_H;}
			SHADERLINK LIGHTCARD_COLOR_SHADER{ANIM OFF; HIDDEN;}
			SHADERLINK LIGHTCARD_COLOR_SHADER_TEMP{ANIM OFF; HIDDEN;}
			POPUP POPUP_LIGHTCARD_COLOR{SCALE_V;}
			
			REAL LIGHTCARD_INTENSITY {MIN 0; MINSLIDER 0; MAXSLIDER 10; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
			//SHADERLINK LIGHTCARD_INTENSITY_SHADER {ANIM OFF;HIDDEN;}
			//SHADERLINK LIGHTCARD_INTENSITY_SHADER_TEMP {ANIM OFF;HIDDEN;}
			//POPUP POPUP_LIGHTCARD_INTENSITY {SCALE_V;}
			
			REAL LIGHTCARD_EXPOSURE {MINSLIDER -5.0; MAXSLIDER 5.0; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
			
			REAL LIGHTCARD_SPREAD {MIN 0; MAX 1; MINSLIDER 0; MAXSLIDER 1;  STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
			
			LONG LIGHTCARD_DECAY
			{
				CYCLE
				{
					LIGHTCARD_DECAY_NODECAY;
					LIGHTCARD_DECAY_LINEAR;
					LIGHTCARD_DECAY_QUADRATIC;
					LIGHTCARD_DECAY_CUBIC;
				}
			}
			SEPARATOR{}
		}
			GROUP
		{
		
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR LIGHTCARD_GEOMETRY{}
			STATICTEXT{}
			
			
			BOOL LIGHTCARD_NORMALIZE {}
			SEPARATOR{}
			
			BOOL LIGHTCARD_SEEN_BY_CAMERA {}
			SEPARATOR{}
			
			BOOL LIGHTCARD_TWO_SIDED {}
			SEPARATOR{}
			
			BOOL LIGHTCARD_PRELIT {}
			SEPARATOR{}
			
			LONG LIGHTCARD_SHAPE
			{
				CYCLE
				{
					LIGHTCARD_SHAPE_RECTANGLE;
					LIGHTCARD_SHAPE_DISC;
					LIGHTCARD_SHAPE_SPHERE;
					LIGHTCARD_SHAPE_CYLINDER;
				}
			}
			SEPARATOR{}		
			REAL LIGHTCARD_WIDTH {UNIT METER;}
			SEPARATOR{}		
			REAL LIGHTCARD_HEIGHT {UNIT METER;}
			SEPARATOR{}			
			REAL LIGHTCARD_RADIUS {UNIT METER;}
			SEPARATOR{}		
		}	
							
		GROUP
		{
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR LIGHTCARD_CONTRIBUTION{}
			STATICTEXT{}
			
			REAL LIGHTCARD_DIFFUSE {MIN 0; MINSLIDER 0; MAXSLIDER 1; MAX 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
			
			REAL LIGHTCARD_SPECULAR {MIN 0; MINSLIDER 0; MAXSLIDER 1; MAX 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
			REAL LIGHTCARD_HAIR {MIN 0; MINSLIDER 0; MAXSLIDER 1; MAX 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
			
			REAL LIGHTCARD_VOLUME {MIN 0; MINSLIDER 0; MAXSLIDER 1; MAX 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
			SEPARATOR{}
		}	
		
		GROUP
		{
			STATICTEXT{}
			SEPARATOR LIGHTCARD_FILTERS{}
			SEPARATOR{}
			SCALE_H;
			LIGHTFILTERSTCUSTOMTYPE DL_CUSTOM_LIGHT_FILTERS
			{
				CUSTOMGUI LIGHTFILTERSCUSTOMGUI;
			}
			
		}
		
		
		GROUP
		{
			//IN_EXCLUDE AREA_LIGHT_GEOMETRY {}
		}
	}
}
