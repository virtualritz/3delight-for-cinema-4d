#include <vector>
#include "DL_Light_Filters.h"
#include "IDs.h"
#include "LightFilterFunctions.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dldecayfilter.h"
#include "dlgobofilter.h"
#include "oarealight.h"

class LightCard : public ObjectData
{
public:
	static NodeData* Alloc(void)
	{
		return NewObjClear(LightCard);
	}
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual Bool TranslateDescID(GeListNode* node,
								 const DescID& id,
								 DescID& res_id,
								 C4DAtom*& res_at);
	virtual DRAWRESULT Draw(BaseObject* op,
							DRAWPASS drawpass,
							BaseDraw* bd,
							BaseDrawHelp* bh);
	virtual Int32 GetHandleCount(BaseObject* op);
	virtual void GetHandle(BaseObject* op, Int32 i, HandleInfo& info);
	virtual void SetHandle(BaseObject* op,
						   Int32 i,
						   Vector p,
						   const HandleInfo& info);
	// virtual BaseObject* GetVirtualObjects(PluginObject *op, HierarchyHelp
	// *hh);
};

Bool LightCard::Init(GeListNode* node)
{
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	data->SetFloat(LIGHTCARD_WIDTH, 200.0);
	data->SetFloat(LIGHTCARD_HEIGHT, 200.0);
	data->SetFloat(LIGHTCARD_RADIUS, 100.0);
	data->SetVector(LIGHTCARD_COLOR, Vector(1, 1, 1));
	data->SetFloat(LIGHTCARD_INTENSITY, 1.0);
	data->SetFloat(LIGHTCARD_EXPOSURE, 0.0);
	data->SetFloat(LIGHTCARD_SPREAD, 1.0);
	data->SetFloat(LIGHTCARD_DIFFUSE, 1.0);
	data->SetFloat(LIGHTCARD_SPECULAR, 1.0);
	data->SetFloat(LIGHTCARD_HAIR, 1.0);
	data->SetFloat(LIGHTCARD_VOLUME, 1.0);
	data->SetBool(LIGHTCARD_TWO_SIDED, false);
	data->SetInt32(LIGHTCARD_DECAY, LIGHTCARD_DECAY_QUADRATIC);
	data->SetBool(LIGHTCARD_NORMALIZE, false);
	data->SetBool(LIGHTCARD_PRELIT, false);
	data->SetInt32(LIGHTCARD_SHAPE, LIGHTCARD_SHAPE_RECTANGLE);
	// data->SetInt32(LIGHTCARD_SAMPLES,32);
	return true;
}

Bool LightCard::GetDDescription(GeListNode* node,
								Description* description,
								DESCFLAGS_DESC& flags)
{
	if (!description->LoadDescription(node->GetType())) {
		return false;
	}

	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_LIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_lights_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_lights_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected objects
	std::vector<BaseShader*> selected_lights =
		getSelectedFilters((BaseObject*) node,
						   selected_lights_guid,
						   light_filter_data->m_all_light_filters.GetCount());
	// This returns 0 because the GUID provided by the aov layer differs from
	// the GUI of the object here.
	auto AddDescription =
		[](GeListNode* node,
		   Description* description,
	std::vector<BaseShader*> selected_lights) -> maxon::Result<void> {
		for (int i = 0; i < selected_lights.size(); i++)
		{
			BaseObject* object = (BaseObject*) node;

			if (object == nullptr) {
				return maxon::UnexpectedError(MAXON_SOURCE_LOCATION);
			}

			// add dynamic group
			const DescID groupID = DescLevel(i + 10, DTYPE_GROUP, 0);
			BaseContainer settings = GetCustomDataTypeDefault(DTYPE_GROUP);
			settings.SetString(DESC_NAME, selected_lights[i]->GetName());
			description->SetParameter(groupID, settings, 0);
			AutoAlloc<Description> sourceDescription;

			if (sourceDescription == nullptr) {
				return maxon::OutOfMemoryError(MAXON_SOURCE_LOCATION);
			}

			// get the Description of the given object
			if (!selected_lights[i]->GetDescription(sourceDescription,
													DESCFLAGS_DESC::NONE)) {
				return maxon::UnexpectedError(MAXON_SOURCE_LOCATION);
			}

			void* handle = sourceDescription->BrowseInit();
			const BaseContainer* bc = nullptr;
			DescID id, gid;
			int k = (i + 200);

			while (sourceDescription->GetNext(handle, &bc, id, gid)) {
				// insert paramter into the dynamic group
				if (selected_lights[i]->GetType() == DL_DECAYFILTER) {
					switch (id[0].id) {
					case DECAY_FILTER:
					case DECAY_TYPE:
					case DECAY_RANGE_START:
					case DECAY_RANGE_END:
					case DECAY_RAMP:
					case DECAY_TYPE_DISTANCE_LIGHT:
					case DECAY_TYPE_DISTANCE_LIGHT_PLANE:
					case DECAY_TYPE_ANGLE_AXIS:
					case DECAY_TYPE_DISTANCE_AXIS: {
						DescLevel topLevel = id[0];
						topLevel.id += 5000;
						DescID newID;
						newID.PushId(topLevel);
						const Int32 depth = id.GetDepth();

						for (int j = 1; j < depth; ++j) {
							newID.PushId(id[j]);
						}

						description->SetParameter(newID, *bc, groupID);
					}

					default:
						break;
					}

				} else if (selected_lights[i]->GetType() == DL_GOBOFILTER) {
					switch (id[0].id) {
					case GOBO_MAP:
					case GOBO_FILTER:
					case GOBO_DENSITY:
					case GOBO_INVERT:
					case GOBO_SCALE_START:
					case GOBO_SCALE_END:
					case GOBO_OFFSET_START:
					case GOBO_OFFSET_END:
					case S_WRAP_MODE:
					case S_WRAP_CLAMP:
					case S_WRAP_BLACK:
					case S_WRAP_MIRROR:
					case S_WRAP_PERIODIC:
					case T_WRAP_MODE:
					case T_WRAP_CLAMP:
					case T_WRAP_BLACK:
					case T_WRAP_MIRROR:
					case T_WRAP_PERIODIC: {
						DescLevel topLevel = id[0];
						topLevel.id += 5000;
						DescID newID;
						newID.PushId(topLevel);
						const Int32 depth = id.GetDepth();

						for (Int32 j = 1; j < depth; ++j) {
							newID.PushId(id[j]);
						}

						description->SetParameter(newID, *bc, groupID);
					}

					default:
						break;
					}
				}
			}

			sourceDescription->BrowseFree(handle);
		}

		return maxon::OK;
	};
	iferr(AddDescription(node, description, selected_lights)) {
		DiagnosticOutput("Error: @", err);
	}
	flags |= DESCFLAGS_DESC::LOADED;

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_RECTANGLE || data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_DISC) {
		ShowDescriptionElement(node, description, LIGHTCARD_WIDTH, true);
		ShowDescriptionElement(node, description, LIGHTCARD_HEIGHT, true);
		ShowDescriptionElement(node, description, LIGHTCARD_RADIUS, false);

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		ShowDescriptionElement(node, description, LIGHTCARD_WIDTH, false);
		ShowDescriptionElement(node, description, LIGHTCARD_HEIGHT, true);
		ShowDescriptionElement(node, description, LIGHTCARD_RADIUS, true);

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
		ShowDescriptionElement(node, description, LIGHTCARD_WIDTH, false);
		ShowDescriptionElement(node, description, LIGHTCARD_HEIGHT, false);
		ShowDescriptionElement(node, description, LIGHTCARD_RADIUS, true);
	}

	HideAndShowTextures(LIGHTCARD_COLOR_GROUP_PARAM,
						LIGHTCARD_COLOR,
						LIGHTCARD_COLOR_SHADER,
						LIGHTCARD_COLOR_SHADER_TEMP,
						node,
						description,
						data);
	// HideAndShowTextures(LIGHTCARD_INTENSITY_GROUP_PARAM, LIGHTCARD_INTENSITY,
	// LIGHTCARD_INTENSITY_SHADER, LIGHTCARD_INTENSITY_SHADER_TEMP, node,
	// description, data);
	return true;
}

Bool LightCard::GetDEnabling(GeListNode* node,
							 const DescID& id,
							 const GeData& t_data,
							 DESCFLAGS_ENABLE flags,
							 const BaseContainer* itemdesc)
{
	bool enabled = true;

	if (id == LIGHTCARD_TWO_SIDED) {
		BaseObject* obj = (BaseObject*) node;
		BaseContainer* data = obj->GetDataInstance();
		Int32 shape = data->GetInt32(LIGHTCARD_SHAPE);

		if (shape == LIGHTCARD_SHAPE_SPHERE || shape == LIGHTCARD_SHAPE_CYLINDER) {
			enabled = false;
		}
	}

	return enabled;
}

Bool LightCard::TranslateDescID(GeListNode* node,
								const DescID& id,
								DescID& res_id,
								C4DAtom*& res_at)
{
	if (!node) {
		return false;
	}

	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_LIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_lights_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_lights_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected objects
	std::vector<BaseShader*> selected_lights =
		getSelectedFilters((BaseObject*) node,
						   selected_lights_guid,
						   light_filter_data->m_all_light_filters.GetCount());

	// This returns 0 because the GUID provided by the aov layer differs from
	// the GUI of the object here.
	for (int i = 0; i < selected_lights.size(); i++) {
		if (id[0].id >= 5000) {
			BaseObject* object = (BaseObject*) node;

			if (object != nullptr) {
				BaseShader* shader = selected_lights[i];

				if (shader != nullptr) {
					int k = (i + 200);
					res_at = (C4DAtom*) shader;
					// define new ID
					DescLevel topLevel = id[0];
					topLevel.id -= (5000);
					DescID newID;
					newID.PushId(topLevel);
					const Int32 depth = id.GetDepth();

					for (Int32 j = 1; j < depth; ++j) {
						newID.PushId(id[j]);
					}

					res_id = newID;
					return true;
				}
			}
		}
	}

	return ObjectData::TranslateDescID(node, id, res_id, res_at);
}

Bool LightCard::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_LIGHTCARD_COLOR:
			FillPopupMenu(dldata, dp, LIGHTCARD_COLOR_GROUP_PARAM);
			break;

		case POPUP_LIGHTCARD_INTENSITY:
			// FillPopupMenu(dldata, dp, LIGHTCARD_INTENSITY_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

DRAWRESULT
LightCard::Draw(BaseObject* op,
				DRAWPASS drawpass,
				BaseDraw* bd,
				BaseDrawHelp* bh)
{
	if (drawpass == DRAWPASS::OBJECT) {
		BaseContainer* data = op->GetDataInstance();
		float width = (float) data->GetFloat(LIGHTCARD_WIDTH);
		float height = (float) data->GetFloat(LIGHTCARD_HEIGHT);

		if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_RECTANGLE) {
			Vector color = HSVToRGB(Vector(0, 0, 1));
			Matrix m = bh->GetMg();
			bd->SetMatrix_Matrix(NULL, m);
			bd->SetPen(color);
			// Outline
			bd->DrawLine(Vector(-width / 2.0, -height / 2.0, 0),
						 Vector(-width / 2.0, height / 2.0, 0),
						 0);
			bd->DrawLine(Vector(-width / 2.0, height / 2.0, 0),
						 Vector(width / 2.0, height / 2.0, 0),
						 0);
			bd->DrawLine(Vector(width / 2.0, height / 2.0, 0),
						 Vector(width / 2.0, -height / 2.0, 0),
						 0);
			bd->DrawLine(Vector(width / 2.0, -height / 2.0, 0),
						 Vector(-width / 2.0, -height / 2.0, 0),
						 0);
			// Cross
			// bd->DrawLine(Vector(-width / 2.0, -height / 2.0, 0), Vector(width
			// / 2.0, height / 2.0, 0), 0); bd->DrawLine(Vector(-width / 2.0,
			// height / 2.0, 0), Vector(width / 2.0, -height / 2.0, 0), 0);
			// Direction
			bd->DrawLine(Vector(0, 0, 0), Vector(0, 0, width / 2.0), 0);

		} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_DISC) {
			float xLength = (float) data->GetFloat(LIGHTCARD_WIDTH);
			float yLength = (float) data->GetFloat(LIGHTCARD_HEIGHT);
			Vector color = Vector(1, 1, 1);
			bd->SetPen(color);
			Matrix m = bh->GetMg();
			bd->SetMatrix_Matrix(NULL, m);
			Matrix m_circle;
			m_circle.off = Vector64(0, 0, 0);
			m_circle.sqmat.v1 = Vector64(xLength / 2.0, 0, 0);
			m_circle.sqmat.v2 = Vector64(0, yLength / 2.0, 0);
			bd->DrawCircle(m_circle);

		} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
			float radius = (float) data->GetFloat(LIGHTCARD_RADIUS);
			Vector color = Vector(1, 1, 1);
			bd->SetPen(color);
			Matrix m = bh->GetMg();
			bd->SetMatrix_Matrix(NULL, m);
			Matrix m_circle;
			m_circle.off = Vector64(0, 0, 0);
			m_circle.sqmat.v1 = Vector64(radius, 0, 0);
			m_circle.sqmat.v2 = Vector64(0, radius, 0);
			bd->DrawCircle(m_circle);
			m_circle.sqmat.v1 = Vector64(radius, 0, 0);
			m_circle.sqmat.v2 = Vector64(0, 0, radius);
			bd->DrawCircle(m_circle);
			m_circle.sqmat.v1 = Vector64(0, radius, 0);
			m_circle.sqmat.v2 = Vector64(0, 0, radius);
			bd->DrawCircle(m_circle);

		} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
			float radius = (float) data->GetFloat(LIGHTCARD_RADIUS);
			float zLength = (float) data->GetFloat(LIGHTCARD_HEIGHT);
			Vector color = Vector(1, 1, 1);
			bd->SetPen(color);
			Matrix m = bh->GetMg();
			bd->SetMatrix_Matrix(NULL, m);
			bd->DrawLine(Vector(0, -radius, -zLength / 2.0),
						 Vector(0, -radius, zLength / 2.0),
						 0);
			bd->DrawLine(
				Vector(0, radius, -zLength / 2.0), Vector(0, radius, zLength / 2.0), 0);
			bd->DrawLine(Vector(-radius, 0, -zLength / 2.0),
						 Vector(-radius, 0, zLength / 2.0),
						 0);
			bd->DrawLine(
				Vector(radius, 0, -zLength / 2.0), Vector(radius, 0, zLength / 2.0), 0);
			Matrix m_circle;
			m_circle.off = Vector64(0, 0, zLength / 2.0);
			m_circle.sqmat.v1 = Vector64(radius, 0, 0);
			m_circle.sqmat.v2 = Vector64(0, radius, 0);
			bd->DrawCircle(m_circle);
			m_circle.off = Vector64(0, 0, -zLength / 2.0);
			m_circle.sqmat.v1 = Vector64(radius, 0, 0);
			m_circle.sqmat.v2 = Vector64(0, radius, 0);
			bd->DrawCircle(m_circle);
			color = Vector(1, 0.76, 0.3);
			bd->SetPen(color);
			AutoAlloc<AtomArray> selected_objects;
			GetActiveDocument()->GetActiveObjects(*selected_objects,
												  GETACTIVEOBJECTFLAGS::NONE);

			if (selected_objects) {
				Int32 object_count = selected_objects->GetCount();

				for (int i = 0; i < object_count; i++) {
					BaseObject* object = (BaseObject*) selected_objects->GetIndex(i);

					if (object && object == op) {
						bd->DrawLine(
							Vector(0, 0, zLength / 2.0), Vector(radius, 0, zLength / 2.0), 0);
						bd->DrawLine(Vector(radius, 0, zLength / 2.0),
									 Vector(radius, 0, zLength / 2.0),
									 0);
						bd->DrawLine(Vector(radius, 0, zLength / 2.0),
									 Vector(radius, 0, -zLength / 2.0),
									 0);
						bd->DrawLine(Vector(radius, 0, -zLength / 2.0),
									 Vector(0, 0, -zLength / 2.0),
									 0);
					}
				}
			}
		}
	}

	return ObjectData::Draw(op, drawpass, bd, bh);
}

Int32 LightCard::GetHandleCount(BaseObject* op)
{
	BaseContainer* data = op->GetDataInstance();

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_RECTANGLE || data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_DISC) {
		return 4;

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
		return 1;

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		return 2;
	}

	return 0;
}

void LightCard::GetHandle(BaseObject* op, Int32 i, HandleInfo& info)
{
	BaseContainer* data = op->GetDataInstance();

	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		float radius = (float) data->GetFloat(LIGHTCARD_RADIUS);
		float height = (float) data->GetFloat(LIGHTCARD_HEIGHT);

		if (i == 0) {
			info.position = Vector(0, 0, height / 2.0);
			info.direction = Vector(0, 0, 1.0);
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
		}

		if (i == 1) {
			info.position = Vector(radius, 0, height / 2.0);
			info.direction = Vector(-1.0, 0, 0);
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
		}

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
		float radius = (float) data->GetFloat(LIGHTCARD_RADIUS);
		info.position = Vector(radius, 0, 0);
		info.direction = Vector(-1.0, 0, 0);
		info.type = HANDLECONSTRAINTTYPE::LINEAR;

	} else {
		float width = (float) data->GetFloat(LIGHTCARD_WIDTH);
		float height = (float) data->GetFloat(LIGHTCARD_HEIGHT);

		if (i == 0) {
			info.position = Vector(-width / 2.0, 0, 0);
			info.direction = Vector(-1.0, 0, 0);
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
			// return (Vector(-width/2.0,0,0));

		} else if (i == 1) {
			info.position = Vector(width / 2.0, 0, 0);
			info.direction = Vector(1.0, 0, 0);
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
			// return (Vector(width/2.0,0,0));

		} else if (i == 2) {
			info.position = Vector(0, -height / 2.0, 0);
			info.direction = Vector(0, -1.0, 0);
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
			// return (Vector(0,-height/2.0,0));

		} else if (i == 3) {
			info.position = Vector(0, height / 2.0, 0);
			info.direction = Vector(0, 1.0, 0);
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
			// return (Vector(0,height/2.0,0));
		}
	}
}

void LightCard::SetHandle(BaseObject* op, Int32 i, Vector p, const HandleInfo& info)
{
	BaseContainer* data = op->GetDataInstance();

	// float width=(float)data->GetFloat(LIGHTCARD_WIDTH);
	// float height=(float)data->GetFloat(LIGHTCARD_HEIGHT);
	if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_CYLINDER) {
		if (i == 0) {
			data->SetFloat(LIGHTCARD_HEIGHT, abs(p.z) * 2);

		} else {
			data->SetFloat(LIGHTCARD_RADIUS, abs(p.x));
		}

	} else if (data->GetInt32(LIGHTCARD_SHAPE) == LIGHTCARD_SHAPE_SPHERE) {
		data->SetFloat(LIGHTCARD_RADIUS, abs(p.x));

	} else {
		if (i == 0 || i == 1) {
			data->SetFloat(LIGHTCARD_WIDTH, abs(p.x) * 2);

		} else {
			data->SetFloat(LIGHTCARD_HEIGHT, abs(p.y) * 2);
		}
	}
}

Bool RegisterAreaLight(void)
{
	return RegisterObjectPlugin(ID_AREALIGHT,
								"Area Light"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								LightCard::Alloc,
								"Oarealight"_s,
								AutoBitmap("shelf_quadraticAreaLight_200.png"_s),
								0);
}
