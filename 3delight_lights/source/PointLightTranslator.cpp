#include "PointLightTranslator.h"
#include "3DelightEnvironment.h"
#include "DL_Light_Filters.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "LightFilterFunctions.h"
#include "nsi.hpp"
#include "opointlight.h"

void PointLightTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Create a particle and connect it to the parent transform
	std::string handle = std::string(Handle);
	std::string transform_handle = std::string(ParentTransformHandle);
	ctx.Create(handle, "particles");
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	Bool seen_by_camera = data->GetBool(POINTLIGHT_SEEN_BY_CAMERA);
	int camera_visibility = 0;

	if (seen_by_camera) {
		camera_visibility = 1;
	}

	std::string attributes_handle = handle + "attributes";
	std::string transform_attributes = transform_handle + "attributes";
	ctx.Create(transform_attributes, "attributes");
	ctx.Connect(transform_attributes, "", transform_handle, "geometryattributes");
	ctx.Create(attributes_handle, "attributes");
	ctx.SetAttribute(attributes_handle,
					 (NSI::IntegerArg("visibility.camera", camera_visibility),
					  NSI::IntegerArg("visibility.camera.priority", 10)));
	std::string shader_handle = handle + "shader";
	ctx.Create(shader_handle, "shader");
	ctx.Connect(shader_handle, "", attributes_handle, "surfaceshader");
	std::vector<float> P(3); // One particle
	P[0] = 0;
	P[1] = 0;
	P[2] = 0;
	NSI::Argument arg_P("P");
	arg_P.SetType(NSITypePoint);
	arg_P.SetCount(1);
	arg_P.SetValuePointer((void*) &P[0]);
	ctx.SetAttribute(handle, (arg_P));
	/*
	            Adding a transformation matrix which we connect to the
	            main transform handle of the light source. This newly
	            created transform will be used to fix the mapping of the
	            image texture used on gobo light filter.
	    */
	std::string scale_transform = handle + "scale_transform";
	ctx.Create(scale_transform, "transform");
	double scale[16] { 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1 };
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &scale[0]);
	ctx.SetAttribute(scale_transform, (xform));
}

void PointLightTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	NSI::Context& ctx(parser->GetContext());
	std::string handle = std::string(Handle);
	std::string scale_transform = handle + "scale_transform";
	std::string transform_handle = std::string(ParentTransformHandle);
	std::string attributes_handle = handle + "attributes";
	std::string shader_handle = handle + "shader";
	ctx.Connect(scale_transform, "", transform_handle, "objects");
	ctx.Connect(handle, "", scale_transform, "objects");
	ctx.Connect(attributes_handle, "", scale_transform, "geometryattributes");
}

void PointLightTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	std::string handle = std::string(Handle);
	std::string shader_handle = std::string(Handle) + "shader";
	float radius = (float) data->GetFloat(POINTLIGHT_RADIUS);
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath = (std::string) delightpath + "/osl/pointLight.oso";
	float intensity = (float) data->GetFloat(POINTLIGHT_INTENSITY);
	intensity = intensity * 100 * 100; // Normalize to meters, not centimeters
	float exposure = (float) data->GetFloat(POINTLIGHT_EXPOSURE);
	int decay = data->GetInt32(POINTLIGHT_DECAY);
	float diffuse = (float) data->GetFloat(POINTLIGHT_DIFFUSE);
	float specular = (float) data->GetFloat(POINTLIGHT_SPECULAR);
	float hair = (float) data->GetFloat(POINTLIGHT_HAIR);
	float volume = (float) data->GetFloat(POINTLIGHT_VOLUME);
	Vector color = toLinear(data->GetVector(POINTLIGHT_COLOR), doc);
	float col[3];
	col[0] = (float) color.x;
	col[1] = (float) color.y;
	col[2] = (float) color.z;
	BaseList2D* color_shader = data->GetLink(POINTLIGHT_COLOR_SHADER, doc);
	BaseList2D* intensity_shader =
		data->GetLink(POINTLIGHT_INTENSITY_SHADER, doc);
	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", &shaderpath[0]));

	if (color_shader) {
		std::string link_shader = parser->GetHandleName(color_shader);
		ctx.Connect(link_shader, "outColor", shader_handle, "i_color");

	} else {
		args.Add(new NSI::ColorArg("i_color", &col[0]));
	}

	if (intensity_shader) {
		std::string link_shader = parser->GetHandleName(intensity_shader);
		ctx.Connect(link_shader, "outColor[0]", shader_handle, "intensity");

	} else {
		args.Add(new NSI::FloatArg("intensity", intensity));
	}

	args.Add(new NSI::IntegerArg("decayRate", decay));
	args.Add(new NSI::FloatArg("diffuse_contribution", diffuse));
	args.Add(new NSI::FloatArg("specular_contribution", specular));
	args.Add(new NSI::FloatArg("hair_contribution", hair));
	args.Add(new NSI::FloatArg("volume_contribution", volume));
	args.Add(new NSI::FloatArg("exposure", exposure));
	ctx.SetAttributeAtTime(shader_handle, info->sample_time, args);
	ctx.SetAttributeAtTime(
		handle, info->sample_time, (NSI::FloatArg("width", 2 * radius)));
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_POINTLIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_filters_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_filters_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected filters
	std::vector<BaseShader*> selected_filters =
		getSelectedFilters((BaseObject*) obj,
						   selected_filters_guid,
						   light_filter_data->m_all_light_filters.GetCount());

	for (int i = 0; i < selected_filters.size(); i++) {
		std::string filter_transform =
			"X_" + parser->GetHandleName(selected_filters[i]);
		std::string filter_shader =
			parser->GetHandleName(selected_filters[i]) + "shader";
		ctx.Connect(filter_shader, "filter_output", shader_handle, "filter");
	}
}
