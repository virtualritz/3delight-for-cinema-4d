#include <math.h>
#include <vector>
#include "DL_Light_Filters.h"
#include "IDs.h"
#include "LightFilterFunctions.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "dldecayfilter.h"
#include "dlgobofilter.h"
#include "ospotlight.h"

class SpotLight : public ObjectData
{
public:
	static NodeData* Alloc(void)
	{
		return NewObjClear(SpotLight);
	}
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool TranslateDescID(GeListNode* node,
								 const DescID& id,
								 DescID& res_id,
								 C4DAtom*& res_at);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual DRAWRESULT Draw(BaseObject* op,
							DRAWPASS drawpass,
							BaseDraw* bd,
							BaseDrawHelp* bh);
};

Bool SpotLight::Init(GeListNode* node)
{
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	data->SetFloat(SPOTLIGHT_RADIUS, 0.001);
	data->SetVector(SPOTLIGHT_COLOR, Vector(1, 1, 1));
	data->SetFloat(SPOTLIGHT_INTENSITY, 1.0);
	data->SetFloat(SPOTLIGHT_EXPOSURE, 0.0);
	data->SetFloat(SPOTLIGHT_SPREAD, 1.0);
	data->SetInt32(SPOTLIGHT_DECAY, SPOTLIGHT_DECAY_QUADRATIC);
	data->SetFloat(SPOTLIGHT_CONE_ANGLE, 40.0);
	data->SetFloat(SPOTLIGHT_PENUMBRA_ANGLE, 0.0);
	data->SetFloat(SPOTLIGHT_DROPOFF, 0.0);
	data->SetBool(SPOTLIGHT_BARNDOORS_USE, false);
	data->SetFloat(SPOTLIGHT_BARNDOORS_LEFT, 90);
	data->SetFloat(SPOTLIGHT_BARNDOORS_RIGHT, 90);
	data->SetFloat(SPOTLIGHT_BARNDOORS_TOP, 90);
	data->SetFloat(SPOTLIGHT_BARNDOORS_BOTTOM, 90);
	data->SetFloat(SPOTLIGHT_DIFFUSE, 1.0);
	data->SetFloat(SPOTLIGHT_SPECULAR, 1.0);
	data->SetFloat(SPOTLIGHT_HAIR, 1.0);
	data->SetFloat(SPOTLIGHT_VOLUME, 1.0);
	return true;
}

Bool SpotLight::GetDDescription(GeListNode* node,
								Description* description,
								DESCFLAGS_DESC& flags)
{
	description->LoadDescription(node->GetType());
	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_SPOTLIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_lights_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_lights_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected filters
	std::vector<BaseShader*> selected_lights =
		getSelectedFilters((BaseObject*) node,
						   selected_lights_guid,
						   light_filter_data->m_all_light_filters.GetCount());
	auto AddDescription =
		[](GeListNode* node,
		   Description* description,
	std::vector<BaseShader*> selected_lights) -> maxon::Result<void> {
		for (int i = 0; i < selected_lights.size(); i++)
		{
			BaseObject* object = (BaseObject*) node;

			if (object == nullptr) {
				return maxon::UnexpectedError(MAXON_SOURCE_LOCATION);
			}

			// add dynamic group
			const DescID groupID = DescLevel(i + 10, DTYPE_GROUP, 0);
			BaseContainer settings = GetCustomDataTypeDefault(DTYPE_GROUP);
			settings.SetString(DESC_NAME, selected_lights[i]->GetName());
			description->SetParameter(groupID, settings, 0);
			AutoAlloc<Description> sourceDescription;

			if (sourceDescription == nullptr) {
				return maxon::OutOfMemoryError(MAXON_SOURCE_LOCATION);
			}

			// get the Description of the given object
			if (!selected_lights[i]->GetDescription(sourceDescription,
													DESCFLAGS_DESC::NONE)) {
				return maxon::UnexpectedError(MAXON_SOURCE_LOCATION);
			}

			void* handle = sourceDescription->BrowseInit();
			const BaseContainer* bc = nullptr;
			DescID id, gid;
			int k = (i + 200);

			while (sourceDescription->GetNext(handle, &bc, id, gid)) {
				// insert paramter into the dynamic group
				if (selected_lights[i]->GetType() == DL_DECAYFILTER) {
					switch (id[0].id) {
					case DECAY_FILTER:
					case DECAY_TYPE:
					case DECAY_RANGE_START:
					case DECAY_RANGE_END:
					case DECAY_RAMP:
					case DECAY_TYPE_DISTANCE_LIGHT:
					case DECAY_TYPE_DISTANCE_LIGHT_PLANE:
					case DECAY_TYPE_ANGLE_AXIS:
					case DECAY_TYPE_DISTANCE_AXIS: {
						DescLevel topLevel = id[0];
						topLevel.id += 5000;
						DescID newID;
						newID.PushId(topLevel);
						const Int32 depth = id.GetDepth();

						for (Int32 j = 1; j < depth; ++j) {
							newID.PushId(id[j]);
						}

						description->SetParameter(newID, *bc, groupID);
					}

					default:
						break;
					}

				} else if (selected_lights[i]->GetType() == DL_GOBOFILTER) {
					switch (id[0].id) {
					case GOBO_MAP:
					case GOBO_FILTER:
					case GOBO_DENSITY:
					case GOBO_INVERT:
					case GOBO_SCALE_START:
					case GOBO_SCALE_END:
					case GOBO_OFFSET_START:
					case GOBO_OFFSET_END:
					case S_WRAP_MODE:
					case S_WRAP_CLAMP:
					case S_WRAP_BLACK:
					case S_WRAP_MIRROR:
					case S_WRAP_PERIODIC:
					case T_WRAP_MODE:
					case T_WRAP_CLAMP:
					case T_WRAP_BLACK:
					case T_WRAP_MIRROR:
					case T_WRAP_PERIODIC: {
						DescLevel topLevel = id[0];
						topLevel.id += 5000;
						DescID newID;
						newID.PushId(topLevel);
						const Int32 depth = id.GetDepth();

						for (Int32 j = 1; j < depth; ++j) {
							newID.PushId(id[j]);
						}

						description->SetParameter(newID, *bc, groupID);
					}

					default:
						break;
					}
				}
			}

			sourceDescription->BrowseFree(handle);
		}

		return maxon::OK;
	};
	iferr(AddDescription(node, description, selected_lights)) {
		DiagnosticOutput("Error: @", err);
	}
	flags |= DESCFLAGS_DESC::LOADED;
	HideAndShowTextures(SPOTLIGHT_COLOR_GROUP_PARAM,
						SPOTLIGHT_COLOR,
						SPOTLIGHT_COLOR_SHADER,
						SPOTLIGHT_COLOR_SHADER_TEMP,
						node,
						description,
						data);
	// HideAndShowTextures(SPOTLIGHT_INTENSITY_GROUP_PARAM, SPOTLIGHT_INTENSITY,
	//					SPOTLIGHT_INTENSITY_SHADER,
	//SPOTLIGHT_INTENSITY_SHADER_TEMP, node, description, data);
	return true;
}

Bool SpotLight::TranslateDescID(GeListNode* node,
								const DescID& id,
								DescID& res_id,
								C4DAtom*& res_at)
{
	if (!node) {
		return false;
	}

	BaseObject* op = (BaseObject*) node;
	BaseContainer* data = op->GetDataInstance();
	const CustomDataType* light_filters = data->GetCustomDataType(
			DL_CUSTOM_SPOTLIGHT_FILTERS, ID_CUSTOMDATATYPE_LIGHTFILTERS);
	iCustomDataTypeFilters* light_filter_data =
		(iCustomDataTypeFilters*) (light_filters);

	if (!light_filter_data) {
		light_filter_data = NewObjClear(iCustomDataTypeFilters);
	}

	std::vector<Int32> selected_lights_guid;

	for (int i = 0;
			i < light_filter_data->m_selected_light_filters_itemID.GetCount();
			i++)
		selected_lights_guid.push_back(
			light_filter_data->m_selected_light_filters_itemID[i]);

	// Getting selected filter
	std::vector<BaseShader*> selected_lights =
		getSelectedFilters((BaseObject*) node,
						   selected_lights_guid,
						   light_filter_data->m_all_light_filters.GetCount());

	for (int i = 0; i < selected_lights.size(); i++) {
		if (id[0].id >= 5000) {
			BaseObject* object = (BaseObject*) node;

			if (object != nullptr) {
				BaseShader* shader = selected_lights[i];

				if (shader != nullptr) {
					int k = (i + 200);
					res_at = (C4DAtom*) shader;
					// define new ID
					DescLevel topLevel = id[0];
					topLevel.id -= (5000);
					DescID newID;
					newID.PushId(topLevel);
					const Int32 depth = id.GetDepth();

					for (Int32 j = 1; j < depth; ++j) {
						newID.PushId(id[j]);
					}

					res_id = newID;
					return true;
				}
			}
		}
	}

	return ObjectData::TranslateDescID(node, id, res_id, res_at);
}

Bool SpotLight::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_SPOTLIGHT_COLOR:
			FillPopupMenu(dldata, dp, SPOTLIGHT_COLOR_GROUP_PARAM);
			break;

		case POPUP_SPOTLIGHT_INTENSITY:
			// FillPopupMenu(dldata, dp, SPOTLIGHT_INTENSITY_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

DRAWRESULT
SpotLight::Draw(BaseObject* op,
				DRAWPASS drawpass,
				BaseDraw* bd,
				BaseDrawHelp* bh)
{
	if (drawpass == DRAWPASS::OBJECT) {
		BaseContainer* data = op->GetDataInstance();
		float angle = (float) data->GetFloat(SPOTLIGHT_CONE_ANGLE) / 2.0;
		float Point2 = 400;
		float Point1 = 0;
		Vector color = data->GetVector(SPOTLIGHT_COLOR);
		bd->SetPen(color);
		Matrix m = bh->GetMg();
		bd->SetMatrix_Matrix(NULL, m);
		/*
			                We take the line at z axis as our origin line. It has a
			   slope of 0. This means that when drawing around x and z axis the
			   slope of the second line will be tan(angle); because tan(angle) =
			   (slope2-slope1)/(1+slope1*slope2) where slope1 is 0;
			        */
		float SlopeXY = tan(angle * PI / 180.0f);
		/*
			                Knowing the angle between two lines where one line is
			   known and one point of the other line is known as well we can find
			   the other point as below.
			        */
		Point1 = Point2 * SlopeXY;
		bd->DrawLine(Vector(0, 0, 0), Vector(0, Point1, Point2), 0);
		bd->DrawLine(Vector(0, 0, 0), Vector(0, -Point1, Point2), 0);
		bd->DrawLine(Vector(0, 0, 0), Vector(Point1, 0, Point2), 0);
		bd->DrawLine(Vector(0, 0, 0), Vector(-Point1, 0, Point2), 0);
		Matrix m_circle;
		m_circle.off = Vector64(0, 0, 0);
		// circle
		m_circle.sqmat.v1 = Vector64(Point1, 0, 0);
		m_circle.sqmat.v2 = Vector64(0, Point1, 0);
		m_circle.off = Vector64(0, 0, Point2);
		bd->DrawCircle(m_circle);
		// arrow
		int Line = 300;
		int arrow = 20;
		bd->DrawLine(Vector(0, 0, 0), Vector(0, 0, Line), 0);
		bd->DrawLine(
			Vector(-arrow / 2.0, 0, Line), Vector(0, 0, Line + (2 * arrow)), 0);
		bd->DrawLine(
			Vector(0, 0, Line + (2 * arrow)), Vector(arrow / 2.0, 0, Line), 0);
		bd->DrawLine(
			Vector(-arrow / 2.0, 0, Line), Vector(arrow / 2.0, 0, Line), 0);
	}

	return ObjectData::Draw(op, drawpass, bd, bh);
}

bool RegisterSpotLight(void)
{
	return RegisterObjectPlugin(ID_SPOTLIGHT,
								"Spot Light"_s,
								OBJECT_GENERATOR | PLUGINFLAG_HIDEPLUGINMENU,
								SpotLight::Alloc,
								"Ospotlight"_s,
								AutoBitmap("shelf_quadraticSpotLight_200.png"_s),
								0);
}
