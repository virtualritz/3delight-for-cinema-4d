#pragma once

#include "DL_TranslatorPlugin.h"
#include "c4d.h"

class LightGroupTranslator : public DL_TranslatorPlugin
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void ConnectNSINodes(const char* Handle,
								 const char* ParentTransformHandle,
								 BaseList2D* C4DNode,
								 BaseDocument* doc,
								 DL_SceneParser* parser);
};