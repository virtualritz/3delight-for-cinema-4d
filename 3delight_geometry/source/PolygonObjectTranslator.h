#ifndef POLYGON_OBJECT_TRANSLATOR_H
#define POLYGON_OBJECT_TRANSLATOR_H

#include <vector>
#include "DL_API.h"
#include "c4d.h"
//#include "Edge.h"
#include <map>
#include <string>

class PolygonObjectTranslator : public DL_TranslatorPlugin
{
public:
	virtual void Init(BaseList2D* C4DNode,
					  BaseDocument* doc,
					  DL_SceneParser* parser);
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);

private:
	bool is_subd;
	// long UV_subdivision_mode;
	bool has_phong;
};

#endif