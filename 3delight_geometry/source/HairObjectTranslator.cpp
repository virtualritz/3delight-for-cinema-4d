#include "HairObjectTranslator.h"
#include "c4d.h"
#include "nsi.hpp"

using namespace std;

void HairObjectTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	HairObject* hair = (HairObject*) C4DNode;

	if (!hair) {
		return;
	}

	HairMaterialData* mat;
	hair->Lock(doc, nullptr, false, 0);
	HairGuides* guides = hair->GenerateHair(0, NOTOK, NOTOK, &mat);

	if (!guides) {
		hair->Unlock();
		return;
	}

	NSI::Context& ctx(parser->GetContext());
	long nguides = guides->GetCount();
	long pointsPerGuide = guides->GetGuidePointCount();
	// string handle(Handle);
	ctx.Create(Handle, "cubiccurves");
	ctx.Connect(Handle, "", ParentTransformHandle, "objects");
	ctx.SetAttribute(Handle,
					 (NSI::IntegerArg("nvertices", pointsPerGuide),
					  // NSI::StringArg("basis", "linear"),
					  NSI::CStringPArg("basis", "catmull-rom"),
					  NSI::IntegerArg("extrapolate", 1)));
	// UVs
	vector<float> st;
	st.reserve(nguides * 2);
	Vector UV;

	for (long i = 0; i < nguides; i++) {
		UV = guides->GetRootUV(i);
		st.push_back(UV.x);
		st.push_back(UV.y);
	}

	int st_flags = (NSIParamPerFace);
	NSI::Argument arg_st("st");
	arg_st.SetCount(nguides);
	arg_st.SetArrayType(NSITypeFloat, 2);
	arg_st.SetValuePointer((void*) &st[0]);
	arg_st.SetFlags(st_flags);
	ctx.SetAttribute(Handle, (arg_st));

	if (mat) {
		// Hair thickness
		vector<float> W;
		W.reserve(nguides * pointsPerGuide);
		float thickness;
		float t;

		for (long i = 0; i < nguides; i++) {
			for (long j = 0; j < pointsPerGuide; j++) {
				t = float(j) / float(pointsPerGuide - 1);
				thickness = mat->GetThickness(i, t);
				W.push_back(thickness);
			}
		}

		NSI::Argument arg_W("width");
		arg_W.SetType(NSITypeFloat);
		arg_W.SetCount(nguides * pointsPerGuide);
		arg_W.SetValuePointer((void*) &W[0]);
		ctx.SetAttribute(Handle, (arg_W));
	}

	hair->Unlock();
}

void HairObjectTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	HairObject* hair = (HairObject*) C4DNode;

	if (!hair) {
		return;
	}

	hair->Lock(doc, nullptr, false, 0);
	HairGuides* guides = hair->GenerateHair();
	hair->Unlock();

	if (!guides) {
		return;
	}

	NSI::Context& ctx(parser->GetContext());
	Vector* points = guides->GetPoints();
	long pointcount = guides->GetPointCount();
	// Vertex positions
	vector<float> P(pointcount * 3);

	for (int i = 0; i < pointcount; i++) {
		P[i * 3] = (float) points[i].x;
		P[i * 3 + 1] = (float) points[i].y;
		P[i * 3 + 2] = (float) points[i].z;
	}

	NSI::Argument arg_P("P");
	arg_P.SetType(NSITypePoint);
	arg_P.SetCount(pointcount);
	arg_P.SetValuePointer((void*) &P[0]);
	string handle = string(Handle);
	ctx.SetAttributeAtTime(handle, info->sample_time, (arg_P));
}