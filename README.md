# 3Delight For Cinema 4D
A [3Delight](https://www.3delight.com/) rendering plugin for *MAXON
Cinema 4D*.

*3Delight for Cinema 4D* consists of four separate plugins:

* `3delight`
* `3delight_geometry`
* `3delight_shaders`
* `3delight_lights`

Each of these plugins comes with a `projectdefinition.txt` file that must be
used together with the *Cinema 4D Project Tool* to generate build solutions
for your desired platform.
## Building
Make sure you have a current and working [3Delight](https://www.3delight.com/download)
installation.
### macOS
1.  Make sure you have a current [XCode](https://apps.apple.com/us/app/xcode/id497799835)
    installation.
    *   If the *App Store* tells you that you need to upgrade *macOS* and
        you do not want to do that, grab an older Xcode installer that is
        compatible with your macOS version [from this website](https://xcodereleases.com/)

    The following build instructions are for *Cinema 4D R23*. They have
    also been tested to work with *R21* and *R22*.
    If you want to build against an older version of the *SDK* you need
    to download a different version of the *Cinema 4D Project Tool* from
    [here](https://developers.maxon.net/?page_id=1118) (step 3) and
    change the path in *step 7* and possibly *step 14*.

2.  Create a folder to work in and change into it:
    ```
    mkdir 3dlfc4d
    cd 3dlfc4d
    ```
3.  Download the *Cinema 4D Project Tool*:
    ```
    wget https://developers.maxon.net/?dl_id=136 -O project_tool_r23.zip
    ```
4.  Remove the *quarantine attribute* from the *Project Tool ZIP*:
    ```
    xattr -d "com.apple.quarantine" project_tool_r23.zip
    ```
5.  Unzip the *Project Tool* into the `project_tool` folder:
    ```
    unzip -d project_tool project_tool_r23.zip
    ```
6.  Make the *Project Tool* application executable:
    ```
    chmod 555 project_tool/kernel_app.app/Contents/MacOS/kernel_app
    ```
7.  Unzip the *SDK* from your *Cinema 4D* installation:
    ```
    unzip -d c4d_r23_sdk /Applications/Maxon\ Cinema\ 4D\ R23/sdk.zip
    ```
8.  Copy the `frameworks` folder from the *SDK* into the project folder.
    ```
    cp -R c4d_r23_sdk/frameworks .
    ```
    *If you are building for R22 or later you can also create a symbolic
    link to `frameworks` instead.*
9.  Clone the *3Delight For Cinema 4D* repository into the `plugins`
    folder:
    ```
    git clone https://gitlab.com/3Delight/3delight-for-cinema-4d plugins
    ```
10. Run the *Project Tool*:
    ```
    project_tool/kernel_app.app/Contents/MacOS/kernel_app g_updateproject=`pwd`
    ```
11. Build the plugins:
    ```
    xcodebuild -project plugins/project/plugins.xcodeproj -alltargets -configuration Release -UseModernBuildSystem=NO
    ```
    *  You may get an error along those lines:
        ```
        xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance
        ```
        In this case you need to re-link your *Xcode* installation via `xcode-select`.
        This requires `root` previleges (will ask for your password):
        ```
        sudo xcode-select -s /Applications/Xcode.app/Contents/Developer
        ```
        After this re-run step *11*.
12. After the build finished plugins are found in the `build` folder.
    You can print the full path of this folder:
    ```
    echo `pwd`/build
    ```
13. Add this folder inside *Cinema 4D* under **Preferences ⟶ Plugins**.
14. Restart Cinema 4D to load the plugins.
    *   If you have trouble rendering/opening a *3Delight Display*, close
        *Cinema 4D* and open it again from a terminal:
        ```
        /Applications/Maxon\ Cinema\ 4D\ R23/Cinema\ 4D.app/Contents/MacOS/Cinema\ 4D&
        ```
        This is a bit cumbersome but it is the only workaround until we
        find out what the cause for [this issue](https://gitlab.com/3Delight/3delight-for-cinema-4d/-/issues/30)
        is.
### Windows
Make sure you have a current [Visual Studio](https://visualstudio.microsoft.com/downloads/)
installation.

**_Note that Visual Studio 2019 is required to build against R23._**

Run the Cinema4D Project Tool:
```
<location of project tool>\kernel_app_64bit.exe g_updateproject=%cd%
```
Build the plugins:
```
msbuild -project project/plugins.sln /p:configuration=release /p:platform=x64
```

After the build finished plugins are found in:

```
3delight/3delight.xdl64
3delight_geometry/3delight_geometry.xdl64
3delight_shaders/3delight_shaders.xdl64
3delight_lights/3delight_lights.xdl64
```
