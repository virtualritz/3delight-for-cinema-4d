#!/bin/bash
astyle -A10 --indent=force-tab -r -xe -k1 -W1 -F "*.cpp"
astyle -A10 --indent=force-tab -r -xe -k1 -W1 -F "*.h"
